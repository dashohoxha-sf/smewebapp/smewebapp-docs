<?php
/**
 * The constants defined in this file change the behaviour  
 * of the framework. You can change the values of the constants
 * according to the instructions given in comments.
 */

/**
 * This is the first page of the application. The framework looks
 * for it at the template folder (specified by TPL). 
 */
define("FIRSTPAGE", "main/main.html");

/**
 * if this constant is true, the framework will load the DB component
 * and will open a default connection with the db specified in the
 * file 'config/const.DB.php'
 */
define("USES_DB", false);

/**
 * this constant defines the type of DB that the application uses
 * it can be: "MySQL", "Oracle", "ODBC", "ProgreSQL", etc.
 * (except "MySQL", the others are not implemented yet)
 */
define("DB_TYPE", "MySQL");

/**
 * This constant is the value returned by the framework 
 * for a DB variable that has a NULL value. It can be
 * "", "NULL", NULL, etc.
 */
define("NULL_VALUE", "NULL");

/**
 * This constant sets the format of the error message that is displayed
 * when a {{variable}} is not found. 'var_name' is replaced by
 * the actual variable name. Examples: "'var_name' is not defined",
 * "", "undefined", etc. It cannot contain "{{var_name}}" inside.
 */
define("VAR_NOT_FOUND", "{var_name}");

/**
 * When this constant is true, then the CGI vars are displayed
 * at the URL window of the browser. See also SHOW_EXTERNAL_LINK
 * at const.Debug.php.
 */
define("DISPLAY_CGI_VARS", false);

/**
 * The file that contains additional configuration options,
 * which are specific for the application.
 */
define("APP_CONFIG_FILE", "books.conf");

//define the application configuration options
//which are declared in the file APP_CONFIG_FILE
if (file_exists(APP_CONFIG_FILE))
{
  $arr_lines = file(APP_CONFIG_FILE);
  for ($i=0; $i < sizeof($arr_lines); $i++)
    {
      $line = trim($arr_lines[$i]);
      if ($line=='')      continue;
      if ($line[0]=='#')  continue;
      list($const, $value) = split('=', $line, 2);
      eval("\$val=$value;");
      define($const, $val);
    }
}
?>