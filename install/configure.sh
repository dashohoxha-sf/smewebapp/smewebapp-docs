#!/bin/bash

### go to the root dir
cd $(dirname $0)/..

### make all shell scripts executable (in case that some are not)
find . -name '*.sh' | xargs chmod +x  
find content/ -name '*.php' | xargs chmod +x  
chmod +x content/SVN/commit-email.pl

### set proper ownership to some application files
### that need to be accessed (written) by apache
app_files="templates/menu/menu.xml
           templates/menu/menu_items.js
           templates/menu/book_list.php
           templates/admin/edit_menu/menu/menu.xml
           templates/admin/access_rights/
           templates/admin/access_rights/users"
./chown.sh $app_files

### modify the php scripts in 'content/' by changing
### the first line to the correct path of the php
php=$(which php)
find content/ -name '*.php' | xargs sed "1 c #\!$php -q" -i
