#!/bin/bash

cat <<EOF

make help      -- display this help screen
make chown     -- compile 'chown.c'
make configure -- configure the application files
make install   -- make content, search, downloads, etc.

There are also these targets:
make l10n
make set_su_passwd
make content
make swish-e
make search
make svn
make downloads
make clean

EOF
