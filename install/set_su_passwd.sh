#!/bin/bash

### go to the root dir
cd $(dirname $0)/..

### set the superuser password
echo -n "Enter the superuser password: "
read su_passwd
mkdir -p .su
crypted_passwd=$(openssl passwd -1 $su_passwd)
echo $crypted_passwd > .su/passwd
