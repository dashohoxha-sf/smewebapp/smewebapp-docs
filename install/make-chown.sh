#!/bin/bash

### go to this dir
cd $(dirname $0)

### check that 'chown' does not already exist
if [ -u ../chown ]
then
  owner=$(ls -l ../chown | gawk '{print $3}')
  if [ $owner = "root" ]
  then
    echo "$0: 'chown' is up to date."
    exit
  fi
fi

### Generate the file 'chown.h' which contains 
### MY_UID and APACHE_UID and is used in 'chown.c'

### get my_uid
user=$(whoami)
my_uid=$(sed -n -e "/^$user:/p" /etc/passwd | cut -f 3 -d ':')

### get apache_uid
if    [ -f /etc/redhat-release ]     # RedHat family
then
  apache_uid=$(sed -n -e "/^apache:/p" /etc/group | cut -f 3 -d ':')
elif  [ -f /etc/lsb-release ]        # Debian family
then
  apache_uid=$(sed -n -e "/^www-data:/p" /etc/group | cut -f 3 -d ':')
elif  [ -f /etc/slackware-version ]  # Slackware
then
  apache_uid=$(sed -n -e "/^nobody:/p" /etc/group | cut -f 3 -d ':')
else
  apache_uid=$(sed -n -e "/^nobody:/p" /etc/group | cut -f 3 -d ':')
fi

### use my_uid and apache_uid at 'chown.h'
cat > chown.h << EOF
#define MY_UID      $my_uid
#define APACHE_UID  $apache_uid
EOF

### compile 'chown.c'
gcc -o chown chown.c
mv chown ..

cat <<EOF
Now change ownership of 'chown' to root and set the suid bit (s).
This can be done only by the root, so switch first to root,
and then use these commands:
    bash# chown root.root chown
    bash# chmod +s chown
EOF
