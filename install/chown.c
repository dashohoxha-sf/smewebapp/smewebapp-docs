/**
 * This program is used so that apache can set the owner and group
 * of the files that it creates to WWW_DATA_OWNER and WWW_DATA_GROUP.
 * Normally, only root can change the owner and group of a file, 
 * so, this program should be owned by root and it should have the
 * stiky bit (+s).
 * In order to reduce any security risks, it changes the owner of
 * a file only if it is currently owned by apache (which usually
 * means that it has been created by apache).
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

#include "chown.h"

#define WWW_DATA_OWNER  MY_UID
#define WWW_DATA_GROUP  APACHE_UID

int main(int argc, char**argv)
{
  size_t n=0;
  char *filename;
  struct stat *file_stats;
  uid_t file_uid;

  /* check that it has been called by apache or by me */
  n = getuid();
  //if(n!=APACHE_UID && n!=MY_UID) exit(-1);

  /* check that there is only one argument */
  if (argc!=2) exit(-1);

  //get the filename
  n = strlen(argv[1]);
  filename = malloc(n+1);
  *filename = 0;
  strcat(filename, argv[1]);

  /* check that the file is owned by apache or by me */
  //stat(filename, file_stats);
  //file_uid = file_stats->st_uid;
  //if (file_uid!=APACHE_UID && file_uid!=MY_UID)  exit(-1);

  /* change the file owner */
  return chown(filename, WWW_DATA_OWNER, WWW_DATA_GROUP);
}

