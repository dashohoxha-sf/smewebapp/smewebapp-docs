<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * This file is used to jump directly (cross reference) to a certain section 
 * of a certain book.  It is called with the mode, id of the book, section 
 * and language as parameters in the query string, like this:
 *
 * xref.php?mode/book_id/node_id/lng
 *
 * All the parts are optional and can be ommitted, like this:
 *   xref.php, xref.php?, xref.php?/, xref.php?/book_id,
 *   xref.php?admin/book_id, xref.php?edit/book_id/node_id,
 *   xref.php?approve/book_id//lng, etc.
 *
 * 'mode can have the values 'view', 'edit', 'approve', 'admin',
 * with the default value being 'view'.
 * 
 * It opens first the application as usually, by including index.php,
 * then redirects it automatically to the specified mode, book and section,
 * by calling GoTo().  The second parameter of GoTo() tells it to open
 * 'index.php' (or 'edit.php') next, instead of 'xref.php' (this file). 
 */

/** get the variables in the query string 
 * (the part of url after the question mark (?) ) */
$_ARR_QPARAMS = explode("/", $_SERVER["QUERY_STRING"]);
$mode = $_ARR_QPARAMS[0];
if ($mode=='')  $mode = 'view';

if ($mode!='view')
{
  include_once 'authenticate.php';
  define('EDIT', 'true');
}

//construct the page as usually
include_once "index.php";

//get the parameters of QUERY_STRING
$mode = $_ARR_QPARAMS[0];
if ($mode=='')  $mode = 'view';

$book_id = $_ARR_QPARAMS[1];
if ($book_id=='')  $book_id = 'docbookwiki_guide';

$node_id = (isset($_ARR_QPARAMS[2]) ? $_ARR_QPARAMS[2] : $book_id);
if ($mode=='admin') $node_id = $book_id;

$lng = (isset($_ARR_QPARAMS[3]) ? $_ARR_QPARAMS[3] : '');


//build the event string
$event_args = "book_id=$book_id;node_id=$node_id;lng=$lng;mode=$mode";
$strEvent = "event=main.xref($event_args)";

//redirect to the specified book, node, language and mode
$action = ($mode=='view' ? 'index' : 'edit.php');
print "<script language='javascript'>
 GoTo('main/main.html?$strEvent', '$action');
</script>";
?>