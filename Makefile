
.PHONY: help chown configure install content swish-e search svn downloads \
        set_su_passwd l10n clean

help:
	@install/help.sh

### make content, search, downloads, etc.
#install: l10n set_su_passwd content search svn downloads
install: l10n set_su_passwd content search svn

### make the translation files
l10n:
	@l10n/msgfmt-all.sh

### set the superuser password
set_su_passwd:
	@install/set_su_passwd.sh

### compile 'chown.c'
chown:
	@install/make-chown.sh

### configure the application files
configure: chown
	install/configure.sh

### make content, search, downloads, etc.
#install: set_su_passwd content search svn downloads
install: set_su_passwd content search svn

### generate the content files from initial xml files
content: configure
	content/clean.sh all
	content/make-content.sh

### compile and install the swish-e tool
swish-e:

### index the content for searching
search: swish-e
	@search/make_index.sh

### import the docs in a svn repository
### and check them out in 'downloads/xml_source/'
svn:
	@content/SVN/init.sh
	@content/downloads/checkout_xml_sources.sh

### run make-all-downloads.sh
downloads: svn
	nice content/downloads/make-all-downloads.sh

clean:
	@content/clean.sh all
	-rm install/chown.h chown
