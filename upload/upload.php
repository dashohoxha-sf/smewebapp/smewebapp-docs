<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once '../config/const.Paths.php';
include_once '../config/const.Options.php';
include_once '../global.php';

//load the translations of the messages
$app_name = basename(dirname(dirname(__FILE__)));
$domain = $app_name;
if (LNG != UNDEFINED)  setlocale(LC_MESSAGES, LNG);
if (CODESET != UNDEFINED)  bind_textdomain_codeset($domain, CODESET);
bindtextdomain($domain, 'l10n/');
textdomain($domain);

$error = $_FILES['media_file']['error'];

switch ($error)
{
 case UPLOAD_ERR_INI_SIZE:
   $upload_msg = T_("The uploaded file exceeds the upload_max_filesize directive in php.ini.");
   break;

 case UPLOAD_ERR_FORM_SIZE:
   $upload_msg = T_("The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the html form.");
   break;

 case UPLOAD_ERR_PARTIAL:
   $upload_msg = T_("The uploaded file was only partially uploaded. Please try again.");
   break;

 case UPLOAD_ERR_NO_FILE:
   $upload_msg = T_("No file was uploaded. Please try again.");
   break;

 default:
 case UPLOAD_ERR_OK:
   upload_file();
}

include_once dirname(__FILE__).'/upload_message.html';

function move_file($tmp_file, $dest_file)
{
  global $upload_msg;

  if (!is_uploaded_file($tmp_file))
    {
      $upload_msg = T_("File not uploaded, there is some error.");
      return false;
    }

  $dest_dir = dirname($dest_file);
  shell("mkdir -p $dest_dir");
  shell("mv -f $tmp_file $dest_file");

  if (file_exists($dest_file))
    {
      $upload_msg = T_("File uploaded successfully.");
      return true;
    }
  else
    {
      $upload_msg = T_("File is not uploaded for some reasons.");
      return false;
    }
}

function T_($msgid)
{
  return gettext($msgid);
}
?>