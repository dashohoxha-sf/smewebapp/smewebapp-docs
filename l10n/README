
To translate the messages for a language, first initialize the translation
files:
--scr
bash$ ./xgettext.sh
bash$ ./msginit.sh ll_CC
----
ll_CC is the language and country code, like 'en_US' or 'sq_AL'.


Then, translate the messages in the translation files:
 * ./ll_CC/LC_MESSAGES/book.po
 * ../templates/search/ll_CC/LC_MESSAGES/search.po
 * ../templates/admin/ll_CC/LC_MESSAGES/admin.po
 * ../templates/docbook/ll_CC/LC_MESSAGES/docbook.po


Finally, convert the translation files into binary format (*.mo):
--scr
bash$ ./msgfmt.sh ll_CC
----


To update the translation files (e.g. after a new release is out
and the message strings may have changed), do:
--scr
bash$ ./xgettext.sh
bash$ ./msgmerge.sh ll_CC
----
Then translate any new entries in the translation files, and then
convert again to binary format: `./msgfmt.sh ll_CC`.


Other gettext tools can be used as well, if needed. For more information
about them see `info gettext`.
 