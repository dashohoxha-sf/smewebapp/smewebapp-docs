#!/bin/bash
### convert translation files (*.po) to binary format (*.mo)

### go to this directory
cd $(dirname $0)

if [ "$1" = "" ]
then
  echo "Usage: $0 ll_CC"
  echo "where ll_CC is the language code, like en_US or sq_AL"
  exit 1
fi

lng=$1

### convert the *.po file of the application
dir=$lng/LC_MESSAGES
msgfmt --output-file=$dir/books.mo  $dir/books.po
