<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class search extends WebObject
{
  function init()
    {
      $this->addSVar('expression', '');
      $this->addSVar('all_books', '');
      $this->addSVar('all_langs', '');

      $this->addSVar('recs_per_page', '20');
      $this->addSVar('current_page', '1');
      $this->addSVar('nr_of_recs', '0');
    }

  function on_new_search($event_args)
    {
      $this->setSVar('expression', $event_args['expression']);
      $this->setSVar('current_page', '1');
    }

  function on_help($event_args)
    {
      WebApp::addGlobalVar('help', 'true');
    }

  function on_next($event_args)
    {
      $page = $event_args['page'];
      $this->setSVar('current_page', $page);
    }

  function onParse()
    {
      global $l10n;
      WebApp::addVar('lng', $l10n->lng);      
    }

  function onRender()
    {
      WebApp::addVars($this->get_page_vars());
      $search_results = $this->get_results_rs();
      global $webPage;
      $webPage->addRecordset($search_results);

      $expression = $this->getSVar('expression');
      $expression = str_replace('\"', '"', $expression);
      WebApp::addVar('expression', $expression);
    }

  /** Returns a recordset of search results. */
  function get_results_rs()
    {
      $rs = new EditableRS('search_results');

      $query = $this->get_query();
      if ($query==UNDEFINED)  return $rs;

      //execute the limited search query
      $output_format = "<book_id>::<lng>::<path>::<title>::<swishlastmodified>\n";
      $first = WebApp::getVar('FirstRec');
      $rp = $this->getSVar('recs_per_page');
      $cmd = SWISH_E." -H0 -x '$output_format' -b $first -m $rp $query";
      $results = shell($cmd);
      //print "<xmp>$cmd \n\n$results\n</xmp>";  //debug

      //get a list of books and their titles
      $arr_books = $this->get_book_list();

      //process the search results and fill the recordset
      $lines = explode("\n", $results);
      for ($i=0; $i < sizeof($lines); $i++)
        {
          list($book_id,$lang,$path,$title,$date) = explode('::', $lines[$i]);
          if ($date=='')  continue;

          //change the format of last modified date
          $arr = explode(' ', $date);
          list($year, $month, $day) = explode('-', $arr[0]);
          $date = date('M d, Y', mktime(0, 0, 0, $month, $day, $year));

          $rec = array(
                       'nr'         => $first+$i,
                       'book_id'    => $book_id,
                       'lang'       => $lang,
                       'book_title' => $arr_books[$book_id.':'.$lang],
                       'node_path'  => $path,
                       'node_title' => $title,
                       'date'       => $date,
                       );
          $rs->addRec($rec);
        }
      return $rs;
    }

  /**
   * Read the file books/book_list and return the associative array
   * $arr_books, with keys 'book_id:lng' and with values 'book_title'.
   */
  function get_book_list()
    {
      $arr_books = array();
      $lines = file(CONTENT.'books/book_list');
      for ($i=0; $i < sizeof($lines); $i++)
        {
          $line = $lines[$i];
          $line = trim($line);
          if ($line=='') continue;
          list($book_id, $lng, $book_title) = split(':', $line);
          $book_title = trim($book_title);
          $arr_books[$book_id.':'.$lng] = $book_title;
        }

      return $arr_books;
    }

  function get_query()
    {
      $expression = $this->getSVar('expression');
      if (trim($expression)=='')  return UNDEFINED;

      $all_books = $this->getSVar('all_books');
      $all_langs = $this->getSVar('all_langs');
      if ($all_books != 'checked')
        {
          $book_id = WebApp::getSVar('docbook->book_id');
          $expression = "book_id=$book_id and ($expression)";
        }

      if ($all_langs != 'checked')
        {
          $lng = WebApp::getSVar('docbook->lng');
          $expression = "lng=$lng and $expression";
        }

      $index_files = "search/global.index search/tag.index";
      $query = "-f $index_files -w '$expression'";

      return $query;
    }

  function get_nr_of_recs()
    {
      //If it is the first page, then run the unlimited
      //query, count the results, and save it in a state var.
      //For the other pages, get it from the state var.
      $current_page = $this->getSVar('current_page');
      if ($current_page==1)
        {
          $query = $this->get_query();
          if ($query==UNDEFINED)  return 0;

          $results = shell(SWISH_E." -H0 $query");
          $arr_lines = explode("\n", $results);
          $nr_of_recs = sizeof($arr_lines) - 1;
          $this->setSVar('nr_of_recs', $nr_of_recs);
        }
      else
        {
          $nr_of_recs = $this->getSVar('nr_of_recs');
        }
      return $nr_of_recs;
    }

  function get_page_vars()
    {           
      $current_page = $this->getSVar('current_page');
      $recs_per_page = $this->getSVar('recs_per_page');
      $nr_of_recs = $this->get_nr_of_recs();

      $nr_of_pages = ceil($nr_of_recs / $recs_per_page);
      if ($current_page>$nr_of_pages) $current_page=$nr_of_pages;

      if ($current_page==$nr_of_pages)
        $next_page = "1"; //next page of the last page is the first page
      else
        $next_page = $current_page + 1;

      if ($current_page==1)
        $prev_page = 1; //previous page of the first page is the first page itself
      else
        $prev_page = $current_page - 1;

      $first_rec = 1 + ($current_page - 1)*$recs_per_page;
      if ($first_rec < 1)  $first_rec = 1;
      $last_rec   = $current_page * $recs_per_page;
      if ($first_rec > $nr_of_recs)  $first_rec = $nr_of_recs; 
      if ($last_rec  > $nr_of_recs)  $last_rec  = $nr_of_recs; 

      $page_vars = 
        array(
              //the number of the first record of the page
              "FirstRec" => $first_rec,
              //the number of the last record of the page               
              "LastRec"  => $last_rec,
              //the number of all records that can be retrieved by the query
              "AllRecs"  => $nr_of_recs,
              //current page of records that is retrieved
              "CurrPage" => $current_page,
              //the number of the next page
              "NextPage" => $next_page,
              //the number of the previous page
              "PrevPage" => $prev_page,
              //the number of the last page
              "LastPage" => $nr_of_pages
              );
      return $page_vars;
    }
}
?>