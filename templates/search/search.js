// -*-C-*-
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function set_all_books(chkbox)
{
  var checked = (chkbox.checked ? 'checked' : '');
  session.setVar('search->all_books',  checked);
}
function set_all_langs(chkbox)
{
  var checked = (chkbox.checked ? 'checked' : '');
  session.setVar('search->all_langs',  checked);
}

function new_search()
{
  var form = document.form_new_search;
  var expression = form.expression.value;
  var all_books = (form.all_books.checked ? 'checked' : '');
  var event_args = 'expression=' + expression + ';all_books=' + all_books;
  SendEvent('search', 'new_search', event_args);
}

function close_search()
{
  SendEvent('main', 'docbook');
}

function search_help()
{
  SendEvent('search', 'help');
}

function display(book_id, node_path, lng)
{
  var event_args = 'book_id='+book_id+';node_path='+node_path+';lng='+lng;
  SendEvent('main', 'docbook', event_args);
}
