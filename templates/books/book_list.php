<?php
/**
 * This file contains an array with the list of books and their titles.
 */
$arr_books =
array(
  'smewebapp_manual'  => T_("SMEWebApp"),
  'menushpk_manual'   => T_("MENUshpk"),
  'docbookwiki_guide' => T_("DocBookWiki")
);
?>