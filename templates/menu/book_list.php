<?php
/**
 * This file contains an array with the list of books and their titles.
 */
$arr_books = 
array(
  "docbookwiki_guide" => "Technical Books",
  "test" => "DocBookWiki Guide (test)",
  "slackware_router" => "Slackware Router",
  "linux_server_admin" => "Linux Server Admin",
  "gateway_server" => "A Simple Gateway Server",
  "install_exim" => "Installing Exim",
  "tools_and_apps" => "Tools and Applications",
  "kushtetuta" => "Kushtetuta e Shqiperise",
  "kushtetuta_it" => "La Costituzione Albanese",
  "cat2_sub2_book1" => "Book 1",
  "cat2_sub2_book2" => "Book 2",
  "test1" => "test1",
  "test2" => "test 3",
  "cat2_sub3_book1" => "Book 1",
  "cat2_sub3_book2" => "Book 2",
  "cat3_sub1_book1" => "Book 1",
  "cat3_sub1_book2" => "Book 2",
  "cat3_sub2_book1" => "Book 1",
  "cat3_sub2_book2" => "Book 2",
  "cat4_sub1_book1" => "Book 1",
  "cat4_sub1_book2" => "Book 2",
  "cat4_sub2_book1" => "Book 1",
  "cat4_sub2_book2" => "Book 2",
);
?>