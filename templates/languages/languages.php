<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once TPL.'languages/func.languages.php';

class languages extends WebObject
{
  function init()
    {
      WebApp::addSVar('language', LNG);
      WebApp::addSVar('codeset',  CODESET);
    }

  function on_select($event_args)
    {
      $lng = $event_args['lng'];
      WebApp::setSVar('language', $lng);
      WebApp::setSVar('codeset',  $this->langs[$lng]['codeset']);
    }

  function onParse()
    {
      $lng = WebApp::getSVar('language');
      $codeset = WebApp::getSVar('codeset');
      global $l10n;
      $l10n->set_lng($lng, $codeset);
    }

  function onRender()
    {
      $language = WebApp::getSVar('language');
      $rs = new EditableRS("languages");
      $langs = get_arr_languages();
      while (list($lng, $lng_details) = each($langs))
        {
          $class = ($lng==$language ? 'lang-selected' : 'lang');
          $label = $lng_details['name'];
          $rs->addRec(compact('lng', 'class', 'label'));
        }
     
      global $webPage;
      $webPage->addRecordset($rs);
    }
}
?>