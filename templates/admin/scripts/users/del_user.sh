#!/bin/bash
### delete the record of the given user
username="$1"
users=templates/admin/access_rights/users
sed -i -n "/^$username:/!p" $users
