#!/bin/bash
### get the number of the users of the given book
book_id="$1"
book_access_rights=templates/admin/access_rights/$book_id
ls $book_access_rights | wc -l
