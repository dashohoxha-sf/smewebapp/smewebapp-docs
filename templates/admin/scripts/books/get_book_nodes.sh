#!/bin/bash
### get from the given book index the list of the node_id's

index_xml=$1

path=$(dirname $0)
xsl_file=$path/get_book_nodes.xsl

xsltproc $xsl_file $index_xml
