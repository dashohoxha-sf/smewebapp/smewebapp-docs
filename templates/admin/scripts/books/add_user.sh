#!/bin/bash
### create the access file for the given user in the given book
book_id=$1
username=$2
book_access_rights=templates/admin/access_rights/$book_id

mkdir -p $book_access_rights
touch $book_access_rights/$username
