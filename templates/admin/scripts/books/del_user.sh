#!/bin/bash
### delete the access file of the given user in the given book
book_id=$1
username=$2
book_access_rights=templates/admin/access_rights/$book_id
rm $book_access_rights/$username
