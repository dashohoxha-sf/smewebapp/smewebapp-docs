<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class admin extends WebObject
{
  function init()
    {
      if (SU=='true')
        {
          WebApp::setSVar('tabs1::topmenu->selected_item', 'users');
          $this->addSVar('file', 'users/users.html'); 
        }
      else
        {
          WebApp::setSVar('tabs1::topmenu->selected_item', 'user_data');
          $this->addSVar('file', 'user_data/user_data.html'); 
        }
    }

  function onParse()
    {
      $module = WebApp::getSVar('tabs1::topmenu->selected_item');
      switch ($module)
        {
        case 'addbook':
          $module_title = T_("Import or Create a New Book");
          $file = 'addbook/addbook.html';
          break;
        case 'menu':
          $module_title = T_("Edit Menus");
          $file = 'edit_menu/edit_menu.html';
          break;
        case 'users':
          $module_title = T_("Administrate Users");
          $file = 'users/users.html';
          break;
        case 'books':
          $module_title = T_("Administrate Books");
          $file = 'books/books.html';
          break;
        default:
        case 'user_data':
          $module_title = T_("My Settings");
          $file = 'user_data/user_data.html';
          break;
        }

      WebApp::setSVar('module_title', $module_title);
      $this->setSVar('file', $file); 
    }
}
?>