<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once SCRIPTS.'user_data.php';
include_once FORM_PATH.'formWebObj.php';

/**
 * @package admin
 * @subpackage users
 */
class userEdit extends formWebObj
{
  var $user_data =  array( 
                          'username' => '',
                          'password' => '',
                          'name'     => '',
                          'email'    => '',
                          'books'    => ''
                          );

  function init()
    {
      $this->addSVar('mode', 'add');    // add | edit
    }

  /** add the new user in database */
  function on_add($event_args)
    {
      //they are merged with $this->user_data in order
      //to be sure about the order of the fields
      $user_data = array_merge($this->user_data, $event_args);

      $books = $user_data['books'];
      $user_data['books'] = preg_replace('#\s+#', ',', $books);

      //check that such a username does not exist
      $username = $event_args['username'];
      $arr = get_user_data($username);
      if ($arr['username'] != '')
        {
          WebApp::message($arr['username']);
          $msg = T_("The username 'v_username' is already used for \n\
somebody else.  Please choose another username.");
          $msg = str_replace('v_username', $username, $msg);
          WebApp::message($msg);
          $user_data['username'] = '';
          $this->user_data = $user_data;
          return;
        }

      //if the password is given, set the password of the new user
      $password = $user_data['password'];
      $password = trim($password);
      if ($password!='')
        {
          //encrypt the given password
          srand(time());
          $user_data['password'] = crypt($password, rand());
        }

      //add the user
      save_user_data($user_data);

      //set the new user as current user and change the mode to edit
      WebApp::setSVar('userList->currentUser', $username);
      $this->setSVar('mode', 'edit');
    }

  /** delete the current user */
  function on_delete($event_args)
    {
      $username = WebApp::getSVar('userList->currentUser');
      shell(SCRIPTS."users/del_user.sh $username");

      //the currentUser is deleted,
      //set current the first user in the list
      $userList = WebApp::getObject('userList');
      $userList->selectFirst();

      //acknowledgment message
      $msg = T_("User v_username deleted.");
      $msg = str_replace('v_username', $username, $msg);
      WebApp::message($msg);
    }

  /** save the changes */
  function on_save($event_args)
    {
      //they are merged with $this->user_data in order
      //to be sure about the order of the fields
      $new_user_data = array_merge($this->user_data, $event_args);
      $new_username = $new_user_data['username'];

      //get old user data
      $old_username = WebApp::getSVar('userList->currentUser');
      $old_user_data = get_user_data($old_username);

      //if the password is not given, keep the old password
      $new_password = $new_user_data['password'];
      $new_password = trim($new_password);
      if ($new_password=='')
        {
          //set the old password
          $new_user_data['password'] = $old_user_data['password'];
        }
      else
        {
          //encrypt the given password
          srand(time());
          $new_user_data['password'] = crypt($new_password, rand());
        }

      $books = $new_user_data['books'];
      $new_user_data['books'] = preg_replace('#\s+#', ',', $books);

      if ($new_username != $old_username)
        {
          //check that the new username does not exist
          $user_data = get_user_data($new_username);
          $username = $user_data['username'];
          if ($username != '')
            {
              $msg = T_("The username 'v_new_username' is already in use.");
              $msg = str_replace('v_new_username', $new_username, $msg);
              WebApp::message($msg);
              $new_username = $old_username;
              $new_user_data['username'] = $old_username;
            }
          else
            {
              WebApp::setSVar('userList->currentUser', $new_username);
              shell(SCRIPTS."users/del_user.sh $old_username");
            }
          
        }

      save_user_data($new_user_data);
    }

  function onParse()
    {
      //get the current user from the list of users
      $user = WebApp::getSVar('userList->currentUser');

      if ($user==UNDEFINED)
        {
          $this->setSVar('mode', 'add');
        }
      else
        {
          $this->setSVar('mode', 'edit');
        }

      WebApp::setSVar('edit_rights->user', $user);
    }

  function onRender()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          $user_data = $this->user_data; 
        }
      else
        {
          $username = WebApp::getSVar('userList->currentUser');
          $user_data = get_user_data($username);
        }
      $user_data['books'] = str_replace(',', "\n", $user_data['books'])."\n";
      WebApp::addVars($user_data);      
    }
}
?>