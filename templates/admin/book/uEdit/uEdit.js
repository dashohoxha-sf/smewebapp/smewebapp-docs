// -*-C-*- //tell emacs to use C mode
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function save()
{
  var form = document.uEdit;
  var edit_rights = form.edit_rights.value;
  SendEvent('uEdit', 'save', 'edit_rights='+edit_rights);
}

function add_node(list)
{
  var idx = list.selectedIndex;
  var node_id = list.options[idx].value;
  var form = document.add_rights;

  if (form.nodes.value=='' || form.nodes.value=='ALL' || node_id=='ALL')
    form.nodes.value = node_id;
  else
    form.nodes.value += ',' + node_id;

  list.selectedIndex = 0;
  form.nodes.focus();
}

function add_lng(list)
{
  var idx = list.selectedIndex;
  var lng = list.options[idx].value;
  var form = document.add_rights;

  if (form.langs.value=='' || form.langs.value=='ALL' || lng=='ALL')
    form.langs.value = lng;
  else
    form.langs.value += ',' + lng;

  list.selectedIndex = 0;
  form.langs.focus();
}

function add()
{
  var form = document.add_rights;
  var access, rights;
  var nodes = form.nodes.value;
  var langs = form.langs.value;

  if (form.access[0].checked) access='allow';
  else if (form.access[1].checked) access='deny';
  else
    {
      alert(T_("Please specify either allow or deny."));
      return;
    }

  if (form.edit.checked && form.approve.checked)
    rights = 'edit,approve';
  else if (!form.edit.checked && form.approve.checked)
    rights = 'approve';
  else
    rights = 'edit';

  document.uEdit.edit_rights.value += 
    access + ':' + rights + ':' + nodes + ':' + langs + "\n";
  form.reset();
}

