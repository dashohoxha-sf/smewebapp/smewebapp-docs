<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package admin
 * @subpackage books
 */
class uEdit extends WebObject
{
  /** save the changes */
  function on_save($event_args)
    {
      $edit_rights = $event_args['edit_rights'];

      //ToDo: validate the format
      //...

      //save them
      $username = WebApp::getSVar('uList->currentUser');
      $book_id = WebApp::getSVar('books->selected_book');
      $fname = "templates/admin/access_rights/$book_id/$username";
      write_file($fname, $edit_rights);
    }

  function onRender()
    {
      $this->add_user_data();
      $this->add_edit_rights();
      $this->add_rs_langs();
      $this->add_rs_nodes();
    }

  /** get the user data and add them to the template */
  function add_user_data()
    {
      include_once SCRIPTS.'user_data.php';
      $username = WebApp::getSVar('uList->currentUser');
      $user_data = get_user_data($username);
      unset($user_data['password']);
      WebApp::addVars($user_data);
    }

  function add_edit_rights()
    {
      $username = WebApp::getSVar('uList->currentUser');
      if ($username==UNDEFINED)
        {
          $edit_rights = '';
        }
      else
        {
          //get the user edit rights and add them to the template
          $book_id = WebApp::getSVar('books->selected_book');
          $cmd = SCRIPTS."books/get_user_rights.sh $book_id $username";
          $edit_rights = shell($cmd);
          if (ereg('No such file or directory', $edit_rights)) 
            $edit_rights = '';
        }
      WebApp::addVar('edit_rights', $edit_rights);
    }

  function add_rs_langs()
    {
      $rs = new EditableRS('langs');
      $arr_langs = explode(',', LANGUAGES);
      for ($i=0; $i < sizeof($arr_langs); $i++)
        {
          $lng = $arr_langs[$i];
          $rs->addRec(array('lng'=>$lng));
        }
      global $webPage;
      $webPage->addRecordset($rs);
    }

  function add_rs_nodes()
    {
      //get book nodes
      $book_id = WebApp::getSVar('books->selected_book');
      $arr_langs = $this->get_langs($book_id);
      $lng = $arr_langs[0];
      $index_xml = BOOKS."$book_id/$lng/index.xml";
      $nodes = shell(SCRIPTS."books/get_book_nodes.sh '$index_xml'");

      //fill the recordset
      $rs = new EditableRS('nodes');
      $arr_nodes = explode("\n", $nodes);
      for ($i=0; $i < sizeof($arr_nodes); $i++)
        {
          $node_id = $arr_nodes[$i];
          $rs->addRec(array('node_id'=>$node_id));
        }

      //add the recodset to the page
      global $webPage;
      $webPage->addRecordset($rs);
    }

  /**
   * Returns an array of languages (en,fr,it,al) 
   * in which the book is available.
   */
  function get_langs($book_id)
    {
      $book_path = BOOKS.$book_id.'/';
      if (!file_exists($book_path))  return array();

      $langs = shell("ls $book_path");
      $arr_langs = explode("\n", chop($langs));

      return $arr_langs;
    }
}
?>