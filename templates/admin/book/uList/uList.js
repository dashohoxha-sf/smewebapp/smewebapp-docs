// -*-C-*- //tell emacs to use C mode
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


function select_user(username)
{
  SendEvent('uList', 'select', 'username='+username);
}

function delete_user(username)
{
  SendEvent('uList', 'delete', 'username='+username);
}

function next_page(page)
{
  SendEvent('uList', 'next', 'page='+page);
}

function add_user()
{
  var form = document.adduser;
  var user = form.new_user.value;
  if (user=='')
    {
      alert(T_("Please give a username."));
      form.new_user.focus();
      return;
    }
  SendEvent('uList', 'add_user', 'user='+user);
}

function set_user(list)
{
  var idx = list.selectedIndex;
  var user = list.options[idx].value;
  var form = document.adduser;
  form.new_user.value = user;
  form.new_user.focus();
}
