<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * @package admin
 * @subpackage books
 */
class uList extends WebObject
{
  function init()
    {
      $this->addSVar('recs_per_page', '10');
      $this->addSVar('current_page', '1');

      $this->addSVar('currentUser', UNDEFINED);      
      //set current the first user in the list
      $this->selectFirst();  
    }

  /** set currentUser as the first user in the list */
  function selectFirst()
    {
      $this->add_users_rs();

      global $webPage;
      $rs = $webPage->getRecordset('users');

      $rs->MoveFirst();
      $first_user = ($rs->EOF() ? UNDEFINED : $rs->Field('username'));

      $this->setSVar('currentUser', $first_user);
    }

  function on_next($event_args)
    {
      $page = $event_args["page"];
      $this->setSVar('current_page', $page);
      $this->selectFirst();
    }

  function on_select($event_args)
    {
      $user = $event_args['username'];
      $this->setSVar('currentUser', $user);
    }

  function on_delete($event_args)
    {
      $book_id = WebApp::getSVar('books->selected_book');
      $username = $event_args['username'];
      shell(SCRIPTS."books/del_user.sh $book_id $username");

      //set current the first user in the list
      $this->selectFirst();

      //acknowledgment message
      $msg = T_("User v_username deleted.");
      $msg = str_replace('v_username', $username, $msg);
      WebApp::message($msg);
    }

  function on_add_user($event_args)
    {
      $username = $event_args['user'];
      $book_id = WebApp::getSVar('books->selected_book');
      shell(SCRIPTS."books/add_user.sh $book_id $username");
    }

  function onRender()
    {
      $this->add_users_rs();
      $this->add_all_users_rs();
      WebApp::addVars($this->get_page_vars());
    }

  /** Constructs and adds to the webpage the recordset 
   *  'all_users' with all the users of the site. */
  function add_all_users_rs()
    {
      //get the results
      $results = shell(SCRIPTS.'users/get_all_users.sh');

      //process the results and fill the recordset
      $lines = explode("\n", $results);
      $rs = new EditableRS('all_users');
      for ($i=0; $i < sizeof($lines); $i++)
        {
          $user = trim($lines[$i]);
          if ($user=='')  continue;
          $rs->addRec(array('user'=>$user));
        }

      //add the recordset to the page
      global $webPage;
      $webPage->addRecordset($rs);
    }

  /** Constructs and adds to the webpage the recordset 'users'. */
  function add_users_rs()
    {
      //check that it is not already added
      global $webPage;
      $rs = $webPage->getRecordset('users');
      if ($rs != UNDEFINED)  return;

      //calculate $first and $last records
      $rp = $this->getSVar('recs_per_page');
      $cp = $this->getSVar('current_page');
      $first = 1 + ($cp - 1)*$rp;
      if ($first < 1)  $first = 1;
      $last = $cp * $rp;

      //get the results
      $book_id = WebApp::getSVar('books->selected_book');
      $cmd = SCRIPTS."books/get_user_list.sh \"$book_id\" $first $last";
      $results = shell($cmd);
      //print "<xmp>$cmd \n\n$results\n</xmp>";  //debug

      //process the results and fill the recordset
      $lines = explode("\n", $results);
      $rs = new EditableRS('users');
      for ($i=0; $i < sizeof($lines); $i++)
        {
          $username = trim($lines[$i]);
          if ($username=='')  continue;
          $rs->addRec(array('username'=>$username));
        }

      //add the recordset to the page
      global $webPage;
      $webPage->addRecordset($rs);
    }

  function get_nr_of_recs()
    {
      $book_id = WebApp::getSVar('books->selected_book');
      $str_cnt = shell(SCRIPTS."books/get_user_nr.sh \"$book_id\"");
      $nr_of_recs = 0 + $str_cnt;
      return $nr_of_recs;
    }

  function get_page_vars()
    {           
      $current_page = $this->getSVar('current_page');
      $recs_per_page = $this->getSVar('recs_per_page');
      $nr_of_recs = $this->get_nr_of_recs();

      $nr_of_pages = ceil($nr_of_recs / $recs_per_page);
      if ($current_page > $nr_of_pages)  $current_page=$nr_of_pages;

      if ($current_page==$nr_of_pages)
        $next_page = "1"; //next page of the last page is the first page
      else
        $next_page = $current_page + 1;

      if ($current_page <= 1)
        {
          //previous page of the first page is the first page itself
          $prev_page = $current_page;
        }
      else
        $prev_page = $current_page - 1;

      $first_rec = 1 + ($current_page - 1)*$recs_per_page;
      if ($first_rec < 1)  $first_rec = 1;
      $last_rec   = $current_page * $recs_per_page;
      if ($first_rec > $nr_of_recs)  $first_rec = $nr_of_recs; 
      if ($last_rec  > $nr_of_recs)  $last_rec  = $nr_of_recs; 

      $page_vars = 
        array(
              //the number of the first record of the page
              "FirstRec" => $first_rec,
              //the number of the last record of the page               
              "LastRec"  => $last_rec,
              //the number of all records that can be retrieved by the query
              "AllRecs"  => $nr_of_recs,
              //current page of records that is retrieved
              "CurrPage" => $current_page,
              //the number of the next page
              "NextPage" => $next_page,
              //the number of the previous page
              "PrevPage" => $prev_page,
              //the number of the last page
              "LastPage" => $nr_of_pages
              );
      return $page_vars;
    }
}
?>