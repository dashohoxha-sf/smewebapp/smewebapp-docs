<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class edit_rights extends WebObject
{
  function init()
    {
      $this->addSVar('user', USER);
    }

  function onRender()
    {
      $rs = new EditableRS('edit_rights');
      $accr_path = ADMIN.'access_rights/';
      $user = $this->getSVar('user');
      $output = shell("find $accr_path -name '$user'");
      $arr_files = explode("\n", $output);
      for ($i=0; $i < sizeof($arr_files); $i++)
        {
          $fname = $arr_files[$i];
          if ($fname=='')  continue;

          $rec = array(
                       'book_id' => basename(dirname($fname)), 
                       'edit_rights' => implode('', file($fname))
                       );
          $rs->addRec($rec);
        }
      global $webPage;
      $webPage->addRecordset($rs);
    }
}
?>