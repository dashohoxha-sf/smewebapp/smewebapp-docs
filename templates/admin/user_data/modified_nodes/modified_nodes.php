<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class modified_nodes extends WebObject
{
  var $arr_books_admin = array();

  function modified_nodes()
    {
      //get the books for which the user is admin
      $record = shell(SCRIPTS.'users/get_user.sh '.USER);
      $arr_fields = explode(':', $record);
      $book_list = $arr_fields[4];
      $this->arr_books_admin = explode(',', $book_list);
    }

  /**
   * First find the books where the user has some access rights.
   * Then find the modified nodes in each of these books.
   * Next, check whether the user has approve rights on these
   * modified nodes, and if yes, add them to the recordset 'modified_nodes'.
   */
  function onRender()
    {
      $rs = new EditableRS('modified_nodes');

      //find the books where the user has some access rights
      $accr_path = ADMIN.'access_rights/';
      $user = USER;
      $output = shell("find $accr_path -name '$user'");
      $arr_files = explode("\n", $output);
      for ($i=0; $i < sizeof($arr_files); $i++)
        {
          $fname = $arr_files[$i];
          if ($fname=='')  continue;

          $book_id = basename(dirname($fname));
          $arr_access_rights = file($fname);
          $this->add_modified_nodes($rs, $book_id, $arr_access_rights);
        }
      global $webPage;
      $webPage->addRecordset($rs);
    }

  /**
   * Add to recordset the modified nodes of the given book
   * that can be approved by the user.
   */
  function add_modified_nodes(&$rs, $book_id, $arr_access_rights)
    {
      //see whether the user is admin of this book
      $is_admin = in_array($book_id, $this->arr_books_admin);

      //get the modified nodes of the book
      $book_path = WS_BOOKS.$book_id.'/';
      $cmd = "find $book_path -name state_*.txt | xargs grep ^modified";
      $output = shell($cmd);

      $lines = explode("\n", $output);
      for ($i=0; $i < sizeof($lines); $i++)
        {
          $line = $lines[$i];
          if ($line=='')  continue;

          $arr = explode(':', $line);
          $state_file = $arr[0];
          ereg('state_(.+)\.txt$', $state_file, $args);
          $lng = $args[1];
          $node_path = ereg_replace('state_.+\.txt$', '', $state_file);
          $node_path = ereg_replace('^'.$book_path, '', $node_path);

          //add the modified node to recordset, if the user can approve it
          if ($is_admin or $this->has_approve_right($node_path, $lng,
                                                    $arr_access_rights))
            {
              $node_title = $this->get_title($book_id, $node_path, $lng);
              $rec = compact('book_id', 'node_path', 'lng', 'node_title');
              $rs->addRec($rec);
            }
        }
    }

  function get_title($book_id, $node_path, $lng)
    {
      $cache_path = WS_CACHE.$book_id.'/';
      $navigation_file = $cache_path.$node_path."navigation_$lng.txt";
      $line = shell("grep full_title $navigation_file");
      $arr = split('=', chop($line), 2);
      $title = $arr[1];
      if ($title=='')  $title = 'Table Of Contents';
      return $title;
    }

  /** Returns true if the user has approve right on the given node. */
  function has_approve_right($node_path, $lng, $arr_access_rights)
    {
      //try to match the given node and language with the access rights
      $approve = false;
      for ($i=0; $i < sizeof($arr_access_rights); $i++)
        {
          $line = chop($arr_access_rights[$i]);
          list($access,$levels,$nodes,$langs) = explode(':', $line);

          //if this line matches the given node and language 
          //then set the value of approve according to it
          if ($this->node_match($node_path, $nodes)
              and $this->lang_match($lng, $langs))
            {
              $arr_levels = explode(',', $levels);
              if (in_array('approve', $arr_levels))
                {
                  $approve = ($access=='allow' ? true : false);
                }
            }
        }
      return $approve;
    }

  /**
   * Returns true if one of the node_path expressions in the list
   * matches the given node path, otherwise returns false.
   * The node_list is a comma separated list of node_path expressions
   * (which are actually regular expressions), or is the string 'ALL'.
   * 'ALL' matches any node path.
   */
  function node_match($node_path, $node_list)
    {
      if (strtoupper($node_list)=='ALL')  return true;

      $arr_nodes = explode(',', $node_list);
      for ($i=0; $i < sizeof($arr_nodes); $i++)
        {
          $expr = $arr_nodes[$i];
          if (ereg('^'.$expr, $node_path)) return true;
        }
    }

  /**
   * Returns true if one of the language ids in the list matches
   * the given language, otherwise returns false. The lang_list
   * is a comma separated list of languages, or 'ALL'.
   * 'ALL' matches any language.
   */
  function lang_match($lng, $lang_list)
    {
      if (strtoupper($lang_list)=='ALL')  return true;

      $arr_langs = explode(',', $lang_list);
      $match = in_array($lng, $arr_langs);

      return $match;
    }
}
?>