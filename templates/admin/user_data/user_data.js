// -*-C-*- //tell emacs to use C mode
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function change_passwd()
{
  var form = document.passwd;
  var new_password_1 = form.new_password_1.value;
  var new_password_2 = form.new_password_2.value;

  //check that New Password and Confirm Password are equal
  if (new_password_1 != new_password_2)
    {
      alert(T_("Passwords do not match. Please try again."));
      form.new_password_1.value = '';
      form.new_password_2.value = '';
      form.new_password_1.focus();
      return;
    }

  var event_args = ('new_password=' + new_password_1);
  SendEvent('user_data', 'change_passwd', event_args);
}

function save()
{
  var form = document.profile;
  var name  = form.name.value;
  var email = form.email.value;

  name = name.replace(/:/, '');
  email = email.replace(/:/, '');
  var event_args = ('name=' + name + ';' + 'email=' + email);
  SendEvent('user_data', 'save', event_args);
}
