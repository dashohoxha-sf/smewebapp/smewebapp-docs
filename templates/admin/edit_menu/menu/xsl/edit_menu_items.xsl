<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This  file is  part  of SMEWebApp.   SMEWebApp  is a  web application  that
displays and edits DocBook documents.

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free software; you  can redistribute it and/or modify it under
the  terms of  the GNU  General  Public License  as published  by the  Free
Software Foundation; either  version 2 of the License,  or (at your option)
any later version.

SMEWebApp is  distributed in the hope  that it will be  useful, but WITHOUT
ANY  WARRANTY; without  even  the implied  warranty  of MERCHANTABILITY  or
FITNESS FOR A  PARTICULAR PURPOSE.  See the GNU  General Public License for
more details.

You should  have received a  copy of the  GNU General Public  License along
with SMEWebApp;  if not,  write to the  Free Software Foundation,  Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="text" indent="no" encoding="iso-latin-1" />


<!-- root element -->
<xsl:template match="menu">
<xsl:text>// -*-C-*- //tell emacs to use the C mode

var MENU_ITEMS = 
[
</xsl:text>
<xsl:apply-templates select="./item">
  <xsl:with-param name="indent" select="'  '" />
</xsl:apply-templates>
<xsl:text>];</xsl:text>
</xsl:template>


<!-- match item -->
<xsl:template match="item">
  <xsl:param name="indent" select="''" />

  <xsl:variable name="link">
    <xsl:text>'javascript:edit(\'</xsl:text>
    <xsl:value-of select="@id" />    
    <xsl:text>\')'</xsl:text>
  </xsl:variable>
  
  <xsl:value-of select="$indent" />
  <xsl:text>['</xsl:text>
  <xsl:value-of select="@caption"/>
  <xsl:text>', </xsl:text>
  <xsl:value-of select="$link"/>
  <xsl:text>, null</xsl:text>

  <xsl:choose>
    <xsl:when test="./item">
      <xsl:text>,
</xsl:text>
      <xsl:apply-templates select="./item">
        <xsl:with-param name="indent" select="concat('  ', $indent)" />
      </xsl:apply-templates>
      <xsl:value-of select="$indent" /><xsl:text> ],
</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>],
</xsl:text>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

</xsl:transform>
