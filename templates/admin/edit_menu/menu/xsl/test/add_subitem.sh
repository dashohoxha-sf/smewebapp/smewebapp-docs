#!/bin/bash

id=technical
new_bookid=test1
new_caption='Test 1'
xsl_file=../add_subitem.xsl
xml_file=../../menu.xml

xsltproc  --stringparam id "$id" \
          --stringparam new_bookid "$new_bookid" \
          --stringparam new_caption "$new_caption" \
          $xsl_file  $xml_file
