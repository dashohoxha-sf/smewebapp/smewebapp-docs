<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This  file is  part  of SMEWebApp.   SMEWebApp  is a  web application  that
displays and edits DocBook documents.

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free software; you  can redistribute it and/or modify it under
the  terms of  the GNU  General  Public License  as published  by the  Free
Software Foundation; either  version 2 of the License,  or (at your option)
any later version.

SMEWebApp is  distributed in the hope  that it will be  useful, but WITHOUT
ANY  WARRANTY; without  even  the implied  warranty  of MERCHANTABILITY  or
FITNESS FOR A  PARTICULAR PURPOSE.  See the GNU  General Public License for
more details.

You should  have received a  copy of the  GNU General Public  License along
with SMEWebApp;  if not,  write to the  Free Software Foundation,  Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!-- 
This transformation moves up the item with the given id.
It is called with a parameter 'id', like this: 
xsltproc -stringparam id $id  move_up.xsl menu.xml
-->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" version="1.0" encoding="iso-latin-1" 
            omit-xml-declaration="no" standalone="no" indent="yes" />

<xsl:template match="item">
  <xsl:choose>

    <!-- if the item next to this one is the selected one, -->
    <!-- then copy the next item instead of this one       -->
    <xsl:when test="following-sibling::item[1]/@id=$id">
      <xsl:copy-of select="following-sibling::item[1]" />
    </xsl:when>

    <!-- if this item is the selected one and there exists a previous -->
    <!-- item, then copy the previous item instead of this one        -->
    <xsl:when test="@id=$id and preceding-sibling::item">
      <xsl:copy-of select="preceding-sibling::*[1]" />
    </xsl:when>

    <!-- otherwise copy this item and apply templates -->
    <xsl:otherwise>
      <xsl:copy>
        <xsl:copy-of select="@*" />
        <xsl:apply-templates />
      </xsl:copy>
    </xsl:otherwise>

  </xsl:choose>
</xsl:template>

<!-- copy everything else -->
<xsl:template match="*|@*">
  <xsl:copy>
    <xsl:apply-templates select="@*" />
    <xsl:apply-templates select="node()" />
  </xsl:copy>
</xsl:template>

</xsl:transform>
