<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This  file is  part  of SMEWebApp.   SMEWebApp  is a  web application  that
displays and edits DocBook documents.

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free software; you  can redistribute it and/or modify it under
the  terms of  the GNU  General  Public License  as published  by the  Free
Software Foundation; either  version 2 of the License,  or (at your option)
any later version.

SMEWebApp is  distributed in the hope  that it will be  useful, but WITHOUT
ANY  WARRANTY; without  even  the implied  warranty  of MERCHANTABILITY  or
FITNESS FOR A  PARTICULAR PURPOSE.  See the GNU  General Public License for
more details.

You should  have received a  copy of the  GNU General Public  License along
with SMEWebApp;  if not,  write to the  Free Software Foundation,  Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!-- 
This transformation updates the id and caption of the selected item.
It is called with parameters 'id', 'new_id', 'new_caption', like this: 
xsltproc -stringparam id $id \
         -stringparam bookid $bookid \
         -stringparam caption $caption \
         update.xsl menu.xml
-->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" version="1.0" encoding="iso-latin-1" 
            omit-xml-declaration="no" standalone="no" indent="yes" />


<!-- match the item with the given id and modify its attributes -->
<xsl:template match="*">
  <xsl:choose>
    <xsl:when test="@id=$id">
      <item id="{@id}" caption="{$caption}">
        <xsl:if test="not($bookid='')">
          <xsl:attribute name="bookid">
            <xsl:value-of select="$bookid" />
          </xsl:attribute>
        </xsl:if>
        <xsl:if test="./item">
          <xsl:apply-templates />
        </xsl:if>
      </item>
    </xsl:when>

    <xsl:otherwise> <!-- copy -->
      <xsl:copy>
        <xsl:copy-of select="@*" />
        <xsl:apply-templates />
      </xsl:copy>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<!-- ignore text nodes -->
<xsl:template match="text()" />

</xsl:transform>
