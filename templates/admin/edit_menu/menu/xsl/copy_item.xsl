<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This  file is  part  of SMEWebApp.   SMEWebApp  is a  web application  that
displays and edits DocBook documents.

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free software; you  can redistribute it and/or modify it under
the  terms of  the GNU  General  Public License  as published  by the  Free
Software Foundation; either  version 2 of the License,  or (at your option)
any later version.

SMEWebApp is  distributed in the hope  that it will be  useful, but WITHOUT
ANY  WARRANTY; without  even  the implied  warranty  of MERCHANTABILITY  or
FITNESS FOR A  PARTICULAR PURPOSE.  See the GNU  General Public License for
more details.

You should  have received a  copy of the  GNU General Public  License along
with SMEWebApp;  if not,  write to the  Free Software Foundation,  Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!-- 
This transformation copies an item as a child of another item.
It is called with parameters 'id' and 'copy_id' like this: 
xsltproc -stringparam id $id \
         -stringparam copy_id $copy_id \
         copy_item.xsl menu.xml
where 'copy_id' is the id of the item that will be copied
and 'id' is the id of the item where it will be copied.
The copy is deep (e.g. the whole branch will be copied).
-->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" version="1.0" encoding="iso-latin-1" 
            omit-xml-declaration="no" standalone="no" indent="yes" />


<!-- match the item with the given id and add a new subitem -->
<xsl:template match="menu | item">
  <xsl:choose>
    <xsl:when test="@id=$id">
      <xsl:copy>
        <xsl:copy-of select="@*" />

        <!-- copy the subitems -->
        <xsl:apply-templates />

        <!-- append the branch that is being copied -->
        <xsl:apply-templates select="//item[@id=$copy_id]" mode="copy" />
      </xsl:copy>
    </xsl:when>

    <xsl:otherwise>
      <xsl:copy>
        <xsl:copy-of select="@*" />
        <xsl:apply-templates />
      </xsl:copy>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<!-- generate a new id for the items that are being copied -->
<xsl:template match="@id" mode="copy">
  <xsl:attribute name="id">
    <xsl:value-of select="generate-id(.)" />
  </xsl:attribute>
</xsl:template>


<!-- copy everything else from the branch that is being copied -->
<xsl:template match="*|@*" mode="copy">
  <xsl:copy>
    <xsl:apply-templates select="@*" mode="copy" />
    <xsl:apply-templates select="node()" mode="copy" />
  </xsl:copy>
</xsl:template>


<!-- ignore text nodes -->
<xsl:template match="text()" />
<xsl:template match="text()" mode="copy" />

</xsl:transform>
