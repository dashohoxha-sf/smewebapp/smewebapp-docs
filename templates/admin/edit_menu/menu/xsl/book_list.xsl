<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This  file is  part  of SMEWebApp.   SMEWebApp  is a  web application  that
displays and edits DocBook documents.

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

SMEWebApp is free software; you  can redistribute it and/or modify it under
the  terms of  the GNU  General  Public License  as published  by the  Free
Software Foundation; either  version 2 of the License,  or (at your option)
any later version.

SMEWebApp is  distributed in the hope  that it will be  useful, but WITHOUT
ANY  WARRANTY; without  even  the implied  warranty  of MERCHANTABILITY  or
FITNESS FOR A  PARTICULAR PURPOSE.  See the GNU  General Public License for
more details.

You should  have received a  copy of the  GNU General Public  License along
with SMEWebApp;  if not,  write to the  Free Software Foundation,  Inc., 59
Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="text" indent="no" encoding="iso-latin-1" />

<xsl:template match="menu">
<xsl:text>&lt;?php
/**
 * This file contains an array with the list of books and their titles.
 */
$arr_books = 
array(
</xsl:text>
<xsl:apply-templates select="./item" />
<xsl:text>);
?&gt;</xsl:text>
</xsl:template>


<!-- match only items that have a bookid attribute -->
<xsl:template match="item[@bookid]">

  <!-- get the count of the book -->
  <xsl:variable name="bookid"><xsl:value-of select="@bookid" /></xsl:variable>
  <xsl:variable name="count">
    <xsl:number level="any" count="item[@bookid=$bookid]" />
  </xsl:variable>

  <!-- don't list a book multiple times -->
  <xsl:if test="$count=1">
    <xsl:text>  "</xsl:text>
    <xsl:value-of select="@bookid" />
    <xsl:text>" => "</xsl:text>
    <xsl:value-of select="@caption" />
    <xsl:text>",
</xsl:text>
  </xsl:if>

  <!-- continue recursivly -->
  <xsl:apply-templates />

</xsl:template>

<!-- ignore text nodes -->
<xsl:template match="text()" />

</xsl:transform>
