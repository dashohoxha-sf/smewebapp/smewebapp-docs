<?php
class edit_menu extends WebObject
{
  function init()
    {
      //the menu item that is being edited, initially the root
      $this->addSVar('item_id', 'menu');
      $this->addSVar('item_bookid', '');
      $this->addSVar('item_caption', '');

      //keeps the items that are copied and can be pasted somewhere else
      $this->addSVar('clipboard', '');
    }

  /** select an item for editing */
  function on_select($event_args)
    {
      $item_id = $event_args['item_id'];
      $this->select_item($item_id);
    }

  /**
   * Get the 'bookid' and the 'caption' of the selected item
   * and refresh the state variables.
   */
  function select_item($item_id)
    {
      //get the bookid and the caption of the selected item
      $output = $this->transform('get_bookid.xsl', array('id'=>$item_id));
      list($bookid, $caption) = split(' ', $output, 2);

      //set state variables
      $this->setSVar('item_id', $item_id);
      $this->setSVar('item_bookid', $bookid);
      $this->setSVar('item_caption', $caption);
    }

  function transform_menu($transformer, $arr_params =array())
    {
      //apply the transformer and get the new menu
      $new_menu = $this->transform($transformer, $arr_params);

      //save the transformed menu
      $menu_path = ADMIN.'edit_menu/menu/';
      write_file($menu_path.'menu.xml', $new_menu);
    }

  /**
   * Applies the given xsl transformer to menu.xml and returns 
   * the result. $arr_params is an associative array of parameters. 
   */
  function transform($transformer, $arr_params =array())
    {
      //construct the string $params
      $params = '';
      while (list($p_name, $p_value) = each($arr_params))
        {
          $params .= "--stringparam $p_name \"$p_value\" ";
        }

      //apply the $transformer with $params to menu.xml
      $menu_path = ADMIN.'edit_menu/menu/';
      $menu_xml = $menu_path.'menu.xml';
      $xsl_file = $menu_path."xsl/$transformer";
      $result = shell("xsltproc $params $xsl_file $menu_xml");
      //print "<xmp>$result</xmp>\n";

      return $result;
    }

  /** save modifications in id and caption of the current item */
  function on_update($event_args)
    {
      $params = $event_args;
      $item_id = $this->getSVar('item_id');
      $params['id'] = $item_id;
      $this->transform_menu('update.xsl', $params);
      $this->select_item($item_id);
    }

  /** delete the current item and set the parent item as the current one */
  function on_delete($event_args)
    {
      $params['id'] = $this->getSVar('item_id');
      $parent_id = $this->transform('get_parent_id.xsl', $params);
      $this->transform_menu('delete.xsl', $params);
      $this->select_item($parent_id);
    }

  /** move up the given item */
  function on_move_up($event_args)
    {
      $params['id'] = $event_args['item_id'];
      $this->transform_menu('move_up.xsl', $params);
    }

  /** move down the given item */
  function on_move_down($event_args)
    {
      $params['id'] = $event_args['item_id'];
      $this->transform_menu('move_down.xsl', $params);
    }

  /** add a new subitem to the current item */
  function on_add_subitem($event_args)
    {
      $params = $event_args;
      $params['id'] = $this->getSVar('item_id');
      $this->transform_menu('add_subitem.xsl', $params);
    }

  /** add any items in clipboard as subitems of the current item */
  function on_paste($event_args)
    {
      $clipboard = $this->getSVar('clipboard');
      $clipboard_items = explode("\n", $clipboard);

      //remove any doublicated items in clipboard
      $arr_items = array();
      for ($i=0; $i < sizeof($clipboard_items); $i++)
        {
          $item = trim($clipboard_items[$i]);
          if ($item=='')  continue;
          if (in_array($item, $arr_items))  continue;
          $arr_items[] = $item;
        }

      //add the items of the clipboard as subitems
      $item_id = $this->getSVar('item_id');
      for ($i=0; $i < sizeof($arr_items); $i++)
        {
          $item = $arr_items[$i];
          list($copy_id, $bookid, $caption) = split(':', $item, 3);

          //add the subitem
          $params['id'] = $item_id;
          $params['copy_id'] = $copy_id;
          $this->transform_menu('copy_item.xsl', $params);
        }
    }

  /** apply the modifications to the main menu */
  function on_apply($event_args)
    {
      //update menu.xml
      $menu_xml = MENU.'menu.xml';
      $edit_menu_xml = ADMIN.'edit_menu/menu/menu.xml';
      shell("cp $edit_menu_xml $menu_xml");

      //update menu_items.js
      $menu_items = $this->transform('menu_items.xsl');
      write_file(MENU.'menu_items.js', $menu_items);

      //update book_list.php
      $book_list = $this->transform('book_list.xsl');
      write_file(MENU.'book_list.php', $book_list);

      //select the root item
      $this->select_item('menu');
    }

  /** discard any modifications and get a copy of the main menu */
  function on_cancel($event_args)
    {
      $menu_xml = MENU.'menu.xml';
      $edit_menu_xml = ADMIN.'edit_menu/menu/menu.xml';
      shell("cp $menu_xml $edit_menu_xml");

      //select the root item
      $this->select_item('menu');
    }

  function onRender()
    {
      $this->add_subitems_rs();
      $this->add_books_rs();
    }

  /** add a recordset of the subitems of the selected item */
  function add_subitems_rs()
    {
      //get the subitems
      $item_id = $this->getSVar('item_id');
      $items = $this->transform('subitems.xsl', array('id'=>$item_id));
      $arr_lines = explode("\n", $items);

      //create a recordset with id-s and captions of the subitems
      $rs = new EditableRS('subitems');
      for ($i=0; $i < sizeof($arr_lines); $i++)
        {
          list($id, $bookid, $caption) = split(' ', $arr_lines[$i], 3);
          if ($id=='')  continue;
          $rs->addRec(compact('id', 'bookid', 'caption'));
        }
      global $webPage;
      $webPage->addRecordset($rs);
    }

  /**
   * Add a recordset of all the books,
   * which will be displayed in a listbox.
   */
  function add_books_rs()
    {
      //books
      $rs = new EditableRS('booklist');
      $rs->addRec(array('id'=>'', 'label'=>'----- '.T_("Select").' -----'));
      $output = shell('ls '.BOOKS);
      $arr_lines = explode("\n", $output);
      for ($i=0; $i < sizeof($arr_lines); $i++)
        {
          $book_id = trim($arr_lines[$i]);
          if ($book_id=='')  continue;
          $rs->addRec(array('id' => $book_id, 'label' => $book_id));
        }
      global $webPage;
      $webPage->addRecordset($rs);      
    }
}
?>