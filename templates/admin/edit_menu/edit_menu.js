// -*-C-*-
/*
This file is  part of SMEWebApp.  SMEWebApp is  a web application that
helps the informatization of small and medium enterprises.

Copyright 2003, 2004 Dashamir Hoxha, dhoxha@inima.al

SMEWebApp is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

SMEWebApp  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with SMEWebApp;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function apply()
{
  var msg = T_("This will copy the modified menu to the main menu.");
  if (confirm(msg))
    {
      SendEvent('edit_menu', 'apply');
    }
}

function cancel()
{
  var msg = T_("This will get a fresh copy of the main menu, discarding any modifications");
  if (confirm(msg))
    {
      SendEvent('edit_menu', 'cancel');
    }
}

function update()
{
  var form = document.edit_menu_item;
  var bookid = form.item_bookid.value;
  var caption = form.item_caption.value;

  if (caption=='')
    {
      alert(T_("Caption cannot be empty."));
      form.item_caption.focus();
      return;
    }

  var event_args = 'bookid='+bookid+';caption='+caption;
  SendEvent('edit_menu', 'update', event_args);
}

function del()
{
  var msg = T_("You are deleting this item and all its subitems.");
  if (confirm(msg))
    {
      SendEvent('edit_menu', 'delete');
    }
}

function move_up(item_id)
{
  SendEvent('edit_menu', 'move_up', 'item_id='+item_id);
}

function move_down(item_id)
{
  SendEvent('edit_menu', 'move_down', 'item_id='+item_id);
}

function add_subitem()
{
  var form = document.new_menu_item;
  var new_bookid = form.new_bookid.value;
  var new_caption = form.new_caption.value;

  if (new_caption=='')
    {
      alert(T_("Please fill the Caption field."));
      form.new_caption.focus();
      return;
    }

  var event_args = 'new_bookid='+new_bookid+';new_caption='+new_caption;
  SendEvent('edit_menu', 'add_subitem', event_args);
}

function copy_to_cb()
{
  var id = session.getVar('edit_menu->item_id');
  var bookid = session.getVar('edit_menu->item_bookid');
  var caption = session.getVar('edit_menu->item_caption');
  var item = id+':' + bookid + ':' + caption;
  var clipboard = session.getVar('edit_menu->clipboard');
  session.setVar('edit_menu->clipboard', clipboard + item + '\n');

  //display an notification message
  var msg = T_("Item 'v_item' appended to clipboard.");
  item = item.replace(/^[^:]*:/, '');
  msg = msg.replace(/v_item/, item);
  alert(msg);
}

function paste_from_cb()
{
  var clipboard = session.getVar('edit_menu->clipboard');
  if (clipboard=='')
    {
      alert(T_("Clipboard is empty!"));
      return;
    }

  SendEvent('edit_menu', 'paste');
}

function show_cb()
{
  var clipboard = session.getVar('edit_menu->clipboard');
  clipboard = clipboard.replace(/^[^:]*:/, '');
  clipboard = clipboard.replace(/\n[^:]*:/g, '\n');
  if (clipboard=='')
    alert(T_("Clipboard is empty."));
  else
    alert(clipboard);
}

function empty_cb()
{
  session.setVar('edit_menu->clipboard', '');
  alert(T_("Clipboard is now empty."));
}

function select_book(listbox)
{
  //get the selected book id
  var idx = listbox.selectedIndex;
  var book_id = listbox.options[idx].value;

  //set it to the field new_bookid of the form
  document.new_menu_item.new_bookid.value = book_id;
  document.new_menu_item.new_caption.value = book_id;
}
