<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class books extends WebObject
{
  function init()
    {
      $arr_book_list = $this->get_book_list_arr();
      $this->addSVar('selected_book', $arr_book_list[0]);
    }

  function on_select($event_args)
    {
      $book_id = $event_args['book_id'];
      $this->setSVar('selected_book', $book_id);
    }

  function onRender()
    {
      $this->add_book_list_rs();

      $book_id = $this->getSVar('selected_book');
      include TPL.'books/book_list.php';
      WebApp::addVar('book_title', $arr_books[$book_id]);
    }

  function add_book_list_rs()
    {
      //'book_list.php' contains $arr_books
      include TPL.'books/book_list.php';

      $rs = new EditableRS('book_list');
      $arr_book_list = $this->get_book_list_arr();
      while (list($book_id,$book_title) = each($arr_books))
        {
          if (in_array($book_id, $arr_book_list))
            {
              $rec = array('id'=>$book_id, 'label'=>$book_title);
              $rs->addRec($rec);
            }
        }
      global $webPage;
      $webPage->addRecordset($rs);
    }

  function get_book_list_arr()
    {
      if (SU=='true')
        {
          //'book_list.php' contains $arr_books
          include TPL.'books/book_list.php';
          $arr = array_keys($arr_books);
        }
      else
        {
          $record = shell(SCRIPTS.'users/get_user.sh '.USER);
          $fields = explode(':', $record);
          $book_list =  $fields[4];
          $arr = explode(',', $book_list);
        }

      return $arr;
    }
}
?>