// -*-C-*- //tell emacs to use C mode
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function create()
{
  var form1 = document.bookid;
  var bookid = form1.bookid.value;
  var lng = form1.lng.value;

  var form = document.create;
  var author = form.author.value;
  var keywords = form.keywords.value;
  var doc_title = form.doc_title.value;
  var doc_abstract = form.doc_abstract.value;

  var doctype;
  if (form.doctype[0].checked)  doctype = form.doctype[0].value;
  else if (form.doctype[1].checked)  doctype = form.doctype[1].value;
  else doctype = '';

  if (bookid=='')
    {
      alert(T_("Please give book id."));
      form1.bookid.focus();
      return;
    }

  if (doctype=='')
    {
      alert(T_("Please select either book or article."));
      return;
    }

  var event_args = new Array();
  event_args.push('bookid='+bookid);
  event_args.push('lng='+lng);
  event_args.push('doctype='+doctype);
  event_args.push('author='+author);
  event_args.push('keywords='+keywords);

  //abstract and title can possibly contain semicolumns (;), 
  //this is why it should be encoded
  event_args.push('title='+encode_arg_value(doc_title));
  event_args.push('abstract='+encode_arg_value(doc_abstract));

  SendEvent('addbook', 'create', event_args.join(';'));
}

function del_book()
{
  var form = document.delete_book;
  var book_id = form.del_bookid.value;
  var lng = form.del_lng.value;
  var msg;

  if (book_id=='all')
    {
      msg = T_("You are deleting all the books!");
      if (!confirm(msg)) return;
    }
  else if (lng=='')
    {
      msg = T_("You are deleting the book 'v_book_id' (all languages)!");
      msg = msg.replace(/v_book_id/, book_id);
      if (!confirm(msg)) return;
    }
  else
    {
      msg = T_("You are deleting the book 'v_book_id (v_lng)'!");
      msg = msg.replace(/v_book_id/, book_id);
      msg = msg.replace(/v_lng/, lng);
      if (!confirm(msg)) return;
    }

  var event_args = 'book_id=' + book_id + ';lng=' + lng;
  SendEvent('addbook', 'delete', event_args);
}
