<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class import_doc extends WebObject
{
  /** refresh the list after uploading a file */
  function on_refresh($event_args)
    {
      sleep(1);
    }

  /** delete an uploaded file */
  function on_delete($event_args)
    {
      $fname = $event_args['fname'];
      $file = CONTENT."initial_xml/uploaded/$fname";
      shell("rm $file");
    }

  function on_import($event_args)
    {
      $book_id = $event_args['book_id'];
      $lng = $event_args['lng'];
      if ($lng=='')  $lng = 'en';
      $fname = $event_args['fname'];
 
      //get xml file to be uploaded
      $xml_file = "initial_xml/uploaded/$fname";

      //ToDo: check for any xml errors
      //....

      //import
      $output = shell("content/import.sh $xml_file $book_id $lng");
      WebApp::debug_msg("<xmp>$output</xmp>");
    }

  function onRender()
    {
      $this->add_uploaded_files_rs();
    }

  /** create and add the recordset 'uploaded_files' */
  function add_uploaded_files_rs()
    {
      $rs = new EditableRS('uploaded_files');

      $path = CONTENT.'initial_xml/uploaded/';
      if (file_exists($path))  $dir = opendir($path);
      if ($dir) 
        {
          while (($fname=readdir($dir)) !== false)
            { 
              if ($fname=='.' or $fname=='..' or $fname=='CVS')  continue;

              $rs->addRec(array('fname'=>$fname));
            }
          closedir($dir); 
        }

      global $webPage;
      $webPage->addRecordset($rs);
    }
}
?>