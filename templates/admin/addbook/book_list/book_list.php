<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class book_list extends WebObject
{
  function onRender()
    {
      $this->add_recordsets();
    }

  function add_recordsets()
    {
      global $webPage;

      //the number of columns/chunks
      $nr_cols = 3; 

      //add a recordset for the columns
      $rs_cols = new EditableRS("cols");

      //get the list of books
      $fname = CONTENT.'books/book_list';
      $arr_lines = file($fname);
      $nr_chunks = sizeof($arr_lines) / $nr_cols + 1;
      $arr_chunks = array_chunk($arr_lines, $nr_chunks);

      //create, fill and add a recordset for each column/chunk
      for ($i=0; $i < sizeof($arr_chunks); $i++)
        {
          //create a new recordset for the chunk
          $rs = new EditableRS("books_$i");

          //add the items of the chunk in the recordset
          $chunk = $arr_chunks[$i];
          for ($j=0; $j < sizeof($chunk); $j++)
            {
              $line = $chunk[$j];
              if (trim($line)=='')  continue;

              //get book_id, lng and title of the book
              list($book_id, $lng, $title) = split(':', $line, 3);
              $title = trim($title);

              //add a record for this book
              $rs->addRec(compact('book_id', 'lng', 'title'));
            }

          //add the recordset to the webPage
          $webPage->addRecordset($rs);

          //add a record for this column/chunk
          $rs_cols->addRec(array('nr'=> $i));
        }

      //add the column recordset to the webPage
      $webPage->addRecordset($rs_cols);
    }
}
?>