<?php
/* This file is part of phpWebApp. */

/** 
 * The $menu_items array contains the items of the tabs1.
 *
 * @package     admin
 */

$menu_items = array();

//modules Menu and Users can be used only by superuser
if (SU=='true')
{
  $menu_items['addbook'] = T_("Add Book");
  $menu_items['menu'] = T_("Menu");
  $menu_items['users'] = T_("Users");
}

//get the list of the books that can be administrated by the user
$record = shell(SCRIPTS."users/get_user.sh ".USER);
$fields = explode(':', $record);
$book_list = trim($fields[4]);

//add the book administration module only if the user 
//can administrate some books (book_list is not empty)
if (!(SU=='true') and $book_list != '')
{
  $menu_items['books'] = T_("Books");
}

//if the menu has no items, don't add the last item at all
if (sizeof($menu_items) > 0)
{
  if (!(SU=='true'))
    {
      $menu_items['user_data'] = T_("My Settings");
    }
}
?>