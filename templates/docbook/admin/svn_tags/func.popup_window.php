<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * Open a popup window from the JS code, which has the given title
 * and displays the given message.
 */
function popup_window($title, $message)
{
  $msg_Close = T_("Close");
  $message = str_replace("\n", "<br/>\n", $message);
  $html_content = "<html>
<head>
  <title>$title</title>
  <style>
    body 
     { 
       background-color: #f8fff8; 
       margin: 10px; 
       font-family: sans-serif; 
       font-size: 10pt; 
       color: #000066; 
     } 
    h1 { font-size: 12pt; color: #000066; }
    h2 { font-size: 10pt; color: #aa0000; }

    .button, a.button, a.button:hover
     {
       margin: 1px;
       border: none;
       padding: 1px 3px;
       background: #0088bb;
       color: #ffffff;
       font-weight: bold;
       text-decoration: none;
     }
  </style>
</head>
<body>
<h1>$title</h1>
$message
<div style='text-align: center;'>
<a class='button' href='javascript:window.close()'>$msg_Close</a>
</div>
</body>
</html>";
  $name = 'output_window';
  $url = '';
  $features = 'width=400,height=300';
  WebApp::popup_window($name, $url, $html_content, $features);
}
?>