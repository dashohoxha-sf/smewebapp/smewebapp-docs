// -*-C-*- //tell emacs to use C mode
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function refresh()
{
  GoTo('thisPage');
}

function unfix()
{
  SendEvent('tags', 'set_rev', 'rev=HEAD');
}

function set_rev(txtbox)
{
  var rev = txtbox.value;
  if (rev=='')  return;
  SendEvent('tags', 'set_rev', 'rev='+rev);
}

function set_date(txtbox)
{
  var date = txtbox.value;
  if (date=='')  return;
  SendEvent('tags', 'set_date', 'date='+date);
}

function set_tag(lstbox)
{
  var idx = lstbox.selectedIndex;
  var tag = lstbox.options[idx].value;
  SendEvent('tags', 'set_tag', 'tag='+tag);
}

function add_tag(txtbox)
{
  var tag = txtbox.value;
  var msg = T_("You are adding the tag 'v_tag' for the currently selected revision.");
  msg = msg.replace(/v_tag/, tag);

  if (tag=='')  return;

  if (confirm(msg))
    {
      SendEvent('tags', 'add_tag', 'tag='+tag);
    }
}

function del_tag(txtbox)
{
  var tag = txtbox.value;
  var msg = T_("You are deleting the tag: v_tag !").replace(/v_tag/, tag);

  if (tag=='')  return;

  if (confirm(msg))
    {
      SendEvent('tags', 'del_tag', 'tag='+tag);
    }
  else
    {
      txtbox.value = '';
    }
}
