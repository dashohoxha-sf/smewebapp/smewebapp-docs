<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include dirname(__FILE__).'/func.popup_window.php';

class tags extends WebObject
{
  function set_revision($revision)
    {
      $book_id = WebApp::getSVar('docbook->book_id');
      $lng = WebApp::getSVar('docbook->lng');
      $book_dir = BOOKS."$book_id/$lng/";

      //if it is sticked to a tag, then switch first to the trunk
      $tag = $this->get_current_tag();
      if ($tag != '')
        {
          $url = $this->get_svn_url();
          shell("svn switch $url/trunk/ $book_dir");
        }

      //update to the given revision
      $output = shell("svn update -r $revision $book_dir");
      popup_window("Updated Files", $output);

      //set propper ownership to the files
      set_data_owner($book_dir);

      //update cache files in the public copy for this lng
      shell(CONTENT."cache/cache.sh $book_id $lng 'books' './'");
    }

  /**
   * Write the file 'fixed' which prevents the update of the latest 
   * changes of the book in the public copy.
   */
  function write_fixed_file($content)
    {
      $fname = $this->get_book_dir().'fixed';
      write_file($fname, $content);
    }

  function on_set_tag($event_args)
    {
      $book_id = WebApp::getSVar('docbook->book_id');
      $lng = WebApp::getSVar('docbook->lng');
      $book_dir = BOOKS."$book_id/$lng/";

      $tag = $event_args['tag'];
      $this->write_fixed_file("tag=$tag");
      $url = $this->get_svn_url();
      $tag_url = ( $tag=='' ? "$url/trunk/" : "$url/tags/$tag/" );

      //switch to the given tag
      $output = shell("svn switch $tag_url $book_dir");
      popup_window("Updated Files", $output);

      //set propper ownership to the files
      set_data_owner($book_dir);

      //update cache files in the public copy for this lng
      shell(CONTENT."cache/cache.sh $book_id $lng 'books' './'");
    }

  function on_set_rev($event_args)
    {
      $rev = $event_args['rev'];
      $this->write_fixed_file("rev=$rev");
      $this->set_revision($rev);

      //if rev is HEAD, remove the 'fixed' file
      if ($rev=='HEAD')  unlink($this->get_book_dir().'fixed');
    }

  function on_set_date($event_args)
    {
      $date = $event_args['date'];
      $this->write_fixed_file("date=$date");
      $this->set_revision("{'$date'}");
    }

  function on_add_tag($event_args)
    {
      $tag = $event_args['tag'];

      $rev = $this->get_revision_nr();
      $date = $this->get_revision_date();
      $msg = date('Y-m-d H:i') . " >> Tag $tag created by " . USER
        . " for revision $rev (last modified date: $date)";

      $book_dir = $this->get_book_dir();
      $url = $this->get_svn_url();
      shell("svn copy -m '$msg' $book_dir $url/tags/$tag/");
    }

  function on_del_tag($event_args)
    {
      $tag = $event_args['tag'];
      $msg = date('Y-m-d H:i') . " >> Tag $tag deleted by " . USER;
      $url = $this->get_svn_url();
      shell("svn delete -m '$msg' $url/tags/$tag/");
    }

  function onRender()
    {
      WebApp::addVar('tag', $this->get_current_tag());
      WebApp::addVar('revision', $this->get_revision_nr());
      WebApp::addVar('revision_date', $this->get_revision_date());
      WebApp::addVar('head_revision', $this->get_head_revision());
      WebApp::addVar('is_fixed', $this->is_fixed());
      $this->add_rs_tags();
    }

  function get_current_tag()
    {
      $book_id = WebApp::getSVar('docbook->book_id');
      $lng = WebApp::getSVar('docbook->lng');
      $book_dir = BOOKS."$book_id/$lng/";

      $url_line = shell("svn info $book_dir | sed -n '/^URL: /p'");
      $url_line = trim($url_line);
      $pattern = "/$book_id/$lng/tags/([^/]+)";
      $tag = ( ereg($pattern, $url_line, $regs) ? $regs[1] : '' );
      return $tag;
    }

  function get_revision_nr()
    {
      $book_dir = $this->get_book_dir();
      $rev_line = shell("svn info $book_dir | sed -n '/^Revision: /p'");
      $rev = str_replace('Revision: ', '', $rev_line);
      $rev = trim($rev);
      return $rev;
    }

  function get_revision_date()
    {
      $book_dir = $this->get_book_dir();
      $sed = "sed -n '/^Last Changed Date: /p'";
      $gawk = 'gawk \'{print $4" "$5}\'';
      $date = shell("svn info $book_dir | $sed | $gawk");
      $date = trim($date);
      return $date;
    }

  function get_head_revision()
    {
      $book_id = WebApp::getSVar('docbook->book_id');
      $lng = WebApp::getSVar('docbook->lng');
      $svn_dir = SVN."$book_id/$lng/";
      $head_rev = shell("svnlook youngest $svn_dir");
      $head_rev = trim($head_rev);
      return $head_rev;
    }

  function is_fixed()
    {
      $book_dir = $this->get_book_dir();
      if (file_exists($book_dir.'fixed'))
        return true;
      else
        return false;
    }

  /** Add to webpage a recordset of tags. */
  function add_rs_tags()
    {
      $rs = new EditableRS('rs_tags');

      $url = $this->get_svn_url();
      $output = shell("svn list $url/tags/");

      $rs->addRec(array('id'=>'', 'label'=>''));
      $lines = explode("\n", $output);
      for ($i=0; $i < sizeof($lines); $i++)
        {
          $line = $lines[$i];
          if ($line=='')  continue;
          $tag = substr($line, 0, -1);
          $rs->addRec(array('id'=>$tag, 'label'=>$tag));
        }

      global $webPage;
      $webPage->addRecordset($rs);
    }

  /** return the URL of the SVN repository: file://path/book_id/lng */
  function get_svn_url()
    {
      $book_id = WebApp::getSVar('docbook->book_id');
      $lng = WebApp::getSVar('docbook->lng');
      $book_dir = BOOKS."$book_id/$lng/";

      $url = shell("svn info $book_dir | sed -n '/^URL: /p'");

      $url = ereg_replace('^URL: ', '', $url);
      $url = ereg_replace("$book_id/$lng/.*", "$book_id/$lng", $url);

      return $url;
    }

  /** return the directory of the book: content/books/xml/book_id/lng/ */
  function get_book_dir()
    {
      $book_id = WebApp::getSVar('docbook->book_id');
      $lng = WebApp::getSVar('docbook->lng');
      $book_dir = BOOKS."$book_id/$lng/";
      return $book_dir;
    }
}
?>
