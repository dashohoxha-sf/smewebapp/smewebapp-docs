<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class admin extends WebObject
{
  function on_update_search_index($event_args)
    {
      shell('search/make_index.sh');
    }

  function on_update_downloadables($event_args)
    {
      $book_id = WebApp::getSVar('docbook->book_id');
      $lng = WebApp::getSVar('docbook->lng');

      $update_downloads_sh = SCRIPTS.'book_admin/update_downloads.sh';
      shell("$update_downloads_sh $book_id $lng");
    }

  function onRender()
    {
    }
}
?>