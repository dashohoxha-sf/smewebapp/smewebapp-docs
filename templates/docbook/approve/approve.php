<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

//we need the function xml_to_text()
include_once DOCBOOK.'/edit/content/convert_from_xml.php';

class approve extends WebObject
{
  function on_set_lock($event_args)
    {
      $lock = $event_args['lock'];
      set_node_lock($lock, 'approve');
    }

  function on_commit($event_args)
    {
      $book_id = WebApp::getSVar('docbook->book_id');
      $lng = WebApp::getSVar('docbook->lng');
      $node_path = WebApp::getSVar('docbook->node_path');

      $workspace_xml_file = file_content_xml(WS_BOOKS);
      $public_xml_file = file_content_xml(BOOKS);
      $tag = book_fixed_to_tag($book_id, $lng);
      $recursive = ($event_args['recursive'] == 'checked');

      if ($recursive)
        {
          //commit all the content.xml files 
          $ws_node_folder = dirname($workspace_xml_file);
          $msg = date('Y-m-d H:i').' >> Approved by '.USER;
          $status = 'synchronized:'.USER.':'.EMAIL.':'.time();
          shell("svn commit $ws_node_folder -m '$msg'");

          //update the content.xml files in the public copy
          if ($tag)
            {
              $msg = T_("The book (v_book_id, v_lng) is fixed to v_tag, \
so the changes will not be displayed in the public copy.");
              $msg = str_replace('v_book_id', $book_id, $msg);
              $msg = str_replace('v_lng', $lng, $msg);
              $msg = str_replace('v_tag', $tag, $msg);
              WebApp::message($msg);
            }
          else
            {
              //update content.xml files
              $book_dir = BOOKS."$book_id/$lng/";
              shell("svn update $book_dir");
              $node_folder = dirname($public_xml_file).'/';
              set_data_owner($node_folder);

              //update cache files
              shell(CONTENT."cache/cache.sh $book_id $lng 'books' '$node_path'");
            }

        }
      else
        {
          $arr_state = get_node_state();
          extract($arr_state);
          $m_time = date('Y-m-d H:i', $m_timestamp);
          $time = date('Y-m-d H:i');
          $msg = ("Modified by $m_user, $m_time. "
                  . "Approved by ".USER.", $time");

          shell("svn commit $workspace_xml_file -m '$msg'");

          //update the content.xml files in the public copy
          if ($tag)
            {
              $msg = T_("The book (v_book_id, v_lng) is fixed to v_tag, \
so the changes will not be displayed in the public copy.");
              $msg = str_replace('v_book_id', $book_id, $msg);
              $msg = str_replace('v_lng', $lng, $msg);
              $msg = str_replace('v_tag', $tag, $msg);
              WebApp::message($msg);
            }
          else
            {
              //update content.xml files
              $book_dir = BOOKS."$book_id/$lng/";
              shell("svn update $book_dir");
              set_data_owner($public_xml_file);

              //update the cache file content.html of the public copy
              update_cache('public');
            }

          //set the status of the node to synchronized
          set_node_status('synchronized');
        }
    }

  function on_revert($event_args)
    {
      $workspace_xml_file = file_content_xml(WS_BOOKS);

      $recursive = ($event_args['recursive'] == 'checked');
      if ($recursive)
        {
          //revert all the content.xml files 
          $node_folder = dirname($workspace_xml_file);
          shell("svn revert -R $node_folder");

          //update cache files in workspace
          $book_id = WebApp::getSVar('docbook->book_id');
          $lng = WebApp::getSVar('docbook->lng');
          $node_path = WebApp::getSVar('docbook->node_path');
          $cache_sh = CONTENT.'cache/cache.sh';
          shell("$cache_sh $book_id $lng 'workspace' '$node_path'");
        }
      else
        {
          //revert
          shell("svn revert $workspace_xml_file");

          //update the cache file content.html in the workspace
          update_cache();

          //set the status of the node to synchronized
          set_node_status('synchronized');
        }
    }

  function onRender()
    {
      //add variables {{status}}, {{m_user}}, {{m_email}}, {{m_time}}
      $arr_state = get_node_state();
      WebApp::addVar('Status', ucfirst($arr_state['status']));
      WebApp::addVar('m_user', $arr_state['m_user']);
      WebApp::addVar('m_email', $arr_state['m_email']);
      $m_time = get_date_str($arr_state['m_timestamp']);
      WebApp::addVar('m_time', $m_time);

      //add the variables {{locked_by_somebody}} and {{locked}}
      $locked_by_somebody = locked_by_somebody($arr_state);
      $str_locked_by_somebody = ($locked_by_somebody ? 'true' : 'false');
      $str_locked = (is_locked($arr_state) ? 'true' : 'false');
      WebApp::addVar('locked_by_somebody', $str_locked_by_somebody);
      WebApp::addVar('locked', $str_locked);

      //display a notification message if the node is locked
      if ($locked_by_somebody)
        {
          extract($arr_state);
          $msg = T_("This node is locked for v_mode\n\
by v_l_user (v_l_email).\n\
Please try again later.");
          $msg = str_replace('v_mode', $mode, $msg);
          $msg = str_replace('v_l_user', $l_user, $msg);
          $msg = str_replace('v_l_email', $l_email, $msg);
          WebApp::message($msg);
          WebApp::addVar('l_mode', $mode);
          WebApp::addVar('l_user', $l_user);
          WebApp::addVar('l_email', $l_email);
          return;
        }

      //add {{id}}
      ereg('([^/]+)/$', $node_path, $regs);
      $id = $regs[1];
      if ($id=='.')  $id = WebApp::getSVar('docbook->book_id');
      WebApp::addVar("id", $id);

      //add the {{diff}} variable
      $diff = $this->get_diff();
      WebApp::addVar('diff', $diff."\n");

      $view_revisions = (trim($diff)=='' ? 'true' : 'false');
      WebApp::addVar('view_revisions', $view_revisions);
    }

  function get_diff()
    {
      $file_xml = file_content_xml();

      //get the HEAD revision of the file
      $xml_HEAD = shell("svn cat $file_xml -r HEAD");
      $file_xml_head = tempnam('/tmp', 'docbookwiki_');
      write_file($file_xml_head, $xml_HEAD);

      //convert to TextWiki the HEAD revision
      $file_txt_head = tempnam('/tmp', 'docbookwiki_');
      $txt_HEAD = xml_to_text($file_xml_head);
      write_file($file_txt_head, $txt_HEAD);

      //convert to TextWiki the working copy
      $file_txt = tempnam('/tmp', 'docbookwiki_');
      $txt = xml_to_text($file_xml);
      write_file($file_txt, $txt);

      //get the difference
      $diff = shell("diff -ubB $file_txt_head $file_txt | sed '1,2d'");

      //clean the temporary files
      shell("rm $file_xml_head $file_txt_head $file_txt");

      //format it for propper display in html
      $diff = htmlentities($diff);
      $patterns = array(
                        '/^(@@.*@@)$/m',
                        '/^(-.*)$/m',
                        '/^(\+.*)$/m'
                        );
      $replacements = array(
                            '<span class="line_nr">\\1</span>',
                            '<span class="removed">\\1</span>',
                            '<span class="added">\\1</span>'
                            );
      $diff = preg_replace($patterns, $replacements, $diff);

      return $diff;
    }
}
?>