<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once dirname(__FILE__).'/funcs.php';

class docbook extends WebObject
{
  function init()
    {
      $this->addSVar('book_id', UNDEFINED);
      $this->addSVar('node_path', './');
      $this->addSVar('languages', LANGUAGES);
      $this->addSVar('lng', 'en');
      $this->addSVar('mode', 'view');  //view | edit | approve | admin
    }

  function on_set_node($event_args)
    {
      $node_path = $event_args['node_path'];
      $this->setSVar('node_path', $node_path);
    }

  function on_set_node_id($event_args)
    {
      $id = $event_args['node_id'];
      $path = process_index('edit/get_node_path.xsl', array('id'=>$id));
      $this->setSVar('node_path', $path);
    }

  function on_set_mode($event_args)
    {
      $mode = $event_args['mode'];
      $this->setSVar('mode', $mode);
    }

  function on_set_lng($event_args)
    {
      $lng = $event_args['lng'];
      $this->setSVar('lng', $lng);

      //set book_title
      $book_id = $this->getSVar('book_id');
      $book_title = main::get_book_title($book_id, $lng);
      WebApp::setSVar('book_title', $book_title);
    }

  function onParse()
    {
      $mode = $this->getSVar('mode');
      WebApp::addVar('node', "$mode/$mode.html");      
      WebApp::addGlobalVar('is_admin', $this->is_admin());
    }

  /**
   * Returns 'true' if the current user is an admin for the current book,
   * otherwise returns 'false'.
   */
  function is_admin()
    {
      //superuser is considered to be admin of each book
      if (SU=='true')  return 'true';

      //get the books for which the user is admin
      $record = shell(SCRIPTS.'users/get_user.sh '.USER);
      $record = trim($record);
      $arr_fields = explode(':', $record);
      $book_list = $arr_fields[4];
      $arr_books = explode(',', $book_list);

      $book_id = $this->getSVar('book_id');
      $is_admin = (in_array($book_id, $arr_books));
      return ($is_admin ? 'true' : 'false');
    }

  function onRender()
    {
      //add navigation variables
      $node_path = $this->getSVar('node_path');
      $vars = get_arr_navigation($node_path);
      if ($vars['this_full_title']=='') 
        $vars['this_full_title'] = T_("Table Of Contents");
      WebApp::addVars($vars);
      WebApp::addVar('toc_path', './');

      $this->add_button_vars();

      //add state vars
      $arr_state = get_node_state();
      //print "<xmp>";  print_r($arr_state);  print "</xmp>";    //debug
      $locked = locked_by_somebody($arr_state);
      $str_locked = ($locked ? 'locked' : 'unlocked');
      WebApp::addVar('locked', $str_locked);
      WebApp::addVar('status', $arr_state['status']);
    }

  /**
   * Add the variables {{approve}} and {{edit}} which are used
   * to display the buttons Approve and Edit.
   */
  function add_button_vars()
    {
      $is_admin = WebApp::getVar('is_admin');
      if ( !defined('EDIT') )
        {
          //no buttons, if not in edit interface
          $edit = 'false';
          $approve = 'false';
          $admin = 'false';
        }
      else if ($is_admin=='true')
        {
          //both buttons, no restrictions
          //for the admins of the book
          $edit = 'true';
          $approve = 'true';

          //the admin button is used only in the root node
          $node_path = WebApp::getSVar('docbook->node_path');
          $admin = ($node_path=='./' ? 'true' : 'false');
        }
      else
        {
          //otherwise show the buttons 
          //according to the edit rights of the user
          list($edit, $approve) = $this->get_edit_rights();

          //don't display the admin button for no admins
          $admin = 'false';
        }

      WebApp::addVar('edit', $edit);
      WebApp::addVar('approve', $approve);
      WebApp::addVar('admin', $admin);
    }

  /**
   * Reads the user edit rights file 'admin/access_rights/username'
   * which contains lines like this:
   *     allow:edit,approve:node_path_1,node_path_2:lng1,lng2
   *     deny:approve:node_path_3,node_path_4:lng2
   * After matching the current node and language with these lines,
   * returns array($edit, $approve) with values 'true' or 'false'.
   */
  function get_edit_rights()
    {
      //initiate them to false
      $edit = 'false';
      $approve = 'false';

      //get the lines of the edit rights file of the user
      $book_id = $this->getSVar('book_id');
      $accr_file = ADMIN."access_rights/$book_id/".USER;
      $arr_lines = (file_exists($accr_file) ? file($accr_file) : array());

      //try to match the lines with the current node and language
      //and set the permissions accordingly
      for ($i=0; $i < sizeof($arr_lines); $i++)
        {
          $line = $arr_lines[$i];
          $line = trim($line);
          list($access,$levels,$nodes,$langs) = explode(':', $line);

          //if this line matches the current node and language 
          //then set the values of edit and approve according to it
          if ($this->node_match($nodes) and $this->lang_match($langs))
            {
              $value = ($access=='allow' ? 'true' : 'false');
              $arr_levels = explode(',', $levels);
              if (in_array('edit', $arr_levels))      $edit = $value;
              if (in_array('approve', $arr_levels))   $approve = $value;
            }
        }

      return array($edit, $approve);
    }

  /**
   * Returns true if one of the node_path expressions in the list
   * matches the current node path, otherwise returns false.
   * The node_list is a comma separated list of node_path expressions
   * (which are actually regular expressions), or is the string 'ALL'.
   * 'ALL' matches any node path.
   */
  function node_match($node_list)
    {
      if (strtoupper($node_list)=='ALL')  return true;

      $node_path = $this->getSVar('node_path');
      $arr_nodes = explode(',', $node_list);
      for ($i=0; $i < sizeof($arr_nodes); $i++)
        {
          $expr = $arr_nodes[$i];
          if (ereg('^'.$expr, $node_path)) return true;
        }
    }

  /**
   * Returns true if one of the language ids in the list matches
   * the current language, otherwise returns false. The lang_list
   * is a comma separated list of languages, or 'ALL'.
   * 'ALL' matches any language.
   */
  function lang_match($lang_list)
    {
      if (strtoupper($lang_list)=='ALL')  return true;

      $lng = $this->getSVar('lng');
      $arr_langs = explode(',', $lang_list);
      $match = in_array($lng, $arr_langs);

      return $match;
    }
}
?>