//-*-C-*-
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function set_edit_mode(mode)	
{
  SendEvent('edit_content', 'set_mode', 'mode='+mode);
}

function save()
{
  var content = document.edit_content.content.value;
  content = encode_arg_value(content);
  SendEvent('edit_content', 'save', 'content='+content);
}

function help()
{
  var mode = session.getVar('edit_content->mode');
  var url = 'xref.php?docbookwiki_guide/' + mode;
  window.open(url, 'Help');
}

function set_normalize(chkbox)
{
  var checked = (chkbox.checked ? 'true' : 'false');
  session.setVar('edit_content->normalize', checked);
}
