<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include dirname(__FILE__).'/convert_from_xml.php';
include dirname(__FILE__).'/convert_to_xml.php';
include dirname(__FILE__).'/validate.php';
include dirname(__FILE__).'/func.popup_window.php';

class edit_content extends WebObject
{
  function init()
    {
      $this->addSVar('mode', 'text');  //text | xml | html | latex | texi
      $this->addSVar('normalize', 'false');  //normalize space
    }

  function on_set_mode($event_args)
    {
      $mode = $event_args['mode'];
      $this->setSVar('mode', $mode);
    }

  function on_save($event_args)
    {
      //make sure that the node is not locked by somebody else
      if (locked_by_somebody())  return;

      $content = $event_args['content'];
      $content = $this->strip_comments($content);
      $content = $this->strip_cdata($content);

      //replace the xml entities
      $content = preg_replace('#&(\w+);#', '&amp;$1;', $content);

      //convert it to xml(docbook)
      $mode = $this->getSVar('mode');
      $converter = $mode.'_to_xml';
      $xml_content = $converter($content);

      if ($xml_content=='ERROR')
        {
          $this->error_on_save($content);
          return;
        }

      //put back the cdata and the comments
      $xml_content = $this->putback_cdata($xml_content);
      $xml_content = $this->putback_comments($xml_content);

      //validate the xml content
      if (! validate_xml($xml_content))
        {
          $this->error_on_save($content);
          return;
        }

      //write the new content to the content file
      $xml_file = file_content_xml();
      write_file($xml_file, $xml_content);

      //update the cache file content.html
      update_cache();

      //set the status of the node to modified
      set_node_status('modified');
    }

  function error_on_save($content)
    {
      $this->error = true;
      $content = $this->putback_cdata($content, 'html');
      $content = $this->putback_comments($content);
      WebApp::addGlobalVar('node_content', $content);
      WebApp::message(T_("There was an error, failed to save!"));
    }

  function onRender()
    {
      $this->add_tab_items();
      $this->add_node_content();
    }

  /**
   * Add to the webpage a recordset with the items of the tabs (modes).
   */
  function add_tab_items()
    {
      $items = array(
                     'text'  => 'Text',
                     'xml'   => 'DocBook',
                     'html'  => 'HTML',
                     'latex' => 'Latex',
                     'texi'  => 'Texi'
                     );

      $rs = new EditableRS('items');

      //fill the recordset
      $mode = $this->getSVar('mode');
      while ( list($item, $label) = each($items) )
        {
          $css_class = ($item==$mode ? 'item-selected' : 'item');
          $rec = array(
                       'item'  => $item, 
                       'label' => $label,
                       'class' => $css_class
                       );
          $rs->addRec($rec);
        }

      //set the recordset to the page
      global $webPage;
      $webPage->addRecordset($rs);
    }

  /**
   * Builds the content of the node by processing content.xml
   * with xml2mode.xsl, and assigns it to a template variable.
   */
  function add_node_content()
    {
      if ($this->error)  return;

      //get the content of the xml file
      $xml_file = file_content_xml();
      $xml_content = implode('', file($xml_file));

      //strip <![CDATA[...]]> and <!--comment-->
      $xml_content = $this->strip_comments($xml_content);
      $xml_content = $this->strip_cdata($xml_content);

      //write the modified xml content to a temporary file
      $tmpfile = tempnam('/tmp', 'docbookwiki_');
      write_file($tmpfile, $xml_content);

      //generate the html content by processing the temporary file
      $mode = $this->getSVar('mode');
      if ($mode=='html' or $mode=='text')
        {
          $converter = "xml_to_$mode";
          $html_content = $converter($tmpfile);
        }
      else 
        {
          $xsl_file = XSLT."edit_content/xml2${mode}.xsl";
          $html_content = shell("xsltproc $xsl_file $tmpfile");
        } 
      shell("rm $tmpfile");

      //put back the <![CDATA[...]]> and <!--comment--> parts
      $html_content = $this->putback_cdata($html_content, 'html');
      $html_content = $this->putback_comments($html_content);

      //comment any {{variables}} so that they are not substituted
      $html_content = str_replace('{{', '{{#', $html_content);

      WebApp::addVar("node_content", $html_content);
    }

  /** replace <![CDATA[...]]> by <cdata>x</cdata> */
  function strip_cdata($str)
    {
      preg_match_all('#<!\[CDATA\[.*?]]>#s', $str, $matches);
      $arr_cdata = $matches[0];

      for ($i=0; $i < sizeof($arr_cdata); $i++)
        {
          $str = str_replace($arr_cdata[$i], "<cdata>$i</cdata>", $str);
          $arr_cdata[$i] = str_replace("\r\n", "\n", $arr_cdata[$i]);
        }
      
      //save it for the function putback_cdata()
      $this->arr_cdata = $arr_cdata;

      return $str;
    }

  /** replace <!--...--> by <comment>x</comment> */
  function strip_comments($str)
    {
      preg_match_all('#<!--.*?-->#s', $str, $matches);
      $arr_comments = $matches[0];

      for ($i=0; $i < sizeof($arr_comments); $i++)
        {
          $str = str_replace($arr_comments[$i], "<comment>$i</comment>", $str);
          $arr_comments[$i] = str_replace("\r\n", "\n", $arr_comments[$i]);
        }
      
      //save it for the function putback_comments()
      $this->arr_comments = $arr_comments;

      return $str;
    }

  /** replace <comment>x</comment> by the corresponding <!--...--> */
  function putback_comments($str)
    {
      $arr_comments = $this->arr_comments;
      for ($i=0; $i < sizeof($arr_comments); $i++)
        {
          $str = str_replace("<comment>$i</comment>", $arr_comments[$i], $str);
        }
      return $str;
    }

  /** replace <cdata>x</cdata> by the corresponding <![CDATA[...]]> */
  function putback_cdata($str, $mode ='xml')
    {
      $arr_cdata = $this->arr_cdata;
      for ($i=0; $i < sizeof($arr_cdata); $i++)
        {
          $cdata = $arr_cdata[$i];
          if ($mode=='html')
            {
              $cdata = preg_replace('#&(\w+);#', '&amp;$1;', $cdata);
              $cdata = preg_replace('#^<#', '&lt;', $cdata);
            }
          $str = str_replace("<cdata>$i</cdata>", $cdata, $str);
        }
      return $str;
    }
}
?>
