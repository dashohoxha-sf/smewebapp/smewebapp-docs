<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function validate_xml($xml_content)
{
  //write the docbook content to a temporary file
  $tmpfile = tempnam('/tmp', 'docbook_');
  write_file($tmpfile, $xml_content);

  //validate the temporary xml file
  $fpi = '-//OASIS//DTD DocBook XML V4.2//EN';
  $xmllint = "xmllint --noout --dtdvalidfpi '$fpi' --nonet --nowarning";
  $sed = 'sed -e \$d -e /warning:/d';  
  $err_lines = shell("$xmllint $tmpfile 2>&1 | $sed");

  exec("rm $tmpfile");

  //remove any xref errors, if the section ID exists
  $pattern = '/IDREF attribute linkend references an unknown ID "([^"]*)"/';
  if (preg_match_all($pattern, $err_lines, $matches))
    {
      //get an array of error section ID-s
      $xref_ids = $matches[1];

      //get a list of all section id-s of the document
      $id_list = process_index_node('get_id_list');
      $doc_ids = explode("\n", $id_list);

      //remove from error lines those lines 
      //that have an id that exists in the document
      for ($i=0; $i < sizeof($xref_ids); $i++)
        {
          $id = $xref_ids[$i];
          if (in_array($id, $doc_ids))
            {
              $pattern = "/^.*IDREF attribute linkend references an unknown ID \"$id\"\$\n?/m";
              $err_lines = preg_replace($pattern, '', $err_lines);
            }
        }
    }

  //check for any errors
  if (trim($err_lines) != '')
    {
      display_error_messages($xml_content, $err_lines);
      return false;
    }

  return true;
}

/** display a popup window with error messages */
function display_error_messages(&$xml_content, &$err_lines)
{
  $arr_xml_lines = explode("\n", $xml_content);
  $arr_err_lines = explode("\n", $err_lines);

  $message = "<ul>\n";
  for ($i=0; $i < sizeof($arr_err_lines); $i++)
    {
      $err_line = $arr_err_lines[$i];
      if (trim($err_line)=='')  continue;

      //extract the $line_nr etc. from the error line
      $arr_fields = explode(':', $err_line);
      $line_nr = $arr_fields[1];
      $elem = $arr_fields[2];
      $err_msg = $arr_fields[4];
      $err_msg = ereg_replace(', expecting.*', '', $err_msg);

      //extract from the xml line a part around the error
      $xml_line = $arr_xml_lines[$line_nr - 1];
      $xml_line = htmlentities($xml_line);
      $elem = str_replace('element', '', $elem);
      $elem = trim($elem);
      $xml_line = preg_replace("#(&lt;/?$elem.*?&gt;)#", '<strong>\1</strong>', $xml_line);

      //output the error line and the xml line
      $message .= "<li><pre><span class='err_line'>$err_msg</span>\n";
      $message .= "<span class='xml_line'>$xml_line</span></pre></li>\n";
    }
  $message .= "</ul>\n";

  popup_window("Validation Errors", $message);
}
?>