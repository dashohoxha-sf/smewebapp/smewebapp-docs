<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


function xml_to_html($xml_file)
{
  //process it with a transformer
  $normalize = WebApp::getSVar('edit_content->normalize');
  $norm = ($normalize=='true' ? '_normalize' : '');
  $xsl_file = XSLT."edit_content/xml2html${norm}.xsl";
  $html_content = shell("xsltproc $xsl_file $xml_file");

  return $html_content;
}

function xml_to_text($xml_file)
{
  $patterns =
    array(
          /* escape some special chars by adding a slash before them */
          "/(_|`|~)/",

          /* escape [...] by adding a slash before it */
          "/\[([^\]]+)\]/",

          /* escape !# by adding a slash before it */
          "/!#/",

          /* escape bash$ and bash# */
          "/^\s*(bash\\\$|bash#)/",
          );

  $replacements =
    array(
          /* escape some special chars by adding a slash before them*/
          '\\\\\\1',

          /* escape [...] by adding a slash before it */
          '\\[\\1]',

          /* escape !# by adding a slash before it */
          '\\!#',

          /* escape bash$ and bash# */
          '\\\\\\1',
          );

  $arr_lines = file($xml_file);
  for ($i=0; $i < sizeof($arr_lines); $i++)
    {
      $arr_lines[$i] = preg_replace($patterns, $replacements, $arr_lines[$i]);
    }
  $xml_content = implode('', $arr_lines);
  write_file($xml_file, $xml_content);

  //process it with a transformer
  $xsl_file = XSLT."edit_content/xml2text.xsl";
  $text_content = shell("xsltproc $xsl_file $xml_file 2>&1");

  $patterns = array('/&amp;lt;/', '/&amp;gt;/');
  $replacements = array('\\<', '\\>');
  $text_content = preg_replace($patterns, $replacements, $text_content);

  return $text_content;
}

?>