<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


/** returns the id and the title of the current node */
function get_id_title()
{
  $node_path = WebApp::getSVar('docbook->node_path');
  $navig = get_arr_navigation($node_path);
  $title = $navig['this_title'];
  if ($node_path=='./')
    {
      $id = WebApp::getSVar('docbook->book_id');
    }
  else
    {
      ereg('([^/]+)/$', $node_path, $regs);
      $id = $regs[1];
    }

  return array($id,$title);
}

function xml_to_xml($content)
{
  //complete the content by adding the title etc.
  list($id, $title) = get_id_title();
  $node_types = process_index_node('get_node_type');
  list($node_type, $subnode_type) = explode(':', $node_types);

  $content = str_replace("\r\n", "\n", $content);
  $content = chop($content)."\n";
  $content = "<?xml version='1.0' encoding='iso-latin-1'?>
<$node_type id='$id'>
<title>$title</title>$content</$node_type>
";

  return (is_wellformed($content) ? $content : 'ERROR');
}

/**
 * Check that the given XML string is well formed.
 * Return true if well formed, false otherwise.
 */
function is_wellformed($xml_content)
{
  //write the xml content to a temporary file
  $tmpfile = tempnam('/tmp', 'docbookwiki_');
  write_file($tmpfile, $xml_content);

  //process it with a transformer which makes no changes
  //just to check for any errors
  $xsl_file = XSLT."edit_content/xml2xml1.xsl";
  $output = shell("xsltproc $xsl_file $tmpfile");

  shell("rm $tmpfile");

  //check for any errors
  if (ereg("^$tmpfile:", $output))
    {
      //WebApp::message($output);
      popup_window('Transformation Error', "<xmp>$output</xmp>");
      return false;
    }
  else
    return true;
}

function html_to_xml($content)
{
  //complete the content by adding the title etc.
  list($id, $title) = get_id_title();
  $node_types = process_index_node('get_node_type');
  list($node_type, $subnode_type) = explode(':', $node_types);

  $content = "<?xml version='1.0' encoding='iso-latin-1'?>
<$node_type id='$id'>
<title>$title</title>

$content
</$node_type>
";

  //write the modified content to a temporary file
  $tmpfile = tempnam('/tmp', 'book_');
  write_file($tmpfile, $content);

  //process it with a transformer
  $normalize = WebApp::getSVar('edit_content->normalize');
  $norm = ($normalize=='true' ? '_normalize' : '');
  $xsl_file = XSLT."edit_content/html2xml${norm}.xsl";
  $xml_content = shell("xsltproc $xsl_file $tmpfile");

  //check for any errors during conversion
  if (ereg("^$tmpfile:", $xml_content))
    {
      //WebApp::message($xml_content);
      popup_window('Transformation Error', "<xmp>$xml_content</xmp>");
      return 'ERROR';
    }

  shell("rm $tmpfile");

  return $xml_content;
}

function text_to_xml($content)
{
  //include the converter from Text/Wiki to XML
  include_once dirname(__FILE__)."/text_to_xml/package.WikiConverter.php";

  //get id, title, node_type, etc.
  list($id, $title) = get_id_title();
  $node_types = process_index_node('get_node_type');
  list($node_type, $subnode_type) = explode(':', $node_types);

  //convert to XML
  if ($node_type=='book' or $node_type=='article')
    {
      $info = new Info();
      $info->parse($content);
      $xml_content = $info->to_xml();
    }
  else
    {
      $parser = new WikiParser;
      $tpl = $parser->parse_string($content);
      $xml_content = $tpl->to_xml();
      if (trim($xml_content)=='')  $xml_content = '<para/>';
    }

  //complete the xml_content by adding the title etc.
  if ($node_type=='book')
    {
      $xml_content = "<?xml version='1.0' encoding='iso-latin-1'?>
<book id='$id'>
<title>$title</title>
<bookinfo>$xml_content</bookinfo>
</book>
";
    }
  else if ($node_type=='article')
    {
      $xml_content = "<?xml version='1.0' encoding='iso-latin-1'?>
<article id='$id'>
<title>$title</title>
<articleinfo>$xml_content</articleinfo>
<para/>
</article>
";
    }
  else
    {
      $xml_content = "<?xml version='1.0' encoding='iso-latin-1'?>
<$node_type id='$id'>
<title>$title</title>$xml_content</$node_type>
";
    }

  return (is_wellformed($xml_content) ? $xml_content : 'ERROR');
}

function latex_to_xml($content)
{
  $msg = "Converting from LATEX to XML is not implemented yet,\n"
    . "so, any modifications are not saved.\n";
  WebApp::message($msg);

  $xml_content = 'ERROR';
  return $xml_content;
}

function texi_to_xml($content)
{
  $msg = "Converting from TEXI to XML is not implemented yet,\n"
    . "so, any modifications are not saved.\n";
  WebApp::message($msg);

  $xml_content = 'ERROR';
  return $xml_content;
}
?>