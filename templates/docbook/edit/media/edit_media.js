//-*-C-*-
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function rename_mitem(item)
{
  var msg = T_("Rename 'v_item' to:").replace(/v_item/, item);
  var new_item = prompt(msg);
  if (new_item==null || new_item=='')  return;
  var event_args =  'item='+item+';new_item='+new_item;
  SendEvent('edit_media', 'rename', event_args);
}

function delete_mitem(item)
{
  var msg = T_("Your are deleting 'v_item'!").replace(/v_item/, item);
  if (confirm(msg))
    {
      SendEvent('edit_media', 'delete', 'item='+item);
    }
}

function upload_file()
{
  var new_win = window.open('', 'upload_message', 'width=200,height=150');
  var file = document.upload.media_file.value;
  var uploading = T_("Uploading:");
  var please_wait = T_("Please wait...");
  var win_content = "<html>\n"
    + "<head>\n"
    + " <title>" + uploading + " " + file + " </title>\n"
    + " <style>\n"
    + "   body \n"
    + "   { \n"
    + "     background-color: #f8fff8; \n"
    + "     margin: 10px; \n"
    + "     font-family: sans-serif; \n"
    + "     font-size: 10pt; \n"
    + "     color: #000066; \n"
    + "   } \n"
    + "   h1 { font-size: 12pt; color: #000066; } \n"
    + "   h2 { font-size: 10pt; color: #aa0000; } \n"
    + " </style>\n"
    + "</head>\n"
    + "<body>\n"
    + "  <h1>" + uploading + " " + file + " </h1>\n"
    + "  <h2>" + please_wait + "<h2>\n"
    + "</body>\n"
    + "</html>\n";
  new_win.document.write(win_content);

  document.upload.submit();
  SendEvent('edit_media', 'refresh');
}
