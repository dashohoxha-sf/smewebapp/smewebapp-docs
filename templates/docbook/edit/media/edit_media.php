<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class edit_media extends WebObject
{
  function on_refresh()
  {
    sleep(1);
  }

  function on_delete($event_args)
  {
    $media_path = $this->get_media_path();
    $item = $event_args['item'];
    $file = $media_path.$item;
    shell("rm $file");
  }

  function on_rename($event_args)
  {
    $media_path = $this->get_media_path();
    $item = $event_args['item'];
    $new_item = $event_args['new_item'];
    $file = $media_path.$item;
    $new_file = $media_path.$new_item;
    shell("mv $file $new_file");
    set_data_owner($new_file);
  }

  function onRender()
  {
    $media_path = $this->get_media_path();
    //add the variable {{media_path}}
    WebApp::addVar('media_path', $media_path);

    //build and add the recordset media_items
    //which contains the items of this section
    $rs = new EditableRS('media_items');
    if (file_exists($media_path)) $dir = opendir($media_path);
    if ($dir) 
      {
        while (($file=readdir($dir)) !== false)
          { 
            if ($file=='.' or $file=='..' or $file=='CVS')  continue;
            if (ereg('\\.(xml|txt)$', $file))  continue;
            if (is_dir($file))  continue;

            $rs->addRec(array('item'=>$file));
          }
        closedir($dir); 
      }
    global $webPage;
    $webPage->addRecordset($rs);
  }

  function get_media_path()
  {
    $book_id = WebApp::getSVar('docbook->book_id');
    $lng = WebApp::getSVar('docbook->lng');
    $node_path = WebApp::getSVar('docbook->node_path');
    $media_path = WS_MEDIA."$book_id/$lng/$node_path";

    return $media_path;
  }
}
?>