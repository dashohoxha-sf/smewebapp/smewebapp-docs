<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


/**
 * This function sets a lock before index (structure of the book)
 * is modified, in order to make sure that the change in the index
 * and update of the cache files (navigation and subnodes) happens
 * as a single transaction, and concurrent editing does not have
 * unexpected results. Since the structure of the book is the same
 * for all the languages and the changes are sinchronized for all
 * the languages, the lock locks all the languages.
 */
function lock_index()
{
  $book_id = WebApp::getSVar('docbook->book_id');
  $lock_file = WS_BOOKS.$book_id.'/lock';

  //if the lock file exists, wait until 
  //it is removed or until it is too old
  while (file_exists($lock_file))
    {
      clearstatcache();
      $lock_age = time() - filectime($lock_file);
      if ($lock_age > 60) break;
    }
  touch($lock_file);
  set_data_owner($lock_file);
}

function unlock_index()
{
  $book_id = WebApp::getSVar('docbook->book_id');
  $lock_file = WS_BOOKS.$book_id.'/lock';
  unlink($lock_file);
}

/*----------------------------------------------*/
    
/** add the recordset 'subsections' */
function add_subsections_rs()
{
  $subsections = process_index_node('subsections');
  $lines = explode("\n", $subsections);

  $rs = new EditableRS('subsections');
  for ($i=0; $i < sizeof($lines); $i++)
    {
      list($path,$title) = split(' ', $lines[$i], 2);
      if ($path=='')  continue;
      $title = trim($title);
      $rs->addRec(compact('path', 'title'));
    }
  global $webPage;
  $webPage->addRecordset($rs);
}

/** 
 * Applies the given transformer to index.xml, 
 * passing the node as a parameter, and returns the output.
 */
function process_index_node($transformer, $node_path =UNDEFINED)
{
  if ($node_path==UNDEFINED)
    {
      $node_path = WebApp::getSVar('docbook->node_path');
    }
  $lng = WebApp::getSVar('docbook->lng');
  $transformer = "edit/${transformer}.xsl";
  $params = array('path'=>$node_path, 'lng'=>$lng);
  $output = process_index($transformer, $params);

  return $output;
}

/**
 * Applies a transformation to index.xml and then writes the
 * result back to index.xml.
 */
function transform_index($action, $arr_params =array())
{
  //process index.xml with an xsl, and get the new index
  $index_xml = process_index("edit/${action}.xsl", $arr_params);

  //construct the commit message
  $params = "action=\"$action\", ".array2str($arr_params);
  $commit_msg = date('Y-m-d H:i').' >> Modified by '.USER. ' >> '.$params;

  $book_id = WebApp::getSVar('docbook->book_id');

  //change index.xml for all the languages
  $langs = WebApp::getSVar('docbook->languages');
  $arr_langs = explode(',', $langs);
  for ($i=0; $i < sizeof($arr_langs); $i++)
    {
      $lng = $arr_langs[$i];

      //write the transformed index to index.xml
      $workspace_xml_file = WS_BOOKS."$book_id/$lng/index.xml";
      write_file($workspace_xml_file, $index_xml);

      //commit in svn
      shell("svn commit $workspace_xml_file -m '$commit_msg'");

      //update the public copy
      $tag = book_fixed_to_tag($book_id, $lng);
      if ($tag)
        {
          $msg = T_("The book (v_book_id, v_lng) is fixed to v_tag, \n\
so the changes will not be displayed in the public copy.");
          $msg = str_replace('v_book_id', $book_id, $msg);
          $msg = str_replace('v_lng', $lng, $msg);
          $msg = str_replace('v_tag', $tag, $msg);
          WebApp::message($msg);
        }
      else
        {
          $book_dir = BOOKS."$book_id/$lng/";
          $public_xml_file = $book_dir.'index.xml';
          shell("svn update $book_dir");
          set_data_owner($public_xml_file);
        }
    }
}

/** Update the files navigation.txt for all languages, for the given node. */
function update_navigation_all_langs($node_path)
{
  $langs = WebApp::getSVar('docbook->languages');
  $arr_langs = explode(',', $langs);
  for ($i=0; $i < sizeof($arr_langs); $i++)
    {
      $lng = $arr_langs[$i];
      update_navigation($node_path, $lng);
    }
}

/** Update the file navigation.txt for the given node and language. */
function update_navigation($node_path, $lng)
{
  $book_id = WebApp::getSVar('docbook->book_id');
  $xsl_file = 'edit/get_navigation.xsl';
  $params = array('path'=>$node_path, 'lng'=>$lng);

  //update the navigation file in the workspace
  $navigation = process_index($xsl_file, $params, WS_BOOKS);
  $fname = WS_CACHE."$book_id/$lng/$node_path/navigation.txt";
  write_file($fname, $navigation);

  //update the navigation file in the public copy
  if (!book_fixed_to_tag($book_id, $lng))
    {
      $navigation = process_index($xsl_file, $params, BOOKS);
      $fname = CACHE."$book_id/$lng/$node_path/navigation.txt";
      write_file($fname, $navigation);
    }
}

/**
 * Updates the cache files subnodes.html for the given node, 
 * for all its ancestors, and for all the languages.
 */
function update_subnodes_html_all_langs($node_path =UNDEFINED)
{
  if ($node_path==UNDEFINED)
    {
      $node_path = WebApp::getSVar('docbook->node_path');
    }

  //update subnodes for each language
  $langs = WebApp::getSVar('docbook->languages');
  $arr_langs = explode(',', $langs);
  for ($i=0; $i < sizeof($arr_langs); $i++)
    {
      $lng = $arr_langs[$i];
      update_subnodes_html($node_path, $lng);
    }
}

/**
 * Updates the cache files subnodes.html for the given language, 
 * for the given node and all its ancestors,
 * by applying 'update-subnodes.xsl' to index.xml.
 */
function update_subnodes_html($node_path, $lng)
{
  $transformer = 'edit/get_subnodes.xsl';
  $book_id = WebApp::getSVar('docbook->book_id');
  $fixed = book_fixed_to_tag($book_id, $lng);

  //update subnodes for this node and
  //for each ancestor (up to the root)
  while (true)
    {
      //parameters to be passed to the transformer
      $params = array('path'=>$node_path);

      //update the workspace subnodes.html
      $subnodes = process_index($transformer, $params, WS_BOOKS, $lng);
      $book_path = WS_CACHE."$book_id/$lng/";
      $subnodes_html = $book_path.$node_path.'subnodes.html';
      write_file($subnodes_html, $subnodes);

      //update the public subnodes.html
      if (!$fixed)
        {
          $subnodes = process_index($transformer, $params, BOOKS, $lng);
          $book_path = CACHE."$book_id/$lng/";
          $subnodes_html = $book_path.$node_path.'subnodes.html';
          write_file($subnodes_html, $subnodes);
        }

      if ($node_path=='./')  break;

      //get the path of the parent, by removing the last id
      $node_path = ereg_replace('[^/]+/$', '', $node_path);
    }
}

/*----------------------------------------------*/

function updatenavig_moveup($node_path)
{
  //update navigation files of both nodes and their neighbours
  update_navigation_all_langs($node_path);
  $navig = get_arr_navigation($node_path);
  update_navigation_all_langs($navig['prev_path']);
  update_navigation_all_langs($navig['next_path']);
  $next_navig = get_arr_navigation($navig['next_path']);
  update_navigation_all_langs($next_navig['next_path']);
}

function updatenavig_movedown($node_path)
{
  //update navigation files of both nodes and their neighbours
  update_navigation_all_langs($node_path);
  $navig = get_arr_navigation($node_path);
  update_navigation_all_langs($navig['prev_path']);
  update_navigation_all_langs($navig['next_path']);
  $prev_navig = get_arr_navigation($navig['prev_path']);
  update_navigation_all_langs($prev_navig['prev_path']);
}

function update_cache_files_all_langs($node_path)
{
  update_navigation_all_langs($node_path);
  $navig = get_arr_navigation($node_path);
  update_navigation_all_langs($navig['prev_path']);
  update_navigation_all_langs($navig['next_path']);      

  //update subnodes.html of the new node and the ancestors
  update_subnodes_html_all_langs($node_path);
}

function update_cache_files($node_path, $lng =UNDEFINED)
{
  if ($lng==UNDEFINED)  $lng = WebApp::getSVar('docbook->lng');

  update_navigation($node_path, $lng);
  $navig = get_arr_navigation($node_path);
  update_navigation($navig['prev_path'], $lng);
  update_navigation($navig['next_path'], $lng);      

  //update subnodes.html of the new node and the ancestors
  update_subnodes_html($node_path, $lng);
}

function delete_removefolders_all_langs($node_path)
{
  $langs = WebApp::getSVar('docbook->languages');
  $arr_langs = explode(',', $langs);
  for ($i=0; $i < sizeof($arr_langs); $i++)
    {
      $lng = $arr_langs[$i];
      delete_removefolders($node_path, $lng);
    }
}

function delete_removefolders($node_path, $lng)
{
  $book_id = WebApp::getSVar('docbook->book_id');
  $path = "$book_id/$lng/$node_path";
  $msg = date('Y-m-d H:i') . " >> Deleted by " . USER
    . " >> node_path='$node_path'";

  //remove from svn and commit
  shell("svn remove --force ".WS_BOOKS.$path);
  shell("svn update ".WS_BOOKS.$path);
  shell("svn commit ".WS_BOOKS.$path." -m '$msg'");

  //remove the workspace cache files
  shell("rm -rf ".WS_MEDIA.$path);
  shell("rm -rf ".WS_CACHE.$path);

  //update the public space files
  if (!book_fixed_to_tag($book_id, $lng))
    {
      //update xml files
      $book_dir = BOOKS."$book_id/$lng/";
      shell("svn update $book_dir");

      //update cache files
      shell("rm -rf ".MEDIA.$path);
      shell("rm -rf ".CACHE.$path);
    }
}

/** create a new node and node files */
function create_new_node($id, $title, $type)
{
  $book_id = WebApp::getSVar('docbook->book_id');
  $node_path = WebApp::getSVar('docbook->node_path');

  $new_node_path = $node_path.$id;

  // content.html, content.xml, state.txt and commit message 
  $content_html = '<html/>';
  $content_xml = "<?xml version='1.0' encoding='iso-latin-1' standalone='no'?>
<$type id='$id'>
  <title>$title</title>
  <para/>
</$type>";
  $state_txt = "unlocked::::\nadded:".USER.':'.EMAIL.':'.time();
  //commit message
  $msg_params = "parent_node='$node_path', new_node_id='$id'";
  $commit_msg = date('Y-m-d H:i'). ' >> Added by '.USER.' >> '.$msg_params;

  $langs = WebApp::getSVar('docbook->languages');
  $arr_langs = explode(',', $langs);
  for ($i=0; $i < sizeof($arr_langs); $i++)
    {
      $lng = $arr_langs[$i];
      $path = "$book_id/$lng/$new_node_path";

      //create the workspace xml directory
      $xml_path = WS_BOOKS.$path;
      mkdir($xml_path, 0775);
      write_file("$xml_path/content.xml", $content_xml);
      shell("svn add $xml_path");
      set_data_owner($xml_path);
      shell("svn commit $xml_path -m '$commit_msg'");
      write_file("$xml_path/state.txt", $state_txt);
      set_data_owner($xml_path);

      //create the workspace cache directory
      $cache_path = WS_CACHE.$path;
      mkdir($cache_path, 0775);
      write_file("$cache_path/content.html", $content_html);
      set_data_owner($cache_path);

      //create the directories and files in the public copy
      if (!book_fixed_to_tag($book_id, $lng))
        {
          //update the xml from svn
          $book_dir = BOOKS."$book_id/$lng/";
          shell("svn update $book_dir");
          set_data_owner(BOOKS.$path);

          //create the cache directory
          $cache_path = CACHE.$path;
          mkdir($cache_path, 0775);
          write_file("$cache_path/content.html", $content_html);
          set_data_owner($cache_path);
        }
    }
}

/** Returns true if the new id is OK, otherwise returns false. */
function change_id_validate($new_id)
{
  $node_path = WebApp::getSVar('docbook->node_path');
  $book_id = WebApp::getSVar('docbook->book_id');

  //get the current node_id 
  ereg('([^/]+)/$', $node_path, $regs);
  $old_id = $regs[1];
  if ($old_id=='.')  $old_id = $book_id;

  if ($new_id==$old_id)  return false;

  //the id of the root must not be changed
  if ($old_id==$book_id)  return false;

  //the id cannot be empty
  if ($new_id=='')
    {
      $msg = T_("The id cannot be empty.");
      WebApp::message($msg);
      return false;
    }

  //check that the new id does not already exist
  $output = process_index('edit/get_node_id.xsl', array('id'=>$new_id));
  if ($output<>'')
    {
      $msg = T_("The id 'v_new_id' is already used by another section.");
      $msg = str_replace('v_new_id', $new_id, $msg);
      WebApp::message($msg);
      return false;
    }

  return true;
}

/** move folders to the new node path, for all the languages */
function changeid_movefolders($node_path, $new_node_path)
{
  $book_id = WebApp::getSVar('docbook->book_id');
  $langs = WebApp::getSVar('docbook->languages');
  $arr_langs = explode(',', $langs);
  for ($i=0; $i < sizeof($arr_langs); $i++)
    {
      $lng = $arr_langs[$i];

      //move the folder in the workspace
      $node_folder = WS_BOOKS."$book_id/$lng/$node_path";
      $new_node_folder = WS_BOOKS."$book_id/$lng/$new_node_path";
      $parent_folder = ereg_replace('[^/]+/$', '', $new_node_folder);
      $msg_params = "node_path='$node_path', new_node_path='$new_node_path'";
      $msg = date('Y-m-d H:i').' >> Changed node id by '.USER.' >> '.$msg_params;
      $status = 'synchronized:'.USER.':'.EMAIL.':'.time();
      shell("svn update $parent_folder");
      shell("svn move --force $node_folder $new_node_folder");
      set_data_owner($parent_folder);
      shell("svn commit $parent_folder -m '$msg'");

      //cache
      $cache_folder = WS_CACHE."$book_id/$lng/$node_path";
      $new_cache_folder = WS_CACHE."$book_id/$lng/$new_node_path";
      shell("mv $cache_folder $new_cache_folder");

      //media
      $media_folder = WS_MEDIA."$book_id/$lng/$node_path";
      $new_media_folder = WS_MEDIA."$book_id/$lng/$new_node_path";
      shell("mv $media_folder $new_media_folder");

      //update the files in the public space
      if (!book_fixed_to_tag($book_id, $lng))
        {
          //xml
          $book_dir = BOOKS."$book_id/$lng/";
          shell("svn update $book_dir");
          $new_node_folder = $book_dir.$new_node_path;
          $parent_folder = ereg_replace('[^/]+/$', '', $new_node_folder);
          set_data_owner($parent_folder);

          //cache
          $cache_folder = CACHE."$book_id/$lng/$node_path";
          $new_cache_folder = CACHE."$book_id/$lng/$new_node_path";
          shell("mv $cache_folder $new_cache_folder");

          //media
          $media_folder = MEDIA."$book_id/$lng/$node_path";
          $new_media_folder = MEDIA."$book_id/$lng/$new_node_path";
          shell("mv $media_folder $new_media_folder");
        }
    }
}

/** set the new id in content.xml, for all the languages */
function changeid_setid($new_id)
{
  $book_id = WebApp::getSVar('doocbook->book_id');
  $node_path = WebApp::getSVar('docbook->node_path');

  $langs = WebApp::getSVar('docbook->languages');
  $arr_langs = explode(',', $langs);
  for ($i=0; $i < sizeof($arr_langs); $i++)
    {
      $lng = $arr_langs[$i];
      $xml_file = file_content_xml(WS_BOOKS, $node_path, $lng);

      //modify id in content.xml
      $params = "--stringparam id \"$new_id\" ";
      $xsl_file = XSLT."edit/set_content_id.xsl";
      $content = shell("xsltproc $params $xsl_file $xml_file");
      write_file($xml_file, $content);

      //commit in svn
      $commit_msg = date('Y-m-d H:i') . ' >> Changed node id by ' . USER
        . " >> node_path='$node_path' new_id='$new_id'";
      shell("svn commit $xml_file -m '$commit_msg'");

      //update the files in the public space
      if (!book_fixed_to_tag($book_id, $lng))
        {
          $book_dir = BOOKS."$book_id/$lng/";
          shell("svn update $book_dir");
          $public_xml_file = file_content_xml(BOOKS, $node_path, $lng);
          set_data_owner($public_xml_file);
        }
    }
}

function changeid_updatecache($node_path)
{
  //update the cache files for all the branch
  $book_id = WebApp::getSVar('docbook->book_id');
  $langs = WebApp::getSVar('docbook->languages');
  $arr_langs = explode(',', $langs);
  for ($i=0; $i < sizeof($arr_langs); $i++)
    {
      $lng = $arr_langs[$i];
      shell(CONTENT."cache/cache.sh $book_id $lng 'workspace' '$node_path'");
      if (!book_fixed_to_tag($book_id, $lng))
        {
          shell(CONTENT."cache/cache.sh $book_id $lng 'books' '$node_path'");
        }
    }

  //update the navigation files of the previous and next nodes
  $navig = get_arr_navigation($node_path);
  update_navigation_all_langs($navig['prev_path']);
  update_navigation_all_langs($navig['next_path']);      

  //update subnodes.html of the new node and the ancestors
  update_subnodes_html_all_langs($node_path);
}
?>