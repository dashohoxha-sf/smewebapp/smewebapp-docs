<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


/**
 * This file has some general functions.
 */

/*--------------- xml and html files -----------------------*/
/**
 * Returns the path of the content.xml for the given node
 * and language.
 */
function file_content_xml($xml_path =WS_BOOKS,
                          $node_path =UNDEFINED,
                          $lng =UNDEFINED)
{
  if ($node_path==UNDEFINED)
    {
      $node_path = WebApp::getSVar('docbook->node_path');
    }
  if ($lng==UNDEFINED)
    {
      $lng = WebApp::getSVar('docbook->lng');
    }

  $book_id = WebApp::getSVar('docbook->book_id');
  $book_path = $xml_path.$book_id.'/'.$lng.'/';
  $content_xml = $book_path.$node_path."content.xml";

  return $content_xml;
}

/**
 * Returns the path of the content_lng.html for the given node
 * and language.
 */
function file_content_html($cache_path =WS_CACHE,
                           $node_path =UNDEFINED,
                           $lng =UNDEFINED)
{
  if ($node_path==UNDEFINED)
    {
      $node_path = WebApp::getSVar('docbook->node_path');
    }
  if ($lng==UNDEFINED)
    {
      $lng = WebApp::getSVar('docbook->lng');
    }
  $book_id = WebApp::getSVar('docbook->book_id');
  $book_path = $cache_path.$book_id.'/'.$lng.'/';
  $content_html = $book_path.$node_path."content.html";

  return $content_html;
}

/** applies the given transformer to index.xml */
function process_index($transformer, $arr_params =array(),
                       $books =UNDEFINED, $lng =UNDEFINED)
{
  if ($books==UNDEFINED)
    {
      $books = (defined('EDIT') ? WS_BOOKS : BOOKS);
    }
  if ($lng==UNDEFINED)
    {
      $lng = WebApp::getSVar('docbook->lng');
    }

  $book_id = WebApp::getSVar('docbook->book_id');
  $xml_file = $books."$book_id/$lng/index.xml";

  //book_dir is relative to xsl file
  $book_dir = '../../'.dirname($xml_file).'/';

  $params = "--stringparam book_dir $book_dir ";
  while ( list($p_name, $p_value) = each($arr_params) )
    {
      $params .= "--stringparam $p_name \"$p_value\" ";
    }

  $xsl_file = XSLT.$transformer;
  //WebApp::debug_msg("xsltproc $params $xsl_file $xml_file");
  $output = shell("xsltproc $params $xsl_file $xml_file");

  return $output;
}

/**
 * Reads from cache 'navigation.txt' for the given node and 
 * returns an associative array with the template variables
 * this_path, next_path, prev_path, up_path, toc_path, 
 * this_title, next_title, prev_title, up_title, toc_title.
 */
function get_arr_navigation($node_path, $cache_path =UNDEFINED, $lng =UNDEFINED)
{
  if ($cache_path==UNDEFINED)
    {
      $cache_path = (defined('EDIT') ? WS_CACHE : CACHE);
    }
  if ($lng==UNDEFINED)
    {
      $lng = WebApp::getSVar('docbook->lng');
    }

  $arr_navigation = array();

  //get the navigation file
  $book_id = WebApp::getSVar('docbook->book_id');
  $book_path = $cache_path.$book_id.'/'.$lng.'/';
  $fname = $book_path.$node_path."navigation.txt";
  if (!file_exists($fname))
    {
      $arr_navigation['next_path'] = './';
      $arr_navigation['prev_path'] = './';
      $arr_navigation['up_path']   = './';
      return $arr_navigation;
    }

  //open it
  $lines = file($fname);

  //parse it
  for ($i=0; $i < sizeof($lines); $i++)
    {
      $line = $lines[$i];
      list($var_name,$var_value) = split('=', $line, 2);
      if ($var_name=='')  continue;
      $arr_navigation[$var_name] = trim($var_value);
    }

  return $arr_navigation;
}

/** Update the cache file content.html from content.xml */
function update_cache($public ='workspace', $node_path =UNDEFINED, $lng =UNDEFINED)
{
  if ($public=='public')
    {
      $xml_file = file_content_xml(BOOKS, $node_path, $lng);
      $html_file = file_content_html(CACHE, $node_path, $lng);
    }
  else  //workspace
    {
      $xml_file = file_content_xml(WS_BOOKS, $node_path, $lng);
      $html_file = file_content_html(WS_CACHE, $node_path, $lng);
    }
  $xsl_file = XSLT."cache/update_content.xsl";
  $content_html = shell("xsltproc $xsl_file $xml_file");
  write_file($html_file, $content_html);
}

/*--------------- state file ----------------------------*/
  
/** Returns the file state.txt of the current node. */
function get_state_filename()
{
  $book_id = WebApp::getSVar('docbook->book_id');
  $node_path = WebApp::getSVar('docbook->node_path');
  $lng = WebApp::getSVar('docbook->lng');
  $fname = WS_BOOKS.$book_id.'/'.$lng.'/'.$node_path."state.txt";
  return $fname;
}

/**
 * Reads the file state.txt of the current node and returns
 * an associative array with the variables found in it. This file
 * contains two lines. The first line is the lock line and contains
 * 'lock:mode:l_user:l_email:l_timestamp', where lock is 'locked' 
 * or 'unlocked' and mode is 'edit' or 'approve'.  The second line
 * is the modification line and it contains 
 * 'status:m_user:m_email:m_timestamp', where status is 'synchronized',
 * or 'modified'.
 * The keys of the associative array are: lock, mode, l_user, l_email,
 * l_timestamp, status, m_user, m_email, m_timestamp.
 */
function get_node_state()
{
  $fname = get_state_filename();
  if (!file_exists($fname))
    {
      list($lock, $mode, $l_user, $l_email, $l_timestamp)
	= array('unlocked', '', '', '', '');
      list($status,$m_user,$m_email,$m_timestamp)
        = array('none', '', '', '');
    }
  else
    {
      $lines = file($fname);
      list($lock, $mode, $l_user, $l_email, $l_timestamp)
	= explode(':', chop($lines[0]));
      list($status,$m_user,$m_email,$m_timestamp) 
	= explode(':', chop($lines[1]));
    }
  
  $arr_state = 
    compact('lock', 'mode', 'l_user', 'l_email', 'l_timestamp',
            'status', 'm_user', 'm_email', 'm_timestamp');
  return $arr_state;
}
  
function write_node_state($arr_state)
{
  extract($arr_state);
  $str_state = ( "$lock:$mode:$l_user:$l_email:$l_timestamp\n"
                 ."$status:$m_user:$m_email:$m_timestamp\n");
  $fname = get_state_filename();
  write_file($fname, $str_state);
}

/**
 * Set the status of the current node to the given new status.
 * The new status can be: synchronized or modified.
 */
function set_node_status($new_status)
{
  $arr_state = get_node_state();
  $arr_state['status'] = $new_status;
  $arr_state['m_user'] = USER;
  $arr_state['m_email'] = EMAIL;
  $arr_state['m_timestamp'] = time();
  write_node_state($arr_state);
}

/**
 * Lock or unlock the current node.
 * The $lock parameter can be 'locked' or 'unlocked',
 * and $mode can be 'edit' or 'approve'.
 */
function set_node_lock($lock, $mode)
{
  //make sure that the node is not locked by somebody else
  $arr_state = get_node_state();
  if (locked_by_somebody($arr_state))  return;

  $arr_state['lock'] = $lock;
  if ($lock=='locked')
    {
      $arr_state['mode'] = $mode;
      $arr_state['l_user'] = USER;
      $arr_state['l_email'] = EMAIL;
      $arr_state['l_timestamp'] = time();
    }
  write_node_state($arr_state);
}

/**
 * Returns true and displays a notification message,
 * if the node is locked by another user.
 */
function locked_by_somebody($arr_state =UNDEFINED)
{
  if ($arr_state==UNDEFINED)  $arr_state = get_node_state();
  if ($arr_state['l_user']==USER) return false;
  return is_locked($arr_state);
}

/** Returns true if the node is locked and the lock has not expired. */
function is_locked($arr_state)
{
  extract($arr_state);
  if ($lock=='unlocked')  return false;

  //check whether the lock has expired (more than 15 minutes ago
  //from the lock time and from the last modification)
  if ( (time() - $l_timestamp) > 15*60 )
    {
      if ($m_timestamp=='')  return false;
      if ( (time() - $m_timestamp) > 15*60 )  return false;
    }

  return true;
}

/**
 * Converts the given time stamp to a date string.
 * If the time is today, then give just hours and minutes,
 * otherwise give more information.
 */
function get_date_str($timestamp)
{
  $day = 24*60*60;  //nr of secs in a day
  $t = getdate(time());
  $midnight = mktime(1, 1, 0, $t['mon'], $t['mday'], $t['year']);
  $firstday = mktime(1, 1, 0, 1, 1, $t['year']);

  if ($timestamp > $midnight)
    return ('today at ' . date('H:i', $timestamp));

  if ($timestamp > $midnight - $day)
    return ('yesterday at ' . date('H:i', $timestamp));

  if ($timestamp > $midnight - 6*$day)
    return ('on ' . date('D H:i', $timestamp));

  if ($timestamp > $firstday)
    return ('on ' . date('M d, H:i', $timestamp));

  //older than the first day of this year
  return ('on ' . date('M d, Y', $timestamp));
}


/**
 * Check whether the public copy of the book is fixed to
 * a certain tag or revision and return it. If book is not
 * fixed, return false.
 */
function book_fixed_to_tag($book_id, $lng)
{
  $book_dir = BOOKS."$book_id/$lng/";

  if (file_exists($book_dir.'fixed'))
    {
      $lines = file($book_dir.'fixed');
      $tag = trim($lines[0]);
      return $tag;
    }
  else
    return false;
}
?>