<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class view extends WebObject
{
  function onParse()
    {
      $book_id = WebApp::getSVar('docbook->book_id');
      $node_path = WebApp::getSVar('docbook->node_path');
      $lng = WebApp::getSVar('docbook->lng');

      $cache = (defined('EDIT') ? WS_CACHE : CACHE);
      $book_path = $cache.$book_id.'/'.$lng.'/';
      $content_html = $book_path.$node_path."content.html";
      $subnodes_html = $book_path.$node_path."subnodes.html";

      WebApp::addVars(compact('content_html', 'subnodes_html'));

      //set the page_id of the webnotes
      ereg('([^/]+)/$', $node_path, $regs);
      $node_id = $regs[1];
      $page_id = "$book_id/$node_id/$lng";
      WebApp::setSVar('webnotes->page_id', $page_id);
    }
}
?>