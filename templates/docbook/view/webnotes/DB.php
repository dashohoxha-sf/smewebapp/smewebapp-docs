<?php
//connection parameters
$host   = 'localhost';
$user   = 'root';
$passwd = '';
$db     = 'webnotes';

//include database classes 
include_once DB_PATH."class.MySQLCnn.php";
include_once DB_PATH."class.Recordset.php";
include_once DB_PATH."class.EditableRS.php";
include_once DB_PATH."class.PagedRS.php";

//create a global connection variable
global $cnn;
$cnn = new MySQLCnn($host, $user, $passwd, $db);  
?>