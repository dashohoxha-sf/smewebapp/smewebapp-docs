// -*-C-*-
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function add_note()
{
  SendEvent('editnote', 'add');
}

function get_event_args()
{
  var form = document.editnote;
  var email = form.email.value;
  var note_text = form.note_text.value;
  //encode_arg_value() is used to escape the semicolumns (;) inside the text
  var event_args = "email="+email+";"+"note_text="+encode_arg_value(note_text);

  return event_args;
}

function save()
{
  SendEvent('editnote', 'save', get_event_args());
}

function preview()
{
  SendEvent('editnote', 'preview', get_event_args());
}

function cancel()
{
  var msg = T_("Any changes will be lost!");
  if (confirm(msg))  SendEvent('editnote', 'cancel');
}

function submit()
{
  SendEvent("editnote", "submit");
}
