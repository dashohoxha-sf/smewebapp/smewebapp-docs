<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class editnote extends WebObject
{
  function init()
    {
      $this->addSVar('state', 'hidden'); // hidden | edit | preview
      $this->addSVar('note_id', UNDEFINED);
    }

  function on_add($event_args)
    {
      $this->setSVar('state', 'edit');
      $this->setSVar('note_id', UNDEFINED);
    }

  function on_edit($event_args)
    {
      $this->setSVar('state', 'edit');
      $this->setSVar('note_id', $event_args['note_id']);
    }

  function on_save($event_args)
    {
      $params = $event_args;

      $allowed_tags = "<b><i><a><br><p><ol><ul><li><hr><pre><xmp>";
      $note = $params["note_text"];
      $params["note_text"] = strip_tags($note, $allowed_tags);
      $params["ip"] = $_SERVER["REMOTE_ADDR"];
      $params["date_modified"] = date("d-Mon-Y h:m");

      $note_id = $this->getSVar('note_id');
      if ($note_id==UNDEFINED)
        {
          $this->insert_new_note($params);
        }
      else
        {
          WebApp::execDBCmd("update_note", $params);
        }
    }

  function insert_new_note($params)
    {
      //add a new note record
      WebApp::execDBCmd("insert_new_note", $params);

      //get the note_id  of the new record
      $rs = WebApp::openRS("get_new_note_id", $params);
      $note_id = $rs->Field('note_id');

      //set it to the statevar note_id
      $this->setSVar('note_id', $note_id);

      //update the status from 'new' to 'edit'
      WebApp::execDBCmd("set_note_status", array('status'=>'edit'));
    }

  function on_preview($event_args)
    {
      $this->on_save($event_args);
      $this->setSVar('state', 'preview');
    }

  function on_cancel($event_args)
    {
      $this->setSVar('state', 'hidden');
    }

  function on_submit($event_args)
    {
      if (WEBNOTES_APPROVE)
        {
          WebApp::execDBCmd("set_note_status", array('status'=>'queued'));
          $this->send_email_notification();
          $msg = "Your note is queued for approval by a moderator.";
          WebApp::message($msg);
        }
      else
        {
          WebApp::execDBCmd("set_note_status", array('status'=>'approved'));
        }

      //hide the editnote webbox
      $this->setSVar('state', 'hidden');
    }

  /** send an email notification to the admins of the book */
  function send_email_notification()
    {
      if (!WEBNOTES_NOTIFY)  return;

      //build the message
      $note_id = WebApp::getSVar('webnotes->note_id');
      $rs = WebApp::openRS('get_note', array('note_id', $note_id));
      extract($rs->Fields());
      $message = "
page_id : $page_id
email   : $email
from ip : $ip
date    : $date_modified
status  : $status

note:
---------------- 
$note_text
";

      //get a list of emails of the admins of the book
      $book_id = WebApp::getSVar('docbook->book_id');
      $users = ADMIN.'access_rights/users';
      $output = shell("gawk -F: '(\$5 ~ \"$book_id\") {print \$4}' $users");
      $arr_admin_emails = explode("\n", trim($output));

      //send a notification message to each admin of the book
      $subject = "[webnotes] $page_id";
      for ($i=0; $i < sizeof($arr_admin_emails); $i++)
        {
          $email = $arr_admin_emails[$i];
          mail($email, $subject, $message);
        }
    }

  function onRender()
    {
      $note_id = $this->getSVar('note_id');
      if ($note_id==UNDEFINED)
        {
          $email = (defined('EMAIL') ? EMAIL : '');
          WebApp::addVars(array(
                                'email' => $email,
                                'note_text' => ''
                                ));
        }
      else
        {
          $rs = WebApp::openRS("get_note");

          //change the date format
          $date = $rs->Field('date_modified');
          $date = date('d-M-Y G:i', strtotime($date));
          $rs->setFld('date_modified', $date);

          WebApp::addVars($rs->Fields());
        }
    }
}
?>