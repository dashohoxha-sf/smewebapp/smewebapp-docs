<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

//include DB support
if (WEBNOTES=='true')  include_once dirname(__FILE__).'/DB.php';

class webnotes extends WebObject
{
  function init()
    {
      $this->addSVar('page_id', '');
    }

  function on_next($event_args)
    {
      $page = $event_args["page"];
      WebApp::setSVar("get_all_page_notes->current_page", $page);
      WebApp::setSVar("get_approved_page_notes->current_page", $page);
    }

  function on_set_status($event_args)
    {
      WebApp::execDBCmd("set_note_status", $event_args);
    }

  function on_delete($event_args)
    {
      if (ADMIN)
        {
          WebApp::execDBCmd("delete_note", $event_args);
        }
      else
        {
          $msg = T_("Only an admin can delete a note!");
          WebApp::message($msg);
        }
    }

  function onRender()
    {
      //add {{notes_rs}}
      $approve = WebApp::getVar('approve');
      $is_admin = WebApp::getVar('is_admin');
      if ($approve=='true' or $is_admin=='true')
        {
          $notes_rs = "get_all_page_notes";
        }
      else
        {
          $notes_rs = "get_approved_page_notes";
        }
      WebApp::addVar('notes_rs', $notes_rs);
      WebApp::setSVar($notes_rs.'->recount', "true");

      //change the date format
      $rs = WebApp::openRS($notes_rs);
      while (!$rs->EOF())
        {
          $date = $rs->Field('date_modified');
          $date = date('d-M-Y G:i', strtotime($date));
          $rs->setFld('date_modified', $date);
          $rs->MoveNext();
        }
      global $webPage;
      $webPage->addRecordset($rs);
    }
}
?>