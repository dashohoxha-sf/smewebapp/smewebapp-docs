-- MySQL dump 8.23
--
-- Host: localhost    Database: webnotes
---------------------------------------------------------
-- Server version	3.23.58

--
-- Current Database: webnotes
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ webnotes;

USE webnotes;

--
-- Table structure for table `webnotes`
--

CREATE TABLE webnotes (
  note_id int(10) unsigned NOT NULL auto_increment,
  page_id varchar(255) NOT NULL default '',
  email varchar(100) default '',
  ip varchar(100) default '',
  date_modified datetime default '0000-00-00 00:00:00',
  status varchar(100) default '',
  note_text text,
  PRIMARY KEY  (note_id)
) TYPE=MyISAM;

