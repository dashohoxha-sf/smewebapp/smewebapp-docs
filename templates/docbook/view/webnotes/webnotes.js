// -*-C-*-
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function edit_note(note_id)
{
  var event_args = 'note_id='+note_id+';'+'status='+status;
  SendEvent('editnote', 'edit', event_args);
}

function delete_note(note_id)
{
  var msg = T_("You are deleting the note #v_note_id from the database!");
  msg = msg.replace(/v_note_id/, note_id);
  if (confirm(msg))
    {
      SendEvent('webnotes', 'delete', 'note_id='+note_id);
    }
}

function set_note_status(note_id, status)
{
  var event_args = 'note_id='+note_id+';'+'status='+status;
  SendEvent('webnotes', 'set_status', event_args);
}
