<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class filternotes extends WebObject
{
  function init()
    {
      $this->addSVar('email', '');
      $this->addSVar('status', '');
      $this->addSVar('condition', '(1=1)');
    }

  function onParse()
    {
      $this->setSVar('condition', $this->get_filter_condition());
    }

  function onRender()
    {
      //add the recordset status
      $rs = new EditableRS('status_rs');
      $rs->addRec( array('id'=>'', 'label'=>'') );
      $rs->addRec( array('id'=>'edit', 'label'=>'edit') );
      $rs->addRec( array('id'=>'queued', 'label'=>'queued') );
      $rs->addRec( array('id'=>'approved', 'label'=>'approved') );
      $rs->addRec( array('id'=>'declined', 'label'=>'declined') );
      $rs->addRec( array('id'=>'archived', 'label'=>'archived') );
      global $webPage;
      $webPage->addRecordset($rs);
    }

  function get_filter_condition()
    {
      $email = $this->getSVar('email');
      $status = $this->getSVar('status');

      $conditions = array();
      if ($email!='')  $conditions[] = "(email LIKE '%$email%')";
      if ($status!='')  $conditions[] = "(status = '$status')";

      $filter_condition = implode(' AND ', $conditions);
      if (trim($filter_condition)=='')  $filter_condition = '1=1';

      return $filter_condition;
    }
}
?>