// -*-C-*-
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function edit_menu1(visible)
{
  SendEvent('main','edit_menu');
}

function search1()
{
  var form = document.form_search;
  var words = form.search_words.value;
  SendEvent('main', 'search', 'words='+words);
}

function download(format)
{
  var book_id = session.getVar('docbook->book_id');
  var lng = session.getVar('docbook->lng');
  var url = 'content/downloads/tar_gz/' + book_id + '/' + lng + '/'
    + book_id + '_' + lng + '_' + format + '.tar.gz';
  window.location = url;
}
