#!/bin/bash
### When the path of the directory 'content/' (this directory)
### changes (e.g. when the application is moved to another path),
### the path of the subversion repositories changes as well,
### since they are inside this directory. As a result, the svn
### working copies (sandboxes) do not work anymore, they still
### are connected to the old repositories.
###  
### This script is used to fix this problem. It changes the URL 
### of the SVN working copies according to the new path of 'content/'.

### go to this directory
cd $(dirname $0)

old_path=/var/www/html/tools/docs_1/content
new_path=/var/www/html/tools/docs/content

function relocate_all
{
  until [ -z "$1" ]  # until all parameters used up...
  do
    local book_id=$1
    local lng=$2
    echo "relocating $book_id $lng"
    relocate_book $book_id $lng
    shift
    shift
  done
}

function relocate_book
{
  local book_id=$1
  local lng=$2

  # get the old and new urls
  path="books/xml/$book_id/$lng/"
  old_url=$(svn info $path | grep 'URL:' | gawk '{print $2}')
  pwd=$(pwd)
  new_url="file://$pwd/books/svn/$book_id/$lng/trunk"

  # relocate the public and workspace copies 
  svn switch --relocate $old_url $new_url  books/xml/$book_id/$lng/
  svn switch --relocate $old_url $new_url  workspace/xml/$book_id/$lng/
}

### get a list of all the books and relocate them
book_list=$(gawk -F: '{print $1" "$2}' books/book_list)
relocate_all $book_list

### relocate the copy in SVN and 'xml_source/' in downloads
pwd=$(pwd)
. SVN/svn_dir.txt  ## get the variable $svn_dir
old_url=$(SVN/get_url.sh)
new_url="file://$pwd/SVN/repository/$svn_dir/trunk"
echo "relocating SVN/$svn_dir"
svn switch --relocate $old_url $new_url SVN/$svn_dir
echo "relocating downloads/xml_source"
svn switch --relocate $old_url $new_url downloads/xml_source


