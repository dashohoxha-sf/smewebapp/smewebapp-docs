#!/bin/bash
### implode all the chunks and create a single xml file
### can implode either the public or the workspace copy of the book

### go to this dir
cd $(dirname $0)

if [ "$1" = "" ]
then
  echo "Usage: $0 book-id [lng] [books_path]"
  echo "'books_path' is either 'books' or 'workspace'"
  exit 1;
fi

book_id=$1
lng=${2:-en}
books=${3:-'books'}

### create a temporary directory
tmp_dir=tmp/$book_id
rm -rf $tmp_dir
mkdir -p $tmp_dir

### copy index.xml and the content.xml chunks to $tmp_dir
book_dir=../$books/xml/$book_id/$lng
cp -rf $book_dir/* $tmp_dir/

### preprocess xml chunks before imploding (extracting <![CDATA[...]]> etc.)
./pre_process.php $tmp_dir

### implode the xml chunks
output_file=$tmp_dir.xml
index_xml=$tmp_dir/index.xml
implode_xsl=../../xsl_transform/implode/implode.xsl
book_dir=../../content/implode/$tmp_dir  #book_dir is relative to $implode_xsl
echo "Imploding '$book_id/$lng' to $(dirname $0)/$output_file"
xsltproc -o $output_file \
         --stringparam book_dir $book_dir/ \
         $implode_xsl $index_xml

### postprocess the imploded xml file (put back <![CDATA[...]]> etc.)
./post_process.php $tmp_dir

### clean the temporary directory
rm -rf $tmp_dir
../../chown_norec.sh tmp/

### set proper ownership to the xml file
../../chown.sh $output_file
