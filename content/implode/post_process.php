#!/usr/bin/php -q
<?php
/**
 * This script replaces <cdata>nr</cdata> and <comment>nr</comment> 
 * by the corresponding <![CDATA[...]]> and <!--...-->
 * in the imploded xml file that is passed as argument.
 * <![CDATA[...]]> and <!--...--> are read from files where they
 * have been saved before by the script implode_strip_cdata.php
 */

if ($argc < 2)
{
  print "Usage: $argv[0] book_dir \n";
  exit(1);
}
$dir = $argv[1];

//get the contents of the file
$fname = $dir.'.xml';
$fcontents = file_get_contents($fname);

//replace <cdata>x</cdata> by the corresponding <![CDATA[...]]>
$arr_cdata = unserialize(file_get_contents("$dir/cdata.txt"));
for ($i=0; $i < sizeof($arr_cdata); $i++)
{
  $cdata = $arr_cdata[$i];
  $fcontents = str_replace("<cdata>$i</cdata>", $cdata, $fcontents);
}

//replace <comment>x</comment> by the corresponding <!--...-->
$arr_comments = unserialize(file_get_contents("$dir/comments.txt"));
for ($i=0; $i < sizeof($arr_comments); $i++)
{
  $fcontents = str_replace("<comment>$i</comment>", $arr_comments[$i], $fcontents);
}

//write back the modified xml file
$fp = fopen($fname, 'w');
fputs($fp, $fcontents);
fclose($fp);
?>
