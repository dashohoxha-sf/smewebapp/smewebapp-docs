#!/bin/bash

### Explode the given DocBook file into XML chunks,
### import them in SVN, and checkout a copy in the workspace.
### Generate also the cache files.

### go to this dir
cd $(dirname $0)

if [ "$1" = "" ]
then
  echo "Usage: $0 xml-file [book_id] [lng]"
  echo "where book-id is the id of the book"
  echo "and lng can be: en, en_US, en_US.UTF-8, etc. (default is en)"
  echo "Important: the path of the xml-file should be either absolute,"
  echo "           or relative to the 'content/' directory."
  exit 1
fi
xml_file=$1
book_id=$2
lng=${3:-en}  #default is english

### check that the xml-file does exist
if [ ! -f $xml_file ]
then
  echo "Error: file not found: $xml_file"
  echo "Important: the path of the xml-file should be either absolute,"
  echo "           or relative to the 'content/' directory."
  exit 2
fi

if [ "$book_id" = "" ]
then
  # get the book_id and lng from the attributes
  # id and lang of the xml file
  xslt=../xsl_transform/explode
  book_id=$(xsltproc $xslt/get_id.xsl $xml_file)
  lng=$(xsltproc $xslt/get_lang.xsl $xml_file)
  lng=${lng:-en}  #default is english
fi 

### clean first, in case there is an existing copy
./clean.sh $book_id $lng 

### explode it
explode/explode.sh $xml_file $book_id

### create a svn repository for this book and language
repository=books/svn/$book_id/$lng
echo "Creating svn repository $repository"
mkdir -p $repository
svnadmin create $repository

### import into books/svn/book_id/lng/
path=explode/tmp/$book_id
url=file://$(pwd)/$repository/trunk
echo "Importing into:"
echo $url
svn import $path $url -q -m ""
../chown.sh $repository
../chown_norec.sh books/svn/$book_id/

### make a directory for tags in the repository 
svn mkdir -m'directory for tags' file://$(pwd)/$repository/tags

### checkout into books/xml/
path=books/xml/$book_id/$lng/
echo "Checking out to $path"
svn checkout $url $path -q
../chown.sh $path
../chown_norec.sh books/xml/$book_id/

### checkout into workspace/xml/
path=workspace/xml/$book_id/$lng/
if [ ! -d workspace/xml/$book_id/ ]
then
  mkdir -p workspace/xml/$book_id/
  ../chown.sh workspace/xml/$book_id/
fi
echo "Checking out to $path"
svn checkout $url $path -q
../chown.sh $path

### create the cache files
echo "Creating cache files in books/cache/$book_id/$lng/"
cache/cache.sh $book_id $lng 'books'
echo "Creating cache files in workspace/cache/$book_id/$lng/"
cache/cache.sh $book_id $lng 'workspace'

### get ADMIN_EMAIL from books.conf
.  ../books.conf
user=$(whoami)
email=$ADMIN_EMAIL

### create a state file state.txt for each section in the workspace
path=workspace/xml/$book_id/$lng/
echo "Creating files state.txt in $path"
timestamp=$(date +%s)
section_list=$(find $path -type d ! -path '*/.svn*')
for section in $section_list
do
  state_file="$section/state.txt"
  echo "unlocked::::" > $state_file
  echo "imported:$user:$email:$timestamp" >> $state_file
  ../chown.sh $state_file
done

### create folders for media files (empty for the time being)
mkdir -p books/media/$book_id/$lng
../chown.sh books/media/$book_id/$lng
../chown_norec.sh books/media/$book_id/
mkdir -p workspace/media/$book_id/$lng
../chown.sh workspace/media/$book_id/$lng
../chown_norec.sh workspace/media/$book_id/

### append book to the book_list
xslt=../xsl_transform/explode
book_title=$(xsltproc $xslt/get_title.xsl $xml_file)
echo "$book_id:$lng:$book_title" >> books/book_list
../chown_norec.sh books/book_list

### done
echo "----------"

