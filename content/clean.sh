#!/bin/bash
### remove a book or all the books

# go to this dir
cd $(dirname $0)

if [ "$1" = "" ]
then
  echo "Usage: $0 [book_id | all] [lng]"
  exit 1;
fi

book_id=$1
lng=$2

if [ "$1" = "all" ]
then
  book_id=''
  bookid_lng=''
else
  bookid_lng=$book_id/$lng
fi

dir_list="books/xml
          books/media
          books/cache
          books/svn
          workspace/xml
          workspace/media
          workspace/cache
          downloads/tar_gz
          downloads/formats"
for dir in $dir_list
do
  rm -rf $dir/$bookid_lng
done

### remove the book from books/book_list
if [ "$1" = "all" ]
then sedexp="/./d"
else sedexp="/^$book_id:$lng/d"
fi
if [ -f books/book_list ]
then
  sed -i $sedexp books/book_list
  ../chown_norec.sh books/book_list
fi

### clean explode/tmp/, implode/tmp/
rm -rf explode/tmp/$book_id implode/tmp/$book_id

### if clean all, then clean some additional things
if [ "$1" = "all" ]
then
  # clean docs from SVN
  rm -rf SVN/{my_docs,repository,svn_dir.txt,post-commit}
  rm -rf downloads/xml_source/

  # clean books/, workspace/
  rm -rf books/ workspace/

  # clean search indexes
  rm -f ../search/*.index*
fi

