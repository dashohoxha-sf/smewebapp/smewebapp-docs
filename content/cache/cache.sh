#!/bin/bash

### Creates the files content.html, subnodes.html
### and navigation.txt for each section, which are
### used to construct the html page of the section.

### go to this dir
cd $(dirname $0)

if [ "$1" = "" ]
then
  echo "Usage: $0 book-id [lng] [books-path] [node-path]"
  echo "'books-path' is either 'books' or 'workspace'"
  echo "'node-path' is the starting node (default is root './')"
  exit 1;
fi

book_id=$1
lng=${2:-en}
books=${3:-"books"}
node_path=${4:-'./'}

index_xml=../$books/xml/$book_id/$lng/index.xml
cache_path=../$books/cache/$book_id/$lng
book_dir=../../content/$books/xml/$book_id/$lng/   # relative to xsl file
media_dir=content/$books/media/$book_id/$lng/      # relative to the app root
xslt=../../xsl_transform/cache

rm -rf $cache_path/${node_path:2}
mkdir -p $cache_path

### create the files 'content.html' for each node in the index
xsltproc -o $cache_path/unused.ignore \
         --stringparam book_dir $book_dir \
         --stringparam media_dir $media_dir \
         --stringparam path "$node_path" \
         $xslt/content-html.xsl $index_xml

### create the files 'subnodes.html' for each node in the index
xsltproc -o $cache_path/unused.ignore \
         --stringparam book_dir $book_dir \
         --stringparam path "$node_path" \
         $xslt/subnodes-html.xsl $index_xml

### create the files 'navigation.txt' for each node in the index
xsltproc -o $cache_path/unused.ignore \
         --stringparam book_dir $book_dir \
         --stringparam path "$node_path" \
         $xslt/navigation-txt.xsl $index_xml

### convert the html files to xhtml
### this script is needed because xsltproc writes out HTML, not XHTML.
find $cache_path/ -name "*.html" | xargs ./html2xhtml.php

### set proper ownership, so that apache can write them
../../chown.sh $cache_path/
../../chown_norec.sh ../$books/cache/$book_id/