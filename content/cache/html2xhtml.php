#!/usr/bin/php -q
<?php
/**
 * This script is called to convert HTML chunks to XHTML, 
 * after they are created by an xsltproc, because xsltproc
 * writes out HTML, not XHTML.
 * It could have been a shell script as well (using sed or
 * other shell tools). 
 */
for ($i=1; $i < $argc; $i++)
{
  $fname = $argv[$i];
  //print "$fname\n";

  $fcontents = file_get_contents($fname);
  $fcontents = reformat($fcontents);

  $fp = fopen($fname, 'w');
  fputs($fp, $fcontents);
  fclose($fp);
}

function reformat($str)
{
  //remove the DOCTYPE declaration
  $str = preg_replace('#^<!DOCTYPE [^>]+>#', '', $str);

  //match any starting or ending tag and make it lowercase
  $pattern = '#</?(\w+)[^<>]*>#';
  $str = preg_replace_callback($pattern, 'lowercase', $str);

  //handle empty tags
  $empty_tag = '(br|hr|img|input|meta|link|include|parameter)';
  $pattern = '#(<'.$empty_tag.'([^>]*[^/-])?)>#';
  $str = preg_replace($pattern, '$1 />', $str);

  /*
  //replace the xml builtin entities
  $str = preg_replace('#&(lt|gt|quot|apos);#', '&amp;$1;', $str);
  //convert also &amp; but not those that are
  //the result of a previous conversion
  $str = preg_replace('#&amp;(?!(amp|lt|gt|apos);)#', '&amp;amp;', $str);
  */

  //replace compact attributes by their xhtml equivalent
  $compact = '(nowrap|checked|disabled|readonly|selected|noresize)';
  $pattern = '#(<\w+[^<>]+\s+)'.$compact.'(?!\s*=)#';
  $str = preg_replace($pattern, '${1}$2="$2"', $str);

  //this is not about XHTML, it is because { and } are written out
  //as %7B and %7D by <xalanredirect:write> (during explosion)
  $str = str_replace('%7B%7B', '{{', $str);
  $str = str_replace('%7D%7D', '}}', $str);

  return $str;
}

function lowercase($matches)
{
  $str = str_replace($matches[1], strtolower($matches[1]), $matches[0]);
  return $str;
}
?>
