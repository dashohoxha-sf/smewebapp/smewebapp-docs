#!/usr/bin/php -q
<?php
/**
 * This script replaces <cdata>nr</cdata> and <comment>nr</comment> 
 * by the corresponding <![CDATA[...]]> and <!--...-->
 * in the exploded xml chunks.
 * <![CDATA[...]]> and <!--...--> are read from files where they
 * have been saved before by the preprocessing script.
 */

if ($argc < 2)
{
  print "Usage: $argv[0] book_dir \n";
  exit(1);
}
$dir = $argv[1];

$arr_cdata = unserialize(file_get_contents("$dir/cdata.txt"));
$arr_comments = unserialize(file_get_contents("$dir/comments.txt"));

$output = shell_exec("find $dir -name 'content.xml'");
$arr_files = explode("\n", $output);

for ($i=0; $i < sizeof($arr_files); $i++)
{
  $fname = $arr_files[$i];
  if (trim($fname)=='')  continue;

  $fcontents = file_get_contents($fname);
  $fcontents = put_cdata($fcontents);
  $fcontents = put_comments($fcontents);
  $fcontents = put_amp($fcontents);

  $fp = fopen($fname, 'w');
  fputs($fp, $fcontents);
  fclose($fp);
}

exit(0);

/** replace <cdata>x</cdata> by the corresponding <![CDATA[...]]> */
function put_cdata($str)
{
  global $arr_cdata;

  //strip empty space around <cdata>x</cdata>
  $str = preg_replace('#\s+(<cdata>\\d+</cdata>)\s+#', '\\1', $str);

  preg_match_all('#<cdata>(\\d+)</cdata>#', $str, $matches);
  $arr_numbers = $matches[1];

  for ($i=0; $i < sizeof($arr_numbers); $i++)
    {
      $nr = $arr_numbers[$i];
      $cdata = $arr_cdata[$nr];
      $str = str_replace("<cdata>$nr</cdata>", $cdata, $str);
    }

  return $str; 
}

/** replace <comment>x</comment> by the corresponding <!--...--> */
function put_comments($str)
{
  global $arr_comments;

  preg_match_all('#<comment>(\\d+)</comment>#', $str, $matches);
  $arr_numbers = $matches[1];
  for ($i=0; $i < sizeof($arr_numbers); $i++)
    {
      $nr = $arr_numbers[$i];
      $comment = $arr_comments[$nr];
      $str = str_replace("<comment>$nr</comment>", $comment, $str);
    }

  return $str;
}

/** replace &xyz; by &amp;xyz; */
function put_amp($str)
{
  $str = preg_replace('#&(\w+);#', '&amp;$1;', $str);
  return $str;
}
?>
