#!/usr/bin/php -q
<?php
/**
 * This script replaces <![CDATA[...]]> by <cdata>nr</cdata>
 * and <!--...--> by <comment>nr</comment> in the xml file
 * before it is exploded. 
 * The stripped cdata and comments are saved in files so that 
 * they can be placed back later in the exploded XML chunks.
 */

if ($argc < 2)
{
  print "Usage: $argv[0] output_dir file.xml \n";
  exit(1);
}
$dir = $argv[1];
$fname = $argv[2];

//get the contents of the file
$fcontents = file_get_contents($fname);

//replace <![CDATA[...]]> by <cdata>x</cdata>
preg_match_all('#<!\[CDATA\[.*?]]>#s', $fcontents, $matches);
$arr_cdata = $matches[0];
for ($i=0; $i < sizeof($arr_cdata); $i++)
{
  $fcontents = str_replace($arr_cdata[$i], "<cdata>$i</cdata>", $fcontents);
}

//replace <!--...--> by <comment>x</comment>
preg_match_all('#<!--.*?-->#s', $fcontents, $matches);
$arr_comments = $matches[0];
for ($i=0; $i < sizeof($arr_comments); $i++)
{
  $fcontents = str_replace($arr_comments[$i], "<comment>$i</comment>", $fcontents);
}

//write the modified xml file
$fname = $dir.'.xml';
$fp = fopen($fname, 'w');
fputs($fp, $fcontents);
fclose($fp);

//save arrays for the post-processing script
$fname = "$dir/cdata.txt";
$fp = fopen($fname, 'w');
fputs($fp, serialize($arr_cdata));
fclose($fp);
$fname = "$dir/comments.txt";
$fp = fopen($fname, 'w');
fputs($fp, serialize($arr_comments));
fclose($fp);
?>