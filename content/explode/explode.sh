#!/bin/bash

### Explodes an XML/DocBook file into chunks of xml files
### according to chapters, sections, etc. Creates also
### the file index.xml which contains the structure of
### the sections, their id-s and their paths.

### go to the 'content/' dir
cd $(dirname $0)/..

if [ "$1" = "" ]
then
  echo "Usage: $0 file.xml [book_id]"
  echo "where file.xml is the file to be exploded"
  echo "and book-id is the id of the book"
  echo "Important: the path of file.xml should be either absolute,"
  echo "           or relative to the 'content/' directory."
  exit 1
fi

original_xml_file=$1
book_id=$2

### check that the original_xml_file does exist
if [ ! -f $original_xml_file ]
then
  echo "Error: file not found: $original_xml_file"
  echo "Important: the path of the file.xml should be either absolute,"
  echo "           or relative to the 'content/' directory."
  exit 2
fi

xslt=../xsl_transform/explode

### if book_id is not passed as a parameter, get it 
### from the attribute 'id' of the xml file
if [ "$book_id" = "" ]
then
  book_id=$(xsltproc $xslt/get_id.xsl $original_xml_file)
fi

### if book_id is still undefined, stop processing
if [ "$book_id" = "" ]
then
  echo "Error: book_id is undefined."
  exit 2;
fi

echo "Exploding '$book_id' $original_xml_file"

tmp_dir=explode/tmp/$book_id
rm -rf $tmp_dir
mkdir -p $tmp_dir
xml_file=${tmp_dir}.xml

### pre-process for extracting <![CDATA[...]]> etc.
cd explode/
if   [ "${original_xml_file:0:1}" = "/" ]  # it is an absolute path
then preprocess_xml_file=$original_xml_file
else preprocess_xml_file=../$original_xml_file
fi
./pre_process.php tmp/$book_id $preprocess_xml_file
cd ..

### convert sectX elements to section elements
xsltproc -o $xml_file $xslt/sect2section.xsl $xml_file

### write the files 'content.xml' for each section
xsltproc -o $tmp_dir/unused.ignore $xslt/content-xml.xsl $xml_file

### create the file 'index.xml'
xsltproc -o $tmp_dir/index.xml $xslt/index-xml.xsl $xml_file

### post-process the xml chunks in order to put back <![CDATA[...]]> etc.
cd explode/
./post_process.php tmp/$book_id
cd ..

### clean intermediate files
rm $tmp_dir/{cdata.txt,comments.txt} $xml_file

### set proper ownership
../chown.sh $tmp_dir
../chown_norec.sh explode/tmp/
