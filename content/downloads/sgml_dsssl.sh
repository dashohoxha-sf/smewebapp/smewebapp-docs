#!/bin/bash

### convert to other formats using SGML tools and DSSSL stylesheets
### (docbook2html, docbook2pdf, etc.)

### go to this dir
cd $(dirname $0)

function usage
{
  echo "Usage: ${0} book.xml [ html | rtf | pdf | ps | tex | txt ]"
  exit 1
}

if [ $# -ne 2 ]; then usage; fi
echo "--> $0 $1 $2"

book_xml=$1
format=$2

xslt=../../xsl_transform/explode

# get the book_id and lng from the attributes
# id and lang of the xml file
book_id=$(xsltproc $xslt/get_id.xsl $book_xml)
book_id=${book_id:-unknown}
lng=$(xsltproc $xslt/get_lang.xsl $book_xml)
lng=${lng:-en}  #default is english

# make a small modification in DOCTYPE declaration, remove the URL of DTD
mkdir -p formats/tmp/
xml_file=formats/tmp/${book_id}_${lng}_dsssl.xml
sed '3s/$/>/; 4d' $book_xml > $xml_file

output_folder=formats/$book_id/$lng/$format
mkdir -p $output_folder

stylesheets=/usr/share/sgml/docbook/dsssl-stylesheets

case "$format" in
  html )
     output_folder=$output_folder/${book_id}_${lng}_dsssl
     rm -f $output_folder/media  #this is a symbolic link
     rm -rf $output_folder
     #docbook2html -o $output_folder -d $stylesheets/html/ldp.dsl#html $xml_file
     docbook2html -o $output_folder $xml_file
     ### create a link to the media files of the book
     ln -s $(pwd)/../books/media/$book_id/$lng/ $output_folder/media
     ;;
  html1 )
     output_folder=$output_folder/${book_id}_${lng}_dsssl
     rm -f $output_folder/media  #this is a symbolic link
     rm -rf $output_folder
     docbook2html -o $output_folder --nochunks $xml_file
     ### create a link to the media files of the book
     ln -s $(pwd)/../books/media/$book_id/$lng/ $output_folder/media
     ;;
  rtf )
     #docbook2rtf -o $output_folder -d $stylesheets/print/ldp.dsl#print $xml_file
     docbook2rtf -o $output_folder $xml_file
     ;;
  pdf | ps | tex | texi | txt )
     convert=docbook2$format
     $convert -o $output_folder $xml_file
     ;;
  * )
     echo "Cannot handle format '$format'."
     usage
     ;;
esac
