#!/bin/bash
### generate the formats and the downloads for the given book_id and lng

### go to this dir
cd $(dirname $0)

if [ "$1" = "" ]
then
  echo "Usage: $0 book_id [lng]"
  exit 1;
fi

book_id=$1
lng=${2:-en}

book_xml="xml_source/${book_id}_${lng}.xml"
path=tar_gz/$book_id/$lng
mkdir -p $path

### create a link to the media files
ln -s -f ../../books/media/$book_id/$lng xml_source/media

### xml
echo "========== ${book_id}_${lng}_xml.tar.gz =========="
tar cfz $path/${book_id}_${lng}_xml.tar.gz \
        -C xml_source/ \
        ${book_id}_${lng}.xml

### html 
echo "========== ${book_id}_${lng}_html.tar.gz =========="
./xml_xsl.sh $book_xml html
./sgml_dsssl.sh $book_xml html
tar chfz $path/${book_id}_${lng}_html.tar.gz \
         -C formats/$book_id/$lng/html/ \
         ${book_id}_${lng}_xsl/ ${book_id}_${lng}_dsssl/

### html1
echo "========== ${book_id}_${lng}_html1.tar.gz =========="
./xml_xsl.sh $book_xml html1
./sgml_dsssl.sh $book_xml html1
tar chfz $path/${book_id}_${lng}_html1.tar.gz \
         -C formats/$book_id/$lng/html1/ \
         ${book_id}_${lng}_xsl/ \
         ${book_id}_${lng}_dsssl/

### pdf
echo "========== ${book_id}_${lng}_pdf.tar.gz =========="
./xml_xsl.sh $book_xml pdf
./sgml_dsssl.sh $book_xml pdf
tar cfz $path/${book_id}_${lng}_pdf.tar.gz \
        -C formats/$book_id/$lng/pdf/ \
        ${book_id}_${lng}_xsl.pdf \
        ${book_id}_${lng}_dsssl.pdf

### rtf
echo "========== ${book_id}_${lng}_rtf.tar.gz =========="
./sgml_dsssl.sh $book_xml rtf
tar cfz $path/${book_id}_${lng}_rtf.tar.gz \
        -C formats/$book_id/$lng/rtf/ \
        ${book_id}_${lng}_dsssl.ps

### txt
echo "========== ${book_id}_${lng}_txt.tar.gz =========="
./xml_xsl.sh $book_xml txt
./sgml_dsssl.sh $book_xml txt
tar cfz $path/${book_id}_${lng}_txt.tar.gz \
        -C formats/$book_id/$lng/txt/ \
        ${book_id}_${lng}_xsl.txt \
        ${book_id}_${lng}_dsssl.txt

### ps
echo "========== ${book_id}_${lng}_ps.tar.gz =========="
./xml_xsl.sh $book_xml ps
./sgml_dsssl.sh $book_xml ps
tar cfz $path/${book_id}_${lng}_ps.tar.gz \
        -C formats/$book_id/$lng/ps/ \
        ${book_id}_${lng}_xsl.ps \
        ${book_id}_${lng}_dsssl.ps

### tex
#echo "========== ${book_id}_${lng}_tex.tar.gz =========="
#./sgml_dsssl.sh $book_xml tex
#tar cfz $path/${book_id}_${lng}_tex.tar.gz \
#        -C formats/$book_id/$lng/tex/ \
#        ${book_id}_${lng}_dsssl.tex

