#!/bin/bash

### convert to other formats using XML tools and XSL stylesheets (xmlto)

### go to this dir
cd $(dirname $0)

function usage
{
  echo "Usage: ${0} book.xml [ html | html1 | pdf | ps | txt ]"
  exit 1
}

if [ $# -ne 2 ]; then usage; fi
echo "--> $0 $1 $2"

book_xml=$1
format=$2

xslt=../../xsl_transform/explode

### get the book_id and lng from the attributes
### id and lang of the xml file
book_id=$(xsltproc $xslt/get_id.xsl $book_xml)
book_id=${book_id:-unknown}
lng=$(xsltproc $xslt/get_lang.xsl $book_xml)
lng=${lng:-en}  #default is english

mkdir -p formats/tmp/
xml_file=formats/tmp/${book_id}_${lng}_xsl.xml
cp $book_xml $xml_file
output_folder=formats/$book_id/$lng/$format/
mkdir -p $output_folder

stylesheets=/usr/share/sgml/docbook/xsl-stylesheets

case "$format" in
  html )
     output_folder=$output_folder/${book_id}_${lng}_xsl
     rm -f $output_folder/media  #this is a symbolic link
     rm -rf $output_folder
     xmlto xhtml -v -o $output_folder $xml_file
     #xmlto html -v -o $output_folder \
     #              -x $stylesheets/html/tldp-html-chunk.xsl \
     #              $xml_file
     ### create a link to the media files of the book
     ln -s $(pwd)/../books/media/$book_id/$lng/ $output_folder/media
     ;;
  html1 )
     output_folder=$output_folder/${book_id}_${lng}_xsl
     rm -f $output_folder/media  #this is a symbolic link
     rm -rf $output_folder
     xmlto xhtml-nochunks -v -o $output_folder $xml_file
     #xmlto html-nochunks -v -o $output_folder \
     #              -x $stylesheets/html/tldp-html.xsl \
     #              $xml_file
     ### create a link to the media files of the book
     ln -s $(pwd)/../books/media/$book_id/$lng/ $output_folder/media
     ;;
  pdf )
     xmlto pdf -v -o $output_folder $xml_file
     #xmlto pdf -v -o $output_folder \
     #              -x $stylesheets/fo/tldp-print.xsl \
     #              $xml_file
     ;;
  ps )
     xmlto ps -v -o $output_folder $xml_file
     ;;
  txt )
     xmlto txt -v -o $output_folder $xml_file
     ;;
  * )
     echo "Cannot handle format '$format'."
     usage
     ;;
esac


