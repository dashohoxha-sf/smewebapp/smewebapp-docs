#!/bin/bash
### checkout the xml sources of the books that will be used
### to generate the downloadables

### go to this dir
cd $(dirname $0)

url=$(../SVN/get_url.sh)
if [ $url = "" ]
then
  echo "SVN repository uninitialized"
elif [ -d "xml_source" ]
then
  echo "$0: 'xml_source' already exists."
else
  # checkout to directory xml_source
  svn checkout $url xml_source
  # set proper ownership
  ../../chown.sh xml_source/
fi
