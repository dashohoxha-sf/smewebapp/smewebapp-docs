#!/bin/bash
### make all the downloadable files, for all the books,
### all the languages and all the formats

### go to this dir
cd $(dirname $0)

### update the books that have changed
../SVN/commit_all.sh
svn update xml_source/

books=$(ls ../books/xml/)
for book_id in $books  #for each book
do
  # get a list of languages in which the book is available
  langs=$(ls ../books/xml/$book_id/)

  for lng in $langs  #for each language
  do
    xml_file="xml_source/${book_id}_${lng}.xml"
    xml_download="tar_gz/$book_id/$lng/${book_id}_${lng}_xml.tar.gz"
    # make the downloads only if the xml file is newer than the download file
    if [ $xml_file -nt $xml_download ]
    then 
      ./make-downloads.sh $book_id $lng
    fi
  done #langs
done #books
