#!/bin/bash
### creates the content of the folders books/, books_html/,
### workspace/books/, etc. by exploding the initial xml files

### go to this dir
cd $(dirname $0)

### create some directories
mkdir -p books/svn workspace initial_xml/uploaded

book_list="docbookwiki_guide
           smewebapp_manual
           menushpk_manual"

### import books in the system
for book_id in $book_list
do
  ./import.sh initial_xml/${book_id}_en.xml $book_id
done

./import.sh initial_xml/menushpk_manual_sq_AL.xml

### set proper ownership to some content directories
### that need to be accessed (written) by apache
content_dirs="books/
              books/xml/
              books/cache/
              books/media/
              books/svn/
              workspace/
              workspace/xml/
              workspace/cache/
              workspace/media/
              initial_xml/uploaded/"
../chown_norec.sh $content_dirs
