#!/bin/bash
### initialise the svn repository

### go to this dir
cd $(dirname $0)

if [ "$1" = "help" ]
then
  echo "Usage: $0 [svn_dir] [repository]
where 'svn_dir'     is the name of the SVN directory that will keep the docs
and   'repository'  is the SVN repository, e.g. http://server/path" 
  exit 1
fi

svn_dir=${1:-my_docs}
repository=$2

### check that it is not initialized yet
if [ -d $svn_dir ]
then
  echo "$0: '$svn_dir' already exists."
  exit
fi

### if no repository argument is given, create a local repository
if [ "$repository" = "" ]
then
  # create it if it does not exist
  if [ ! -d 'repository' ]
  then
    svnadmin create $(pwd)/repository  #create
    ../../chown.sh $(pwd)/repository   #set proper ownership
  fi
  repository=file://$(pwd)/repository 
fi

### save the svn_dir in a file for later use by commit, update and sync
echo "svn_dir='$svn_dir'" > svn_dir.txt
../../chown.sh svn_dir.txt

### implode the xml files
mkdir -p $svn_dir
. ./book_list.sh   # include the $book_list
for ((i=0; i < ${#book_list[*]}; i++))
do
  book_id_lng=${book_list[$i]}
  xml_file=$svn_dir'/'${book_id_lng/ /_}'.xml'  # svn_dir/book_id_lng.xml
  ../implode/implode.sh $book_id_lng
  book_id=${book_id_lng/ *}
  cp ../implode/tmp/$book_id.xml $xml_file
done

### import the svn_dir directory into the svn repository
echo "Importing '$svn_dir' in '$repository/$svn_dir/trunk/'"
svn import $svn_dir $repository/$svn_dir/trunk/ -m "imported by 'init.sh'"

### checkout to svn_dir
rm -rf $svn_dir
echo "Checking out '$repository/$svn_dir/trunk/' in '$svn_dir'"
svn checkout $repository/$svn_dir/trunk/ $svn_dir

### set proper ownership to $svn_dir
../../chown.sh $svn_dir

### setup the post-commit script
if [ -d 'repository' ]
then
  .  ../../books.conf  #get ADMIN_EMAIL
  cp -f post-commit.tmpl post-commit

  cat <<EOF >> post-commit

$(pwd)/commit-email.pl "\$REPOS" "\$REV" \\
    --from $ADMIN_EMAIL -s '[docbookwiki]' $ADMIN_EMAIL

EOF

  chmod +x post-commit
  ln -s ../../post-commit repository/hooks/post-commit
fi