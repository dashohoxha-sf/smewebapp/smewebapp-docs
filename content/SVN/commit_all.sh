#!/bin/bash

### go to this dir
cd $(dirname $0)

### include the book_list
. ./book_list.sh

### commit each book in the list
for ((i=0; i < ${#book_list[*]}; i++))
do
  book_id_lng=${book_list[$i]}
  ./commit.sh $book_id_lng
done
