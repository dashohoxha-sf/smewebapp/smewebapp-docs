#!/bin/bash
### finds and outputs the repository url

### go to this dir
cd $(dirname $0)

. svn_dir.txt
if [ -d $svn_dir ]
then
  url=$(svn info $svn_dir | grep 'URL:' | gawk '{print $2}')
  echo $url
else
  echo ''
fi
