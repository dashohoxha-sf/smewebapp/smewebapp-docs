#!/bin/bash
### get the old and new revions of the given xml file from cvs
### converts them to text format, and makes a diff

if [ $# -ne 2 ]
then
  echo "Usage: $0 repository revision"
  exit 1
fi

### go to this dir
cd $(dirname $0)

repos=$1
rev=$2

svnlook=/usr/bin/svnlook

### get the file that was changed
file=$($svnlook changed $repos -r $rev | sed 's/^U *//')

### get from svn the old and new versions in a temporary directory
p_rev=$(($rev - 1))  # previos revision number
mkdir -p tmp/
$svnlook cat $repos $file -r $p_rev > tmp/old_version.xml
$svnlook cat $repos $file -r $rev   > tmp/new_version.xml

### convert to text format
xmlto txt -v -o tmp/ tmp/old_version.xml 2>>tmp/output.txt
xmlto txt -v -o tmp/ tmp/new_version.xml 2>>tmp/output.txt

### find the difference of the text formats
diff -ubB tmp/old_version.txt tmp/new_version.txt | sed '1,2d'

### clean the temporary files
rm -rf tmp/
