#!/bin/bash
### commit in svn the modifications done in a book

### go to this dir
cd $(dirname $0)

if [ "$1" = "" ]
then
  echo "Usage: $0 book_id [lng]"
  echo "where lng is en, de, fr, it, al, etc. (default is en)"
  exit 1;
fi

book_id=$1
lng=${2:-en}

### read the name of the svn sandbox from svn_dir.txt
. svn_dir.txt

xml_file=$svn_dir/${book_id}_${lng}.xml

### implode
../implode/implode.sh $book_id $lng
cp ../implode/tmp/$book_id.xml $xml_file

### commit
echo "Commiting '$xml_file'"
svn commit $xml_file -m "$(hostname -i):$(pwd)"

