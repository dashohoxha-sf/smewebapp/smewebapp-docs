#!/bin/bash
### The list of the books that are maintained in the svn repository.
### It is included by init.sh, commit_all.sh, update_all.sh, sync_all.sh, etc.

### this list can be iterated with a code like this:
#for ((i=0; i < ${#book_list[*]}; i++))
#do
#  echo ${book_list[$i]}
#done

### enlist the book_id and lng of each book 
### in the array $book_list
book_list=( 
    'docbookwiki_guide en'
    'smewebapp_manual en'
    'menushpk_manual en'
    'menushpk_manual sq_AL'
)
