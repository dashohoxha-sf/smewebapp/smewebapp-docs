#!/bin/bash
### update from svn the latest version of a book

### go to this dir
cd $(dirname $0)

if [ "$1" = "" ]
then
  echo "Usage: $0 book_id [lng]"
  echo "lng is: en, de, fr, it, al, etc. (default is en)"
  exit 1;
fi

book_id=$1
lng=${2:-en}
workspace=$3

### read the name of the svn sandbox from svn_dir.txt
. svn_dir.txt

### update from svn
xml_file=$svn_dir/${book_id}_${lng}.xml
echo "Updating  '$xml_file'"
svn update $xml_file

### explode in the public space
../explode/explode.sh SVN/$xml_file

### commit in svn
xml_chunks=../books/xml/$book_id/$lng/
echo "Commiting '$xml_chunks'"
svn commit $xml_chunks -q -m 'SVN/update.sh'

### update in the workspace
ws_xml_chunks=../workspace/xml/$book_id/$lng/
echo "Updating  'ws_xml_chunks'"
svn update -q $ws_xml_chunks

### refresh cache files in books/ and workspace/
echo "Refreshing '../books/cache/$book_id/$lng/'"
../cache/cache.sh $book_id $lng 'books'
echo "Refreshing '../workspace/cache/$book_id/$lng/'"
../cache/cache.sh $book_id $lng 'workspace'


