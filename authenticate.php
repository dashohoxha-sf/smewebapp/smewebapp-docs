<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once 'global.php';

/** 
 * Checks if the given username and password are valid.
 * Returns true or false.
 */
function valid_user()
{
  $username = $_SERVER["PHP_AUTH_USER"];
  $password = $_SERVER["PHP_AUTH_PW"];

  if ($username=='superuser')
    {
      $crypted_passwd = shell('cat .su/passwd');
      $crypted_passwd = trim($crypted_passwd);
    }
  else
    {
      //get the user data
      $record = shell("templates/admin/scripts/users/get_user.sh $username");
      $fields = explode(':', $record);
      if ($fields[0]!=$username)  return false;
      $crypted_passwd = $fields[1];
      $email = $fields[3];
    }

  $valid = ($crypted_passwd == crypt($password, $crypted_passwd));
  if (!$valid) return false;

  if ($username=='superuser')
    {
      define('SU', 'true');
      define('USER', 'su');
      define('EMAIL', ADMIN_EMAIL);
    }
  else
    {
      define('SU', 'false');
      define('USER', $username);
      define('EMAIL', $email);
    }

  return true;
}

function authenticate()
{
  header("WWW-Authenticate: Basic realm=\"DocBook Editor\"");
  header("HTTP/1.0 401 Unauthorized");
  $host = $_SERVER['HTTP_HOST'];
  $file = $_SERVER['SCRIPT_NAME'];
  $url = 'http://'.$host.dirname($file).'/';
  print "
<html>
<head>
  <title>Unauthorized</title>
  <meta http-equiv='refresh' content='2;url=$url'>
</head>
<body>
<h1>Sorry, you cannot access this page.</h1>
</body>
";

  exit;
}

//authenticate if the user is unknown or not valid
if (!isset($_SERVER['PHP_AUTH_USER']))  authenticate();
else if (!valid_user())  authenticate();
?>
