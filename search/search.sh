#!/bin/bash
### the search command that is used in the application
### is similar to this, so this script can be used for 
### testing, debugging etc.

if [ $# != 1 ]
then
  echo "Usage: $0 search-expression" 
  exit 1
fi

expression=$1

swish-e -H0 -f global.index tag.index \
        -x '<book_id>::<lng>::<path>::<title>::<swishlastmodified>\n' \
        -w $expression

