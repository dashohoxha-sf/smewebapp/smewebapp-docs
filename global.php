<?php
/*
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

/**
 * This file has some general functions.
 */

/** Execute a shell command, checking it first for invalid input. */
function shell($cmd)
{
  //some basic check for invalid input
  $cmd = ereg_replace(';.*', '', $cmd);
  $cmd .= ' 2>&1';
  $output = shell_exec($cmd);
  //print "<xmp>$cmd \n----- \n$output</xmp>\n";  //debug
  return $output;
}

/** Set the owner and group of the file to WWW_DATA_OWNER and WWW_DATA_GROUP */
function set_data_owner($fname)
{
  shell("./chown.sh $fname");
}

/** Write the content to the file. */
function write_file($fname, &$fcontent)
{
  //create the directories in the path, in case they do not exist
  shell('mkdir -p '.dirname($fname));

  //write the content to the file
  $fp = fopen($fname, 'w');
  fputs($fp, $fcontent);
  fclose($fp);

  //set proper owner and permissions
  set_data_owner($fname);
}

/**
 * Converts an associative array to a string 
 * like this: key1="value1", key2="value2"
 */
function array2str($assoc_array)
{
  while ( list($key, $value) = each($assoc_array) )
    {
      $str_array[] = "$key=\"$value\"";
    }
  return implode(', ', $str_array);
}
?>