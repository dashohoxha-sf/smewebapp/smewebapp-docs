#!/bin/bash
### Set the proper ownership and group of several files and
### directories recursivly. Uses '../chown' (see ../chown.c).
###
### '../chown' changes the ownership and group of a single file
### to WWW_DATA_OWNER(MY_UID) and WWW_DATA_GROUP(APACHE_UID),
### but only if the file is owned by me (data owner) or apache.
###
### This script is equivalent to the command:
### bash# chown MY_UID:APACHE_UID file_or_dir_1 file_or_dir_2 ...
### but it does not need root permissions and it works only for
### files owned by me or apache.
###
### Different from chown.sh, this script does not work
### recursivly for directories.

chown=$(dirname $0)/chown

for arg in "$@"
do
  if [ -f $arg ]
  then
    chmod ug+rw $arg
    $chown $arg
  elif [ -d $arg ]
  then
    chmod ug+rwx $arg
    $chown $arg
  fi
done
