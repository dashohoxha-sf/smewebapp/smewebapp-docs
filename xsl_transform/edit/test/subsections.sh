#!/bin/bash

xml_file=../../../content/books/xml/linux_server_admin/en/index.xml
node_path=./basicservices/
# book_dir is relative to xsl file
book_dir=../../content/books/xml/linux_server_admin/en/

xsltproc --stringparam book_dir $book_dir \
         --stringparam path $node_path \
         ../subsections.xsl $xml_file | more
