<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!--
Transforms index.xml by adding a new subnode to the node identified by
the given path.  Is called with the parameters path, id and type (where
type is the type of the node, like chapter, section, etc.), like this:
xsltproc -stringparam path $node_path \
         -stringparam id $new_id \
         -stringparam type $new_type \
         add_subnode.xsl index.xml
-->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" version="1.0" encoding="iso-latin-1" 
            omit-xml-declaration="no" standalone="no" indent="yes" />

<xsl:template match="*">
  <xsl:choose>
    <xsl:when test="@path=$path">
      <xsl:copy>
        <xsl:copy-of select="@*" />
        <xsl:apply-templates />
        <xsl:element name="{$type}">
          <xsl:attribute name="id">
            <xsl:value-of select="$id"/>
          </xsl:attribute>
          <xsl:attribute name="path">
            <xsl:value-of select="concat(./@path, $id, '/')" />
          </xsl:attribute>
        </xsl:element>
      </xsl:copy>
    </xsl:when>

    <xsl:otherwise>
      <xsl:copy>
        <xsl:copy-of select="@*" />
        <xsl:apply-templates />
      </xsl:copy>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<!-- ignore text nodes -->
<xsl:template match="text()" />

</xsl:transform>
