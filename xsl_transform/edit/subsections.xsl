<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!--
Outputs a list of the subnodes of a certain node (their paths and 
titles), like this:
node/path1/ Subnode1 Title
node/path2/ Title of the Second Subnode

Is called with parameters book_dir and path, like this:
xsltproc -stringparam book_dir $book_dir \
         -stringparam path $path \
         subsections.xsl index.xml
-->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="text" encoding="iso-latin-1" />

<xsl:include href="../common/get-title.xsl" />

<!-- match the node with the given path -->
<xsl:template match="*">
  <xsl:if test="@path=$path">
    <xsl:for-each select="./chapter|./section|./simplesect">
      <xsl:variable name="title">
        <xsl:call-template name="get-title">
          <xsl:with-param name="book_dir" select="$book_dir" />
          <xsl:with-param name="path" select="@path" />
        </xsl:call-template>
      </xsl:variable>
      <xsl:value-of select="concat(@path, ' ', $title)" />
      <xsl:text>
</xsl:text>
    </xsl:for-each>
  </xsl:if>
  <xsl:apply-templates />
</xsl:template>

<!-- ignore text nodes -->
<xsl:template match="text()" />

</xsl:transform>
