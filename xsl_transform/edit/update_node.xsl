<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!-- 
Updates the id and path of a node in index.xml.  Is called with 
the parameters 'path' and 'new_id', like this:
xsltproc -stringparam path $path \
         -stringparam new_id $new_id \
         update_node.xsl  index.xml
-->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" version="1.0" encoding="iso-latin-1" 
            omit-xml-declaration="no" standalone="no" indent="yes" />

<xsl:template match="*">
  <xsl:choose>
    <xsl:when test="@path=$path">
      <xsl:variable name="new_path">
        <xsl:call-template name="get-new-path">
          <xsl:with-param name="old_id" select="@id" />
        </xsl:call-template>
      </xsl:variable>

      <xsl:copy>
        <xsl:attribute name="id">
          <xsl:value-of select="$new_id"/>
        </xsl:attribute>
        <xsl:attribute name="path">
          <xsl:value-of select="$new_path"/>
        </xsl:attribute>

        <xsl:apply-templates mode="update">
          <xsl:with-param name="old_id" select="@id" />
        </xsl:apply-templates>

      </xsl:copy>
    </xsl:when>

    <xsl:otherwise> <!-- copy unmodified -->
      <xsl:copy>
        <xsl:copy-of select="@*" />
        <xsl:apply-templates select="node()" />
      </xsl:copy>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<!-- substitute the old id with the new one -->
<xsl:template match="chapter|section|simplesect" mode="update">
  <xsl:param name="old_id" />

  <xsl:variable name="new_path">
    <xsl:call-template name="get-new-path">
      <xsl:with-param name="old_id" select="$old_id" />
    </xsl:call-template>
  </xsl:variable>

  <xsl:copy>
    <xsl:attribute name="id">
      <xsl:value-of select="@id"/>
    </xsl:attribute>
    <xsl:attribute name="path">
      <xsl:value-of select="$new_path"/>
    </xsl:attribute>

    <xsl:apply-templates mode="update">
      <xsl:with-param name="old_id" select="$old_id" />
    </xsl:apply-templates>

  </xsl:copy>

</xsl:template>


<!-- get the new path by replacing the old id with the new one -->
<xsl:template name="get-new-path">
  <xsl:param name="old_id" />

  <xsl:variable name="substr1">
    <xsl:value-of select="substring-before(@path, $old_id)"/>
  </xsl:variable>
  <xsl:variable name="substr2">
    <xsl:value-of select="substring-after(@path, $old_id)"/>
  </xsl:variable>

  <xsl:value-of select="concat($substr1, $new_id, $substr2)"/>

</xsl:template>


<!-- ignore text nodes -->
<xsl:template match="text()" />

</xsl:transform>
