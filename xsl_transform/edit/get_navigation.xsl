<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!--
Finds and outputs the navigation variables: next-path, next-title, 
prev-path, prev-title, this-path, this-title for the node in the given
path.  It is applied on index.xml, but uses content.xml files as
well, in order to get the title.  It is called with the parameters 
book_dir and path, like this:
xsltproc -stringparam book_dir $book_dir \
         -stringparam path $path \
         get_navigation.xsl index.xml
-->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="text" encoding="iso-latin-1" />

<xsl:include href="../common/navigation.xsl" />

<xsl:template match="*">
  <xsl:if test="@path=$path">
    <xsl:call-template name="get-navigation">
      <xsl:with-param name="book_dir" select="$book_dir" />
    </xsl:call-template>
  </xsl:if>
  <xsl:apply-templates />
</xsl:template>

<!-- ignore text nodes -->
<xsl:template match="text()" />

</xsl:transform>
