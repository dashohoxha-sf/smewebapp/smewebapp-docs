<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!--
Writes the file navigation.txt for each section (node)
in index.xml. It is called after explode.
It is called with the parameters book_dir and path, like this:
xsltproc -o $cache_path/ \
         -stringparam book_dir $book_dir \
         -stringparam path "$path" \
         navigation-txt.xsl index.xml
-->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'
            xmlns:xalanredirect="org.apache.xalan.xslt.extensions.Redirect"
            extension-element-prefixes="xalanredirect">

<xsl:output method="xml" version="1.0" encoding="iso-latin-1" 
            omit-xml-declaration="yes" standalone="yes" indent="yes" /> 

<xsl:include href="../common/navigation.xsl" />

<!-- consider only the branch with the given path -->
<xsl:template match="*">
  <xsl:if test="@path=$path">
    <xsl:apply-templates select="." mode="cache" />
  </xsl:if>
  <xsl:apply-templates />
</xsl:template>

<xsl:template match="book|article|chapter|section|simplesect" mode="cache">
  <xsl:call-template name="write-navigation-txt" />
  <xsl:apply-templates mode="cache" />
</xsl:template>

<xsl:template name="write-navigation-txt">
  <xsl:variable name="navigation-file">
    <xsl:value-of select="concat(@path, 'navigation.txt')" />
  </xsl:variable>
  <xalanredirect:write file="{$navigation-file}"
                       method="text"
                       encoding="iso-latin-1">
    <xsl:call-template name="get-navigation">
      <xsl:with-param name="book_dir" select="$book_dir" />
    </xsl:call-template>
  </xalanredirect:write>
</xsl:template>

<!-- ignore text nodes -->
<xsl:template match="text()" />

</xsl:transform>
