<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:include href="bookinfo.xsl" />
<xsl:include href="list.xsl" />
<xsl:include href="link.xsl" />
<xsl:include href="figure.xsl" />
<xsl:include href="footnotes.xsl" />
<xsl:include href="media.xsl" />
<xsl:include href="key_gui.xsl" />


<!-- suppress node title in content.xml -->
<xsl:template match="/*/title" />


<!-- paragraph -->
<xsl:template match="para">
  <p class="para"><xsl:apply-templates /></p>
</xsl:template>


<!-- preformated elements -->
<xsl:template match="screen | literallayout">
  <pre class="{name(.)}"><xsl:apply-templates /></pre>
</xsl:template>
<xsl:template match="programlisting">
  <!-- 'example' is an element that is processed by the framework -->
  <example class="{name(.)}"><xsl:apply-templates /></example>
</xsl:template>


<!-- block elements -->
<xsl:template match="title | note | tip | caution | important | warning">
  <div class="{name(.)}"><xsl:apply-templates /></div>
</xsl:template>


<!-- ignored -->
<xsl:template match="indexterm" />


<!-- ignore empty text nodes... -->
<xsl:template match="text()">
  <xsl:choose>
    <xsl:when test="normalize-space(.)=''" />
    <xsl:otherwise><xsl:value-of select="." /></xsl:otherwise>
  </xsl:choose>
</xsl:template>
<!-- ...but not in preformated contexts -->
<xsl:template match="screen//text() | literallayout//text() 
                    | programlisting//text()">
  <xsl:value-of select="." />
</xsl:template>


<!-- inline elements and the rest of elements -->
<xsl:template match="*">
  <span class="{name(.)}"><xsl:apply-templates /></span>
</xsl:template>


</xsl:transform>
