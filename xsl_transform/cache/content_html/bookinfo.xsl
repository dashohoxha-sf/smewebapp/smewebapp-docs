<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->


<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>

<xsl:template match="bookinfo | articleinfo">
  <div class="{name(.)}">
    <xsl:apply-templates select="./date" />
    <xsl:apply-templates select="./releaseinfo" />
    <xsl:apply-templates select="./author" />
    <xsl:apply-templates select="./abstract" />
    <xsl:apply-templates select="./keywordset" />
    <xsl:apply-templates select="./copyright" />
    <xsl:apply-templates select="./legalnotice" />
    <hr />
  </div>
</xsl:template>

<!-- suppress node title -->
<xsl:template match="bookinfo/title | articleinfo/title" />

<!-- date -->
<xsl:template match="date">
  <div class="date">
    <span class="header">Date:</span>
    <xsl:apply-templates />
  </div>
</xsl:template>

<!-- releaseinfo -->
<xsl:template match="releaseinfo">
  <div class="release">
    <span class="header">Release:</span>
    <xsl:apply-templates />
  </div>
</xsl:template>

<!-- author -->
<xsl:template match="author">
  <div class="author">
    <span class="header">Author:</span>
    <xsl:value-of select="./firstname" /><xsl:text> </xsl:text>
    <xsl:value-of select="./surname" /><xsl:text>, </xsl:text>

    <xsl:variable name="email">
      <xsl:value-of select="./affiliation/address/email" />
    </xsl:variable>
    <a href="mailto:{$email}"><xsl:value-of select="$email" /></a>
    <xsl:text>, </xsl:text>

    <xsl:apply-templates select="./affiliation/orgname" />
  </div>
</xsl:template>

<!-- abstract -->
<xsl:template match="abstract">
  <div class="abstract">
    <span class="header">Abstract:</span>
    <xsl:apply-templates />
  </div>
</xsl:template>

<!-- keywordset -->
<xsl:template match="keywordset">
  <div class="keywordset">
    <span class="header">Keywords:</span>
    <xsl:apply-templates />
  </div>
</xsl:template>
<xsl:template match="keyword"><xsl:apply-templates />,</xsl:template>

<!-- copyright -->
<xsl:template match="copyright">
  <div class="copyright">Copyright &#169; <xsl:apply-templates /></div>
</xsl:template>
<xsl:template match="copyright/year | copyright/holder">
  <xsl:value-of select="concat(., ', ')"/>
</xsl:template>

<!-- legalnotice -->
<xsl:template match="legalnotice">
  <div class="legalnotice"><xsl:apply-templates /></div>
</xsl:template>

</xsl:transform>
