<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->


<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>

<xsl:template match="itemizedlist">
  <ul clas="itemizedlist">
   <xsl:apply-templates />
  </ul>
</xsl:template>

<xsl:template match="listitem">
  <li class="listitem"><xsl:apply-templates /></li>
</xsl:template>

<xsl:template match="orderedlist">
  <ol class="orderedlist">
   <xsl:apply-templates select="@numeration"/>
   <xsl:apply-templates />
  </ol>
</xsl:template>

<xsl:template match="orderedlist/@numeration">
  <xsl:attribute name="type">
    <xsl:choose>
      <xsl:when test=".='loweralpha'"><xsl:value-of select="'a'"/></xsl:when>
      <xsl:when test=".='upperalpha'"><xsl:value-of select="'A'"/></xsl:when>
      <xsl:when test=".='lowerroman'"><xsl:value-of select="'i'"/></xsl:when>
      <xsl:when test=".='upperroman'"><xsl:value-of select="'I'"/></xsl:when>
      <xsl:otherwise><xsl:value-of select="'1'"/></xsl:otherwise>
    </xsl:choose>
  </xsl:attribute>
</xsl:template>

</xsl:transform>
