<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>


<!-- after processing a node, append footnotes after it -->
<xsl:template match="/">
  <xsl:apply-templates />
  <xsl:if test="//footnote">
    <div class="footnotes">
      <hr />
      <a name="footnotes"></a>
      <xsl:apply-templates select="//footnote" mode="footnote" />
    </div>
  </xsl:if>
</xsl:template>


<!-- create a footnote refrence (inside the node content) -->
<xsl:template match="footnote">
  <xsl:variable name="nr">
    <xsl:number level="any" count="footnote" from="/" format="1" />
  </xsl:variable>
  <a href="#footnotes">
    <span class="footnote">
      <xsl:value-of select="concat('[', $nr, ']')" />
    </span>
  </a>
</xsl:template>


<!-- create a footnote body (at the end of the node) -->
<xsl:template match="footnote" mode="footnote">
  <xsl:variable name="nr">
    <xsl:number level="any" count="footnote" from="/" format="1" />
  </xsl:variable>
  <div class="footnote-body">
    <a href="javascript:back()">
      <xsl:value-of select="concat($nr, ') ')" />
    </a>
    <xsl:apply-templates select="./*" />
  </div>
</xsl:template>


<!-- skip para tags inside footnotes -->
<xsl:template match="footnote/para"><xsl:apply-templates /></xsl:template>


</xsl:transform>
