<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!--
This transformer creates the cache files 'subnodes.html' for each 
node in the index, taking the title from the files content.xml.
It takes the parameters book_dir and path like this:
xsltproc -o cache_path/ \
         -stringparam book_dir $book_dir \
         -stringparam path "$path" \
         subnodes-html.xsl index.xml
-->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'
            xmlns:xalanredirect="org.apache.xalan.xslt.extensions.Redirect"
            extension-element-prefixes="xalanredirect">

<xsl:output method="xml" version="1.0" encoding="iso-latin-1" 
            omit-xml-declaration="yes" standalone="yes" indent="yes" /> 

<xsl:include href="../common/get-subnodes.xsl" />

<!-- consider only the branch with the given path -->
<xsl:template match="*">
  <xsl:if test="@path=$path">
    <xsl:apply-templates select="." mode="cache" />
  </xsl:if>
  <xsl:apply-templates />
</xsl:template>

<!-- write the file subnodes.html for each section -->
<xsl:template match="book|article|chapter|section|simplesect" mode="cache">
  <xsl:call-template name="write-subnodes-html" />
  <xsl:apply-templates mode="cache" />
</xsl:template>

<xsl:template name="write-subnodes-html">
  <xsl:variable name="subnodes-html"
                select="concat(@path, 'subnodes.html')" />

  <xalanredirect:write file="{$subnodes-html}"
                       method="xml"
                       encoding="iso-latin-1" 
                       omit-xml-declaration="yes" 
                       standalone="yes"
                       indent="yes">

    <xsl:call-template name="get-subnodes">
      <xsl:with-param name="book_dir" select="$book_dir" />
    </xsl:call-template>
  </xalanredirect:write>
</xsl:template>

<!-- ignore text nodes -->
<xsl:template match="text()" />

</xsl:transform>
