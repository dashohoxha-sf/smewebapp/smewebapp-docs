<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!--
This is used by implode.sh to create the file book_id.xml
by imploding all the chunks content.xml.  It is called with
the parameter 'book_dir', like this:
xsltproc -o book_id.xml \
         -stringparam book_dir $book_dir/ \
	 implode.xsl index.xml
-->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="xml" version="1.0" encoding="iso-latin-1" 
            omit-xml-declaration="no" standalone="no" indent="yes" />


<!-- output the doctype declaration -->
<xsl:template match="/">
<xsl:text disable-output-escaping="yes"><![CDATA[
<!DOCTYPE ]]></xsl:text> <xsl:value-of select="name(/*)"/>
<xsl:text disable-output-escaping="yes"><![CDATA[ PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN"
                      "http://docbook.org/xml/4.2/docbookx.dtd">
]]>
</xsl:text>
<xsl:apply-templates />
</xsl:template>


<!-- copy each section and then include its content and subsections -->
<xsl:template match="book | article | chapter | section | simplesect">
  <!-- copy the section and its attribute id -->
  <xsl:copy>
    <xsl:copy-of select="@id" />

    <!-- include the section content -->
    <xsl:variable name="section-content"
         select="concat($book_dir, @path, 'content.xml')" />
    <xsl:apply-templates select="document($section-content)" mode="include">
      <xsl:with-param name="path" select="@path" />
    </xsl:apply-templates>

    <!-- process subsections -->
    <xsl:apply-templates />
  </xsl:copy>
</xsl:template>


<!-- ignore section tags in included files because -->
<!-- they are included from the index file         -->
<xsl:template match="book | article | chapter | section | simplesect"
              mode="include">
  <xsl:param name="path" />

  <xsl:copy-of select="@lang" />
  <xsl:apply-templates mode="include">
    <xsl:with-param name="path" select="$path" />
  </xsl:apply-templates>
</xsl:template>


<!-- ignore empty text nodes (in included files) -->
<xsl:template match="text()" mode="include">
  <xsl:choose>
    <xsl:when test="normalize-space(.)=''" />
    <xsl:otherwise><xsl:value-of select="." /></xsl:otherwise>
  </xsl:choose>
</xsl:template>
<!-- ...but not for screen or programlisting -->
<xsl:template match="screen/text()|programlisting/text()" mode="include">
  <xsl:value-of select="." />
</xsl:template>


<!-- modify attribute fileref of imagedata -->
<xsl:template match="imagedata/@fileref" mode="include">
  <xsl:param name="path" />

  <xsl:attribute name="fileref">
    <xsl:value-of select="'media/'" />
    <xsl:value-of select="substring($path, 3)" />
    <xsl:value-of select="." />
  </xsl:attribute>
</xsl:template>


<!-- copy everything else (from included files) -->
<xsl:template match="* | @*" mode="include">
  <xsl:param name="path" />

  <xsl:copy>
    <xsl:apply-templates select="@*" mode="include">
      <xsl:with-param name="path" select="$path" />
    </xsl:apply-templates>
    <xsl:apply-templates select="node()" mode="include">
      <xsl:with-param name="path" select="$path" />
    </xsl:apply-templates>
  </xsl:copy>
</xsl:template>


<!-- ignore the content of index.xml -->
<xsl:template match="text()" />


</xsl:transform>
