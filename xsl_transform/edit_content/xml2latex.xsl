<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!-- Used to convert content.xml to latex, in latex editing mode. -->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>

<xsl:output method="xml" version="1.0" encoding="iso-latin-1" 
            omit-xml-declaration="yes" standalone="yes" indent="yes" />

<!-- skip the section elements and the title -->
<xsl:template match="book|article|chapter|section|simplesect">
  <xsl:apply-templates />
</xsl:template>

<xsl:template match="title" />

<xsl:template match="para" xml:space="preserve">
<xsl:apply-templates />
</xsl:template>


<!-- inline elements -->

<xsl:template match="filename">
  <xsl:text> \file{</xsl:text>
  <xsl:apply-templates />
  <xsl:text>} </xsl:text>
</xsl:template>

<xsl:template match="prompt">
  <xsl:copy>
    <xsl:apply-templates />
  </xsl:copy>
</xsl:template>

<xsl:template match="command">
  <xsl:text> \command{</xsl:text>
  <xsl:apply-templates />
  <xsl:text>} </xsl:text>
</xsl:template>

<xsl:template match="emphasis">
  <xsl:text> \em{</xsl:text>
  <xsl:apply-templates />
  <xsl:text>} </xsl:text>
</xsl:template>

<!-- ordered and unordered lists -->

<xsl:template match="orderedlist" xml:space="preserve">
\begin{orderedlist}<xsl:apply-templates />\end{orderedlist}
</xsl:template>

<xsl:template match="itemizedlist" xml:space="preserve">
\begin{itemizedlist}<xsl:apply-templates />\end{itemizedlist}
</xsl:template>

<xsl:template match="listitem//orderedlist" xml:space="preserve">
    \begin{orderedlist}<xsl:apply-templates />    \end{orderedlist}
</xsl:template>

<xsl:template match="listitem//itemizedlist" xml:space="preserve">
    \begin{itemizedlist}<xsl:apply-templates />    \end{itemizedlist}
</xsl:template>

<xsl:template match="listitem" xml:space="preserve">
  \item <xsl:apply-templates />
</xsl:template>

<xsl:template match="listitem//listitem" xml:space="preserve">
      \item <xsl:apply-templates />
</xsl:template>

<xsl:template match="listitem/para">
  <xsl:apply-templates />
</xsl:template>

<!-- block elements (figure, screen, programlisting, etc. -->

<xsl:template match="figure" xml:space="preserve">
\figure{<xsl:value-of select="normalize-space(./title)" />}<xsl:apply-templates />
</xsl:template>
<xsl:template match="figure/title" />

<xsl:template match="screen" xml:space="preserve">
\begin{verbatim}<xsl:apply-templates />\end{verbatim}
</xsl:template>

<xsl:template match="programlisting" xml:space="preserve">
\begin{verbatim}<xsl:apply-templates />\end{verbatim}
</xsl:template>

<!-- normalize text, but not for screen or programlisting -->
<xsl:template match="text()">
  <xsl:value-of select="normalize-space(.)" />
</xsl:template>
<xsl:template match="screen/text()|programlisting/text()">
  <xsl:value-of select="." />
</xsl:template>

<!-- copy the rest of elements (and their attributes) unchanged -->
<xsl:template match="*|@*">
  <xsl:copy>
    <xsl:apply-templates select="@*"/>
    <xsl:apply-templates select="node()"/>
  </xsl:copy>
</xsl:template>

</xsl:transform>
