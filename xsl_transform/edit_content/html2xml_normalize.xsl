<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!--
Used to convert html to xml, before saving it to content.xml,
in html editing mode.
-->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>

<xsl:output method="xml" version="1.0" encoding="iso-latin-1" 
            omit-xml-declaration="yes" standalone="yes" indent="yes" />


<!-- Converts html tag names to xml tag names -->
<xsl:include href="html2xml_names.xsl" />


<!-- book, article, chapter, section, simplesect, etc, -->
<xsl:template match="/*">
  <xsl:element name="{name(.)}">
    <xsl:apply-templates select="@*"/>
<xsl:text>
</xsl:text>
    <xsl:apply-templates select="node()"/>
  </xsl:element>
</xsl:template>

<xsl:template match="title">
  <xsl:element name="{name(.)}">
    <xsl:apply-templates select="@*"/>
    <xsl:apply-templates select="node()"/>
  </xsl:element>
<xsl:text>
</xsl:text>
</xsl:template>


<!-- normalize text ... -->
<xsl:template match="text()">
  <xsl:value-of select="normalize-space(.)" />
</xsl:template>
<!-- ... but not in verbatim contexts -->
<xsl:template match="scr//text() | ll//text() | code//text()">
  <xsl:value-of select="." />
</xsl:template>


<!-- add a new line before and after block elements -->
<!-- this is necessary because of the normalization of the text -->
<xsl:template match="p | scr | ll | code | ol | ul | comment">
  <xsl:variable name="new-tag">
    <xsl:call-template name="get-xml-name">
      <xsl:with-param name="name" select="name(.)"/>
    </xsl:call-template>
  </xsl:variable>

<xsl:text>
</xsl:text>

  <xsl:element name="{$new-tag}">
    <xsl:apply-templates select="@*"/>
    <xsl:apply-templates select="node()"/>
  </xsl:element>

<xsl:text>
</xsl:text>

</xsl:template>


<!-- add a space before and after inline elements -->
<!-- this is necessary because of the normalization of the text -->
<xsl:template match="*">
  <xsl:variable name="new-tag">
    <xsl:call-template name="get-xml-name">
      <xsl:with-param name="name" select="name(.)"/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:text> </xsl:text>

  <xsl:element name="{$new-tag}">
    <xsl:apply-templates select="@*"/>
    <xsl:apply-templates select="node()"/>
  </xsl:element>

  <xsl:text> </xsl:text>

</xsl:template>


<!-- elements in preformated contexts are not padded with any space -->
<xsl:template match="scr//* | ll//* | code//*">
  <xsl:variable name="new-tag">
    <xsl:call-template name="get-xml-name">
      <xsl:with-param name="name" select="name(.)"/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:element name="{$new-tag}">
    <xsl:apply-templates select="@*"/>
    <xsl:apply-templates select="node()"/>
  </xsl:element>

</xsl:template>


<!-- copy attributes, translating their names if needed -->
<xsl:template match="@*">
  <xsl:variable name="new-attr">
    <xsl:call-template name="get-xml-name">
      <xsl:with-param name="name" select="name(.)"/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:attribute name="{$new-attr}">
    <xsl:value-of select="."/>
  </xsl:attribute>

</xsl:template>


<!-- example, figure -->
<xsl:template match="fig/t | xmp/t" />

<xsl:template match="fig | xmp">
  <xsl:variable name="new-tag">
    <xsl:call-template name="get-xml-name">
      <xsl:with-param name="name" select="name(.)"/>
    </xsl:call-template>
  </xsl:variable>

<xsl:text>
</xsl:text>

  <xsl:element name="{$new-tag}">
    <xsl:apply-templates select="@*"/>
    <xsl:if test="./t"><title><xsl:value-of select="./t"/></title></xsl:if>
    <xsl:apply-templates />
  </xsl:element>

<xsl:text>
</xsl:text>

</xsl:template>


<!-- admonitions -->
<xsl:template match="n/t | tip/t | caution/t | imp/t | w/t" />

<xsl:template match="n | tip | caution | imp | w">
  <xsl:variable name="new-tag">
    <xsl:call-template name="get-xml-name">
      <xsl:with-param name="name" select="name(.)"/>
    </xsl:call-template>
  </xsl:variable>

<xsl:text>
</xsl:text>

  <xsl:element name="{$new-tag}">
    <xsl:apply-templates select="@*"/>
    <xsl:if test="./t"><title><xsl:value-of select="./t"/></title></xsl:if>
    <para><xsl:apply-templates /></para>
  </xsl:element>

<xsl:text>
</xsl:text>

</xsl:template>


<!-- media objects (images) -->
<xsl:template match="img" xml:space="preserve">
<mediaobject>
  <imageobject>
    <imagedata fileref="{@src}"><xsl:copy-of
               select="@width"/><xsl:copy-of
               select="@height"/><xsl:copy-of
               select="@align"/><xsl:copy-of
               select="@valign"/></imagedata>
  </imageobject>
  <xsl:if test="@alt"><textobject><phrase><xsl:value-of
    select="@alt"/></phrase></textobject></xsl:if>
  <xsl:if test="@title"><caption><para><xsl:value-of
    select="@title"/></para></caption></xsl:if>
</mediaobject>
</xsl:template>


<!-- nested ordered and unordered lists -->

<xsl:template match="li">
  <xsl:variable name="indent">
    <xsl:apply-templates select="." mode="get-indent" />
  </xsl:variable>

  <xsl:text>
</xsl:text>
  <xsl:value-of select="$indent" />
  <listitem><para>
  <xsl:apply-templates />
  <xsl:if test="descendant::li or descendant::code
                or descendant::scr or descendant::ll">
    <xsl:value-of select="$indent" />
  </xsl:if>
  </para>
  <xsl:if test="descendant::li or descendant::code
                or descendant::scr or descendant::ll">
    <xsl:text>
</xsl:text>
    <xsl:value-of select="$indent" />
  </xsl:if>
  </listitem>
  <xsl:text>
</xsl:text>
</xsl:template>


<xsl:template match="li/ul">
  <xsl:variable name="indent">
    <xsl:apply-templates select="." mode="get-indent" />
  </xsl:variable>

  <xsl:text>
</xsl:text>
  <xsl:value-of select="$indent" />
  <itemizedlist>
  <xsl:apply-templates />
  <xsl:value-of select="$indent" />
  </itemizedlist>
  <xsl:text>
</xsl:text>
</xsl:template>


<xsl:template match="li/ol">
  <xsl:variable name="indent">
    <xsl:apply-templates select="." mode="get-indent" />
  </xsl:variable>

  <xsl:text>
</xsl:text>
  <xsl:value-of select="$indent" />
  <orderedlist>
  <xsl:apply-templates select="@type"/>
  <xsl:apply-templates />
  <xsl:value-of select="$indent" />
  </orderedlist>
  <xsl:text>
</xsl:text>
</xsl:template>


<xsl:template match="ol/@type">
  <xsl:attribute name="numeration">
    <xsl:choose>
      <xsl:when test=".='a'"><xsl:value-of select="'loweralpha'"/></xsl:when>
      <xsl:when test=".='A'"><xsl:value-of select="'upperalpha'"/></xsl:when>
      <xsl:when test=".='i'"><xsl:value-of select="'lowerroman'"/></xsl:when>
      <xsl:when test=".='I'"><xsl:value-of select="'upperroman'"/></xsl:when>
      <xsl:otherwise><xsl:value-of select="'arabic'"/></xsl:otherwise>
    </xsl:choose>
  </xsl:attribute>
</xsl:template>


<xsl:template match="ul|ol" mode="get-indent">
  <xsl:choose>
    <xsl:when test="ancestor::ul or ancestor::ol">
      <xsl:apply-templates select=".." mode="get-indent" />
      <xsl:value-of select="'  '" />
    </xsl:when>
    <xsl:otherwise><xsl:value-of select="''" /></xsl:otherwise>
  </xsl:choose>
</xsl:template>
<xsl:template match="li" mode="get-indent">
  <xsl:apply-templates select=".." mode="get-indent" />
  <xsl:value-of select="'  '" />
</xsl:template>


</xsl:transform>
