<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!-- Converts html tag names to xml tag names -->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>

<!-- for an xml tag or attribute, get the corresponding short name -->
<xsl:template name="get-xml-name">
  <xsl:param name="name" />

  <xsl:choose>

    <!-- preformated elements -->

    <xsl:when test="$name='scr'">
      <xsl:value-of select="'screen'"/>
    </xsl:when>

    <xsl:when test="$name='ll'">
      <xsl:value-of select="'literallayout'"/>
    </xsl:when>

    <xsl:when test="$name='code'">
      <xsl:value-of select="'programlisting'"/>
    </xsl:when>

    <!-- list elements -->

    <xsl:when test="$name='ul'">
      <xsl:value-of select="'itemizedlist'"/>
    </xsl:when>

    <xsl:when test="$name='ol'">
      <xsl:value-of select="'orderedlist'"/>
    </xsl:when>

    <xsl:when test="$name='li'">
      <xsl:value-of select="'listitem'"/>
    </xsl:when>

    <!-- admonitions -->

    <xsl:when test="$name='n'">
      <xsl:value-of select="'note'"/>
    </xsl:when>

    <xsl:when test="$name='tip'">
      <xsl:value-of select="'tip'"/>
    </xsl:when>

    <xsl:when test="$name='caution'">
      <xsl:value-of select="'caution'"/>
    </xsl:when>

    <xsl:when test="$name='imp'">
      <xsl:value-of select="'important'"/>
    </xsl:when>

    <xsl:when test="$name='w'">
      <xsl:value-of select="'warning'"/>
    </xsl:when>

    <!-- block elements -->

    <xsl:when test="$name='p'">
      <xsl:value-of select="'para'"/>
    </xsl:when>

    <xsl:when test="$name='t'">
      <xsl:value-of select="'title'"/>
    </xsl:when>

    <xsl:when test="$name='fig'">
      <xsl:value-of select="'figure'"/>
    </xsl:when>

    <xsl:when test="$name='xmp'">
      <xsl:value-of select="'example'"/>
    </xsl:when>

    <!-- inline elements -->

    <xsl:when test="$name='fn'">
      <xsl:value-of select="'footnote'"/>
    </xsl:when>

    <xsl:when test="$name='a'">
      <xsl:value-of select="'ulink'"/>
    </xsl:when>

    <xsl:when test="$name='pr'">
      <xsl:value-of select="'prompt'"/>
    </xsl:when>

    <xsl:when test="$name='c'">
      <xsl:value-of select="'command'"/>
    </xsl:when>

    <xsl:when test="$name='o'">
      <xsl:value-of select="'option'"/>
    </xsl:when>

    <xsl:when test="$name='app'">
      <xsl:value-of select="'application'"/>
    </xsl:when>

    <xsl:when test="$name='f'">
      <xsl:value-of select="'filename'"/>
    </xsl:when>

    <xsl:when test="$name='co'">
      <xsl:value-of select="'computeroutput'"/>
    </xsl:when>

    <xsl:when test="$name='ui'">
      <xsl:value-of select="'userinput'"/>
    </xsl:when>

    <xsl:when test="$name='r'">
      <xsl:value-of select="'replaceable'"/>
    </xsl:when>

    <xsl:when test="$name='tm'">
      <xsl:value-of select="'trademark'"/>
    </xsl:when>

    <xsl:when test="$name='ft'">
      <xsl:value-of select="'firstterm'"/>
    </xsl:when>

    <xsl:when test="$name='acr'">
      <xsl:value-of select="'acronym'"/>
    </xsl:when>

    <xsl:when test="$name='waw'">
      <xsl:value-of select="'wordasword'"/>
    </xsl:when>

    <xsl:when test="$name='em'">
      <xsl:value-of select="'emphasis'"/>
    </xsl:when>

    <!-- gui... -->

    <xsl:when test="$name='gl'">
      <xsl:value-of select="'guilabel'"/>
    </xsl:when>

    <xsl:when test="$name='gb'">
      <xsl:value-of select="'guibutton'"/>
    </xsl:when>

    <xsl:when test="$name='gi'">
      <xsl:value-of select="'guiicon'"/>
    </xsl:when>

    <xsl:when test="$name='gm'">
      <xsl:value-of select="'guimenu'"/>
    </xsl:when>

    <xsl:when test="$name='gmi'">
      <xsl:value-of select="'guimenuitem'"/>
    </xsl:when>

    <xsl:when test="$name='k'">
      <xsl:value-of select="'keycap'"/>
    </xsl:when>

    <xsl:when test="$name='kc'">
      <xsl:value-of select="'keycombo'"/>
    </xsl:when>

    <xsl:when test="$name='mch'">
      <xsl:value-of select="'menuchoice'"/>
    </xsl:when>

    <xsl:when test="$name='shc'">
      <xsl:value-of select="'shortcut'"/>
    </xsl:when>

    <!-- attributes -->

    <xsl:when test="$name='href'"> <!-- attr of a -->
      <xsl:value-of select="'url'"/>
    </xsl:when>

    <xsl:when test="$name='type'"> <!-- attr of ol -->
      <xsl:value-of select="'numeration'"/>
    </xsl:when>

    <!-- if nothing matches, return the name itself -->

    <xsl:otherwise>
      <xsl:value-of select="$name"/>
    </xsl:otherwise>

  </xsl:choose>

</xsl:template>


</xsl:transform>
