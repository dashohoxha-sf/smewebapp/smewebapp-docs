<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!-- Used to convert content.xml to html, in html editing mode. -->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>

<xsl:output method="xml" version="1.0" encoding="iso-latin-1" 
            omit-xml-declaration="yes" standalone="yes" indent="no" />


<!-- Converts xml tag names to html tag names -->
<xsl:include href="xml2html_names.xsl" />


<!-- skip these tags -->
<xsl:template match="/*">
  <xsl:apply-templates />
</xsl:template>

<xsl:template match="listitem/para | note/para | tip/para 
                     | caution/para | important/para | warning/para">
  <xsl:apply-templates />
</xsl:template>


<!-- ignore these elements -->
<xsl:template match="/*/title | bookinfo/title | articleinfo/title
                     | orderedlist/@continuation | orderedlist/@inheritnum" />


<!-- normalize text ... -->
<xsl:template match="text()">
  <xsl:value-of select="normalize-space(.)" />
</xsl:template>
<!-- ... but not in verbatim contexts -->
<xsl:template match="screen//text()
                    | literallayout//text()
                    | programlisting//text()">
  <xsl:value-of select="." />
</xsl:template>


<!-- add a new line before and after block elements -->
<!-- this is necessary because of the normalization of the text -->
<xsl:template match="para | figure | example 
                    | screen | literallayout | programlisting 
                    | itemizedlist | orderedlist
                    | note | tip | caution | important | warning | comment">
  <xsl:variable name="new-tag">
    <xsl:call-template name="get-short-name">
      <xsl:with-param name="name" select="name(.)"/>
    </xsl:call-template>
  </xsl:variable>

<xsl:text>
</xsl:text>

  <xsl:element name="{$new-tag}">
    <xsl:apply-templates select="@*"/>
    <xsl:apply-templates select="node()"/>
  </xsl:element>

<xsl:text>
</xsl:text>

</xsl:template>


<!-- add a space before and after inline elements -->
<!-- this is necessary because of the normalization of the text -->
<xsl:template match="*">
  <xsl:variable name="new-tag">
    <xsl:call-template name="get-short-name">
      <xsl:with-param name="name" select="name(.)"/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:text> </xsl:text>

  <xsl:element name="{$new-tag}">
    <xsl:apply-templates select="@*"/>
    <xsl:apply-templates select="node()"/>
  </xsl:element>

  <xsl:text> </xsl:text>

</xsl:template>


<!-- elements in preformated contexts are not padded with any space -->
<xsl:template match="screen//* | literallayout//* | programlisting//*">
  <xsl:variable name="new-tag">
    <xsl:call-template name="get-short-name">
      <xsl:with-param name="name" select="name(.)"/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:element name="{$new-tag}">
    <xsl:apply-templates select="@*"/>
    <xsl:apply-templates select="node()"/>
  </xsl:element>

</xsl:template>


<!-- copy attributes, translating their names if needed -->
<xsl:template match="@*">
  <xsl:variable name="new-attr">
    <xsl:call-template name="get-short-name">
      <xsl:with-param name="name" select="name(.)"/>
    </xsl:call-template>
  </xsl:variable>

  <xsl:attribute name="{$new-attr}">
    <xsl:value-of select="."/>
  </xsl:attribute>

</xsl:template>


<!-- media object -->
<xsl:template match="mediaobject">
<xsl:text>
</xsl:text>
<img src="{./imageobject/imagedata/@fileref}">
  <xsl:copy-of select="./imageobject/imagedata/@width"/>
  <xsl:copy-of select="./imageobject/imagedata/@height"/>
  <xsl:copy-of select="./imageobject/imagedata/@align"/>
  <xsl:copy-of select="./imageobject/imagedata/@valign"/>
  <xsl:if test="./caption">
    <xsl:attribute name="title">
      <xsl:value-of select="./caption/para" />
    </xsl:attribute>
  </xsl:if>
  <xsl:if test="./textobject/phrase">
    <xsl:attribute name="alt">
      <xsl:value-of select="./textobject/phrase" />    
    </xsl:attribute>
  </xsl:if>
</img>
<xsl:text>
</xsl:text>
</xsl:template>


<!-- nested ordered and unordered lists -->

<xsl:template match="listitem">
  <xsl:variable name="indent">
    <xsl:apply-templates select="." mode="get-indent" />
  </xsl:variable>

  <xsl:text>
</xsl:text>
  <xsl:value-of select="$indent" />
  <li>
  <xsl:apply-templates />
  <xsl:if test="descendant::listitem or descendant::programlisting
                or descendant::screen or descendant::literallayout">
    <xsl:value-of select="$indent" />
  </xsl:if>
  </li>
  <xsl:text>
</xsl:text>
</xsl:template>


<xsl:template match="listitem//itemizedlist">
  <xsl:variable name="indent">
    <xsl:apply-templates select="." mode="get-indent" />
  </xsl:variable>

  <xsl:text>
</xsl:text>
  <xsl:value-of select="$indent" />
  <ul>
  <xsl:apply-templates />
  <xsl:value-of select="$indent" />
  </ul>
  <xsl:text>
</xsl:text>
</xsl:template>


<xsl:template match="listitem//orderedlist">
  <xsl:variable name="indent">
    <xsl:apply-templates select="." mode="get-indent" />
  </xsl:variable>

  <xsl:text>
</xsl:text>
  <xsl:value-of select="$indent" />
  <ol>
  <xsl:apply-templates select="@numeration"/>
  <xsl:apply-templates />
  <xsl:value-of select="$indent" />
  </ol>
  <xsl:text>
</xsl:text>
</xsl:template>


<xsl:template match="orderedlist/@numeration">
  <xsl:attribute name="type">
    <xsl:choose>
      <xsl:when test=".='loweralpha'"><xsl:value-of select="'a'"/></xsl:when>
      <xsl:when test=".='upperalpha'"><xsl:value-of select="'A'"/></xsl:when>
      <xsl:when test=".='lowerroman'"><xsl:value-of select="'i'"/></xsl:when>
      <xsl:when test=".='upperroman'"><xsl:value-of select="'I'"/></xsl:when>
      <xsl:otherwise><xsl:value-of select="'1'"/></xsl:otherwise>
    </xsl:choose>
  </xsl:attribute>
</xsl:template>


<xsl:template match="itemizedlist|orderedlist" mode="get-indent">
  <xsl:choose>
    <xsl:when test="ancestor::itemizedlist or ancestor::orderedlist">
      <xsl:apply-templates select="../.." mode="get-indent" />
      <xsl:value-of select="'  '" />
    </xsl:when>
    <xsl:otherwise><xsl:value-of select="''" /></xsl:otherwise>
  </xsl:choose>
</xsl:template>
<xsl:template match="listitem" mode="get-indent">
  <xsl:apply-templates select=".." mode="get-indent" />
  <xsl:value-of select="'  '" />
</xsl:template>

</xsl:transform>
