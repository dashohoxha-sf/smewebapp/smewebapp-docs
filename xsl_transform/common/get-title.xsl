<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!--
Gets and returns the title from the file content.xml
in the given path. It is called with the parameters book_dir
and path, like this:
    <xsl:call-template name="get-title">
      <xsl:with-param name="book_dir" select="$book_dir" />
      <xsl:with-param name="path" select="$path" />
    </xsl:call-template>
-->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:template name="get-title">
  <xsl:param name="book_dir" />
  <xsl:param name="path" />

  <xsl:variable name="xml-file">
    <xsl:value-of select="concat($book_dir, $path)" />
    <xsl:value-of select="'content.xml'" />
  </xsl:variable>
  <xsl:apply-templates mode="get-title" select="document($xml-file)" />
</xsl:template>

<xsl:template mode="get-title" match="/*/title
                                     | /book/bookinfo/title
                                     | /article/articleinfo/title">
  <xsl:value-of select="normalize-space(.)" />
</xsl:template>

<xsl:template mode="get-title" match="text()" />

</xsl:transform>
