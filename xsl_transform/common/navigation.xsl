<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!--
Contains the template 'get-navigation', which finds and returns the 
navigation variables.  It is called with the parameter book_dir, like this:
  <xsl:call-template name="get-navigation">
    <xsl:with-param name="book_dir" select="$book_dir" />
  </xsl:call-template>
-->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:include href="get-title.xsl" />

<!-- get prev-path -->
<xsl:template match="book|article|chapter|section|simplesect" mode="prev-path">
  <xsl:value-of select="(/* | parent::* | preceding::chapter
                            | preceding::section | preceding::simplesect)
                            [position()=last()]/@path" />
</xsl:template>

<!-- get next-path -->
<xsl:template match="book|article|chapter|section|simplesect" mode="next-path">
  <xsl:variable name="next-path">
    <xsl:value-of select="(child::chapter|child::section|child::simplesect|following::chapter|following::section|following::simplesect)[1]/@path" />
  </xsl:variable>
  <xsl:choose>
    <xsl:when test="$next-path=''"><xsl:value-of select="/*/@path"/></xsl:when>
    <xsl:otherwise><xsl:value-of select="$next-path"/></xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!-- find and output the navigation variables -->
<xsl:template name="get-navigation">
  <xsl:param name="book_dir" />

  <!-- get navigation variables -->
  <xsl:variable name="prev-path">
    <xsl:apply-templates mode="prev-path" select="." />
  </xsl:variable> 

  <xsl:variable name="next-path">
    <xsl:apply-templates mode="next-path" select="." />
  </xsl:variable> 
 
  <xsl:variable name="prev-title">
    <xsl:call-template name="get-title">
      <xsl:with-param name="book_dir" select="$book_dir" />
      <xsl:with-param name="path" select="$prev-path" />
    </xsl:call-template>
  </xsl:variable>

  <xsl:variable name="next-title">
    <xsl:call-template name="get-title">
      <xsl:with-param name="book_dir" select="$book_dir" />
      <xsl:with-param name="path" select="$next-path" />
    </xsl:call-template>
  </xsl:variable> 

  <xsl:variable name="up-title">
    <xsl:call-template name="get-title">
      <xsl:with-param name="book_dir" select="$book_dir" />
      <xsl:with-param name="path" select="(..|.)[1]/@path" />
    </xsl:call-template>
  </xsl:variable> 

  <xsl:variable name="this-title">
    <xsl:call-template name="get-title">
      <xsl:with-param name="book_dir" select="$book_dir" />
      <xsl:with-param name="path" select="@path" />
    </xsl:call-template>
  </xsl:variable> 

  <xsl:variable name="this-full-title">
    <xsl:apply-templates select="." mode="get-full-title">
      <xsl:with-param name="book_dir" select="$book_dir" />
    </xsl:apply-templates>
  </xsl:variable> 

  <!-- output navigation variables -->
  <xsl:value-of select="concat('this_path=', @path)"/><xsl:text>
</xsl:text>
  <xsl:value-of select="concat('this_title=', $this-title)"/><xsl:text>
</xsl:text>
  <xsl:value-of select="concat('this_full_title=', $this-full-title)"/><xsl:text>
</xsl:text>
  <xsl:value-of select="concat('up_path=', (..|.)[1]/@path)"/><xsl:text>
</xsl:text>
  <xsl:value-of select="concat('up_title=', $up-title)"/><xsl:text>
</xsl:text>
  <xsl:value-of select="concat('prev_path=', $prev-path)"/><xsl:text>
</xsl:text>
  <xsl:value-of select="concat('prev_title=', $prev-title)"/><xsl:text>
</xsl:text>
  <xsl:value-of select="concat('next_path=', $next-path)"/><xsl:text>
</xsl:text>
  <xsl:value-of select="concat('next_title=', $next-title)"/><xsl:text>
</xsl:text>
</xsl:template>


<!-- get the full title of current node (chap_t/sect_t/subsect_t) -->
<xsl:template match="book | article" mode="get-full-title">
  <xsl:param name="book_dir" />
</xsl:template>

<xsl:template match="chapter | section | simplesect" mode="get-full-title">
  <xsl:param name="book_dir" />

  <xsl:apply-templates select=".." mode="get-full-title">
    <xsl:with-param name="book_dir" select="$book_dir" />
  </xsl:apply-templates>

  <xsl:call-template name="get-title">
    <xsl:with-param name="book_dir" select="$book_dir" />
    <xsl:with-param name="path" select="@path" />
  </xsl:call-template>

  <xsl:text> / </xsl:text>
</xsl:template>


</xsl:transform>
