<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!--
Returns the node path of a section, which is the folder
where the content, index, etc. for this section will be stored.
-->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">


<xsl:include href="count.xsl" />


<!-- returns the name of the folder for the current section -->
<xsl:template name="get-folder">
  <xsl:choose>
    <xsl:when test="@id">
      <xsl:value-of select="concat(@id, '/')" />
    </xsl:when>
    <xsl:otherwise>
      <xsl:variable name="count">
        <xsl:apply-templates select="." mode="count" />
      </xsl:variable> 
      <xsl:value-of select="concat(name(.), '-', $count, '/')" />
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<!-- stops the recursion, the path of a  -->
<!-- book or article is the empty string -->
<xsl:template match="book | article" mode="path">
  <xsl:value-of select="'./'" />
</xsl:template>


<!-- recursivly construct the path by appending  -->
<!-- the folder name to the path of the parent   -->
<xsl:template match="chapter | section | simplesect" mode="path">
  <xsl:apply-templates select=".." mode="path" />
  <xsl:call-template name="get-folder" />
</xsl:template>


<!-- for any other element or attribute, its path     -->
<!-- is the path of the containing section or chapter -->
<xsl:template match="* | @*" mode="path">
  <xsl:apply-templates select=".." mode="path" />
</xsl:template>


</xsl:transform>
