<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!--
Contains the template 'get-subnodes' which creates an html list of
the subnodes of the current node.  It is called with the parameter
book_dir, like this:
    <xsl:call-template name="get-subnodes">
      <xsl:with-param name="book_dir" select="$book_dir" />
    </xsl:call-template>
-->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:include href="section-level.xsl" />
<xsl:include href="get-title.xsl" />

<!-- creates an html list of subnodes of the current node -->
<xsl:template name="get-subnodes">
  <xsl:param name="book_dir" />
  <dl>
    <xsl:apply-templates mode="get-subnodes" />
  </dl>
</xsl:template>

<!-- subnodes for a section -->
<xsl:template match="chapter | section" mode="get-subnodes">
  <xsl:variable name="href">
    <xsl:text>javascript:set_node('</xsl:text>
    <xsl:value-of select="@path" />
    <xsl:text>')</xsl:text>
  </xsl:variable>
  <xsl:variable name="css_class">
    <xsl:apply-templates select="." mode="css_class" />
  </xsl:variable> 
  <xsl:variable name="title">
    <xsl:call-template name="get-title">
      <xsl:with-param name="book_dir" select="$book_dir" />
      <xsl:with-param name="path" select="@path" />
    </xsl:call-template>
  </xsl:variable> 

  <dt class="{$css_class}">
    <a href="{$href}"><xsl:value-of select="$title"/></a>
  </dt>
  <xsl:if test="(name(..)='book' or name(..)='article' or name(..)='chapter') and not(./simplesect)">
    <dd><dl><xsl:apply-templates mode="get-subnodes"/></dl></dd>
  </xsl:if>
</xsl:template>

<xsl:template mode="css_class" match="chapter">
  <xsl:value-of select="'chapter'" />
</xsl:template>

<xsl:template mode="css_class" match="section">
  <xsl:variable name="level">
    <xsl:call-template name="get-section-level" />
  </xsl:variable>
  <xsl:value-of select="concat('sect', $level)" />
</xsl:template>

<!-- handle simplesect differently -->
<xsl:template match="simplesect" mode="get-subnodes">
  <xsl:variable name="count">
    <xsl:number count="simplesect" format="1" />
  </xsl:variable> 

  <xsl:variable name="bgcolor">
    <xsl:choose>
      <xsl:when test="$count mod 2 = 0">#eeeeee</xsl:when>
      <xsl:otherwise>#f8f8f8</xsl:otherwise>
    </xsl:choose>
  </xsl:variable> 

  <xsl:variable name="href">
    <xsl:text>javascript:set_node('</xsl:text>
    <xsl:value-of select="@path" />
    <xsl:text>')</xsl:text>
  </xsl:variable> 

  <xsl:variable name="title">
    <xsl:call-template name="get-title">
      <xsl:with-param name="book_dir" select="$book_dir" />
      <xsl:with-param name="path" select="@path" />
    </xsl:call-template>
  </xsl:variable> 

  <xsl:variable name="summary">
    <xsl:variable name="xml-file">
      <xsl:value-of select="concat($book_dir, @path)" />
      <xsl:value-of select="'content.xml'" />
    </xsl:variable>
    <xsl:apply-templates mode="get-summary" select="document($xml-file)" />
  </xsl:variable> 

  <xsl:variable name="short-summary">
    <xsl:value-of select="substring(normalize-space($summary), 0, 70)" />
  </xsl:variable>

  <dt style="background:{$bgcolor}">
    <a href="{$href}"><xsl:value-of select="$title" /></a>
    --> <xsl:value-of select="$short-summary" />...
  </dt>
</xsl:template>

<!-- don't include the title in the summary of simplesect -->
<xsl:template mode="get-summary" match="simplesect/title" />

<!-- ignore text nodes -->
<xsl:template match="text()" mode="get-subnodes" />

</xsl:transform>
