<?xml version='1.0'?><!-- -*-SGML-*- -->
<!--
This file  is part of  DocBookWiki.  DocBookWiki is a  web application
that  displays  and  edits  DocBook  documents.  

Copyright (C) 2004, 2005 Dashamir Hoxha, dashohoxha@users.sf.net

DocBookWiki is free software; you can redistribute it and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

DocBookWiki is  distributed in  the hope that  it will be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR A  PARTICULAR PURPOSE.   See  the GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with DocBookWiki; if not, write to the Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
-->

<!--
Explodes a DocBook document and creates the files content.xml 
for each section (node).  It is called by explode.sh like this:
xsltproc -o $outputdir/ content-xml.xsl book.xml
-->

<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
            xmlns:xalanredirect="org.apache.xalan.xslt.extensions.Redirect"
            extension-element-prefixes="xalanredirect">


<xsl:include href="../common/node-path.xsl" />  <!-- find the node path  -->


<!-- write the files content.xml -->
<xsl:template match="book | article | chapter | section | simplesect">
  <xsl:variable name="folder">
    <xsl:call-template name="get-folder" />
  </xsl:variable>
  <xsl:variable name="content-file">
    <xsl:if test="name(.)!='book' and name(.)!='article'">
      <xsl:value-of select="$folder"/>
    </xsl:if>
    <xsl:value-of select="'content.xml'" />
  </xsl:variable>

  <!-- write content.xml -->
  <xalanredirect:write file="{$content-file}" method="xml"
                          encoding="iso-latin-1" omit-xml-declaration="no"
                          standalone="no" indent="yes">
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates/>
    </xsl:copy>
  </xalanredirect:write>
</xsl:template>


<!-- modify attribute fileref of imagedata (remove the prefix) -->
<xsl:template match="imagedata/@fileref">
  <xsl:variable name="path">
    <xsl:apply-templates select="." mode="path" />
  </xsl:variable>
  <xsl:variable name="prefix" select="concat('media/', substring($path,3))" />
  <xsl:attribute name="fileref">
    <xsl:value-of select="substring-after(., $prefix)" />
  </xsl:attribute>
</xsl:template>


<!-- ignore empty text nodes... -->
<xsl:template match="text()">
  <xsl:choose>
    <xsl:when test="normalize-space(.)=''" />
    <xsl:otherwise><xsl:value-of select="." /></xsl:otherwise>
  </xsl:choose>
</xsl:template>
<!-- ...but not for screen or programlisting -->
<xsl:template match="screen/text() | programlisting/text()">
  <xsl:value-of select="." />
</xsl:template>


<!-- ignore some attributes -->
<xsl:template match="@moreinfo['none']" />
<xsl:template match="@format['linespecific']" />


<!-- copy everything else -->
<xsl:template match="*|@*">
  <xsl:copy>
    <xsl:apply-templates select="@*"/>
    <xsl:apply-templates select="node()"/>
  </xsl:copy>
</xsl:template>

</xsl:transform>
